<?php

return [
    'is_osp_feedback_login' => env('IS_OSP_FEEDBACK_LOGIN'),
    'is_osp_feedback_password' => env('IS_OSP_FEEDBACK_PASSWORD'),
    'is_osp_feedback_api_url' => env('IS_OSP_FEEDBACK_API_URL'),
];

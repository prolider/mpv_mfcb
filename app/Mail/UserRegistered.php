<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $subject = 'Регистрация на портале "МойБизнес73"';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $logoData = json_decode(setting('site.logo'));

        return $this
        ->subject('Регистрация нового пользователя на портале "МойБизнес73"')
        ->markdown('vendor.mail.html.registered', compact('logoData'));
    }
}

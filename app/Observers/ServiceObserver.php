<?php

namespace App\Observers;

use App\Models\Affiliate;
use App\Models\AffiliateService;
use App\Models\Service;
use Exception;

class ServiceObserver
{
    /**
     * Handle the service "created" event.
     *
     * @param  Service  $service
     * @return void
     */
    public function created(Service $service)
    {
        $affiliates = Affiliate::all();

        foreach ($affiliates as $affiliate) {
            AffiliateService::create([
                'affiliate_id' => $affiliate->id,
                'service_id' => $service->id,
            ]);
        }
    }

    /**
     * Handle the service "deleted" event.
     *
     * @param  Service  $service
     * @return void
     *
     * @throws Exception
     */
    public function deleted(Service $service)
    {
        if (! is_null($service->form_id)) {
            $service->form->delete();
        }

        $service->affiliateServices()->delete();
    }

    /**
     * Handle the service "restored" event.
     *
     * @param  Service  $service
     * @return void
     */
    public function restored(Service $service)
    {
        if (! is_null($service->form_id)) {
            $service->form->restore();
        }

        $service->affiliateServices()->restore();
    }

    /**
     * Handle the service "force deleted" event.
     *
     * @param  Service  $service
     * @return void
     */
    public function forceDeleted(Service $service)
    {
        if (! is_null($service->form_id)) {
            $service->form->forceDelete();
        }

        $service->affiliateServices()->forceDelete();
    }
}

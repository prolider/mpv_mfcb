<?php

namespace App\Observers;

use App\Models\Log;
use App\Mail\TicketCreated;
use App\Mail\TicketCreatedUser;
use App\Mail\TicketStatusChanged;
use App\Services\MspFeedbackService;
use App\Services\TicketService;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TicketObserver
{
    protected MspFeedbackService $mspFeedBackService;

    public function __construct(MspFeedbackService $mspFeedbackService)
    {
        $this->mspFeedBackService = $mspFeedbackService;
    }

    /**
     * Handle the ticket "created" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function created(Ticket $ticket)
    {
        $this->setFactEndDate($ticket);

        $admin = User::getAdmin();
        $user = $ticket->formResult->user;

        TicketService::creationNotificate($user, $admin, $ticket);

        Mail::to($admin->email)->send(new TicketCreated($user, $ticket));
        Mail::to($user->email)->send(new TicketCreatedUser($user, $ticket));

        if (!is_null($ticket->application_id)) {
            $this->mspFeedBackService->setStatus([
                'applicationId' => $ticket->application_id,
                'status' => Ticket::MSP_STATUS_REGISTERED
            ]);
        }

        if (in_array($ticket->status, ['C', 'R'])) {
            $ticket->fact_end_date = Carbon::now();
        }
    }

    /**
     * Handle the ticket "updated" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function updated(Ticket $ticket)
    {
        $this->setFactEndDate($ticket);

        if ($ticket->wasChanged('status')) {
            Log::create([
                'operator_id' => Auth::id(),
                'ticket_id' => $ticket->id,
                'change_type' => 'status',
                'change_value' => $ticket->status,
            ]);
        }

        if ($ticket->wasChanged('operator_id')) {
            Log::create([
                'operator_id' => Auth::id(),
                'ticket_id' => $ticket->id,
                'change_type' => 'operator',
                'change_value' => $ticket->operator->id,
            ]);
        }

        if ($ticket->checkStatusChange()) {
            if (!is_null($ticket->application_id) && $ticket->getAttribute('status') == Ticket::STATUS_DONE) {
                $this->mspFeedBackService->setStatus([
                    'applicationId' => $ticket->application_id,
                    'status' => Ticket::MSP_STATUS_RENDERED
                ]);
            }

            $user = $ticket->formResult->user;

            Mail::to($user->email)->send(new TicketStatusChanged($ticket, $user));
        }
    }

    protected function setFactEndDate($ticket)
    {
        if (in_array($ticket->status, ['C', 'R'])) {
            $ticket->fact_end_date = Carbon::now();
        }
    }
}

<?php

namespace App\Observers;

use App\Models\Discussion;
use App\Mail\DiscussionCreated;
use App\Models\Notification;
use App\Services\FileService;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class DiscussionObserver
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function created(Discussion $discussion)
    {
        $admin = User::getAdmin();
        $user = User::find($discussion->user_owner_id);

        $sendToAdmin = false;

        if (! $discussion->is_alert) {
            $sendToAdmin = true;
            $text = "Пользователь {$user->fullName} создал обращение";
        } else {
            $sendToAdmin = true;
            $text = "Пользователь {$user->fullName} создал обращение требующее внимание";
        }

        if ($sendToAdmin) {
            User::operator()->get()->each(function (User $admin) use ($discussion, $text) {
                Notification::create([
                    'name' => 'Новое обращение',
                    'description' => $text,
                    'notificable_type' => Discussion::class,
                    'notificable_id' => $discussion->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $admin->id,
                ]);
            });
        }

        Notification::create([
            'name' => 'Новое обращение',
            'description' => $text,
            'notificable_type' => Discussion::class,
            'notificable_id' => $discussion->id,
            'status' => Notification::STATUS_NEW,
            'user_id' => $discussion->user_recipient_id,
        ]);

        Notification::create([
            'name' => 'Новое обращение',
            'description' => 'Вы создали новое обращение',
            'notificable_type' => Discussion::class,
            'notificable_id' => $discussion->id,
            'status' => Notification::STATUS_NEW,
            'user_id' => $user->id,
        ]);

        Mail::to($admin->email)->send(new DiscussionCreated($user));
    }

    public function creating(Discussion $discussion)
    {
        if ($discussion->user_owner_id && $discussion->user_recipient_id) {
            return;
        }
        $discussion->user_owner_id = auth()->user()->id;
        $discussion->user_recipient_id = User::operator()->first()->id;
        $discussion->is_alert = true;
    }

    public function updating(Discussion $discussion)
    {
        if ($discussion->isDirty('filenames')) {
            $new = json_decode($discussion->filenames);
            $original = json_decode($discussion->getOriginal('filenames', []));
            $diffs = [];

            foreach ($original as $data) {
                foreach ($new as $newData) {
                    if ($data->original_name !== $newData->original_name) {
                        $diffs[] = $data;
                    }
                }
            }

            foreach ($diffs as $diff) {
                if (! isset($diff->name)) {      //Voyager does not create a "name" attribute
                    $this->service->delete('public', $diff->download_link);
                } else {
                    $this->service->delete('discussions', $diff->name);
                }
            }
        }
    }

    public function deleted(Discussion $discussion)
    {
        $notification = Notification::where('notificable_type', Discussion::class)
                        ->where('notificable_id', $discussion->id)
                        ->first();

        $notification->delete();

        $files = json_decode($discussion->filenames);

        foreach ((array) $files as $file) {
            Storage::delete($file->download_link);
        }
    }
}

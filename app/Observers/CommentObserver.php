<?php

namespace App\Observers;

use App\Models\Comment;
use App\Mail\CommentCreated;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class CommentObserver
{
    /**
     * Handle the comment "created" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        $user = $comment->user;
        $admin = User::getAdmin();

        if ($user->isNotRegularUser() && $comment->visibility == Comment::VISIBILITY_ALL) {
            $ownerTicket = $comment->ticket->formResult->user;

            Mail::to($ownerTicket->email)->send(new CommentCreated($user));
        }

        Mail::to($admin->email)->send(new CommentCreated($user));
    }
}

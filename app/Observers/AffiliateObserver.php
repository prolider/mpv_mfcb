<?php

namespace App\Observers;

use App\Models\Affiliate;
use App\Models\AffiliateService;
use App\Models\Service;
use Exception;

class AffiliateObserver
{
    /**
     * Handle the affiliate "created" event.
     *
     * @param  Affiliate  $affiliate
     * @return void
     */
    public function created(Affiliate $affiliate)
    {
        $services = Service::all();

        foreach ($services as $service) {
            AffiliateService::create([
                'affiliate_id' => $affiliate->id,
                'service_id' => $service->id,
            ]);
        }
    }

    /**
     * Handle the affiliate "deleted" event.
     *
     * @param  Affiliate  $affiliate
     * @return void
     *
     * @throws Exception
     */
    public function deleted(Affiliate $affiliate)
    {
        $affiliate->affiliateServices()->delete();
    }

    /**
     * Handle the service "restored" event.
     *
     * @param  Affiliate  $affiliate
     * @return void
     */
    public function restored(Affiliate $affiliate)
    {
        $affiliate->affiliateServices()->restore();
    }

    /**
     * Handle the service "force deleted" event.
     *
     * @param  Affiliate  $affiliate
     * @return void
     */
    public function forceDeleted(Affiliate $affiliate)
    {
        $affiliate->affiliateServices()->forceDelete();
    }
}

<?php

namespace App\Services;

class CheckListService
{
    public static function TransformCheckList($checkList)
    {
        $checkList = json_decode($checkList ?? []);

        foreach ($checkList as $key => $value) {
            $value->id = $key + 1;
            $value->checked = false;
        }

        return json_encode($checkList);
    }
}

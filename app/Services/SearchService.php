<?php

namespace App\Services;

use App\Models\Service;

class SearchService
{
    public function searchByService(string|null $search)
    {
        if (empty($search)) {
            return collect();
        }

        $search = 'LOWER("%'.trim($search).'%")';

        $services = Service::where('name', '!=', null)
            ->whereRaw("(LOWER(name) like {$search} OR LOWER(description) like {$search})")
            ->get();

        return $services;
    }
}

<?php

namespace App\Services;

use App\Clients\MspFeedbackClient;
use App\Mail\MSPRegistered;
use App\Models\FormResult;
use App\Models\Service;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class MspFeedbackService
{
    protected MspFeedbackClient $client;

    public function __construct(MspFeedbackClient $client)
    {
        $this->client = $client;
    }

    public function createTicket($application)
    {
        $service = Service::where('feedback', true)->first();

        if (!$service) {
            return ['message' => 'Service measure is not found'];
        }

        $operator = User::operator()->where('role_id', 1)->first();

        $password = Str::random(10);
        $user = User::where('email', $application['inn'].'@mfcb.ru')->first();

        if (!$user) {
            $user = User::create([
                'email' => $application['inn'] . "@mfcb.ru",
                'name' => $application['firstname'],
                'firstname' => $application['surname'],
                'patronymic' => $application['patronymic'],
                'phone' => $application['phone'],
                'company_inn' => $application['inn'] ?? null,
                'company_name' => $application['organizationName'] ?? null,
                'password' => Hash::make($password),
            ]);
        }

        $resetToken = app('auth.password.broker')->createToken($user);

        Mail::to($user->email)->send(new MSPRegistered($user, $resetToken));

        $formResult = FormResult::create([
            'form_id' => $service->form->id,
            'user_id' => $user->id,
        ]);

        Ticket::create([
            'form_result_id' => $formResult->id,
            'operator_id' => optional($operator)->id,
            'status' => Ticket::STATUS_NEW,
            'archived' => 'N',
            'plan_end_date' => null,
            'fact_end_date' => null,
            'attachments' => null,
            'application_id' => $application['applicationId'],
            'affiliate_id' => $operator->affiliate_id,
            'create_from' => Ticket::FROM_MSP,
        ]);

        return ['message' => "Ticket created"];
    }

    public function commit($code)
    {
        return $this->client->commit($code);
    }

    public function getFeedback(?int $limit)
    {
        return $this->client->getFeedback($limit);
    }

    public function setStatus($data)
    {
        return $this->client->setStatus($data);
    }

}

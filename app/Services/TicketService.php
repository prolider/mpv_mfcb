<?php

namespace App\Services;

use App\Models\Affiliate;
use App\Http\Resources\AffiliateResourse;
use App\Http\Resources\WindowResource;
use App\Models\Notification;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Window;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class TicketService
 */
class TicketService
{
    public static function creationNotificate(User $user, User $admin, Ticket $ticket)
    {
        Notification::create([
            'name' => 'Новая заявка',
            'description' => "Пользователь {$user->fullName} создал новую заявку",
            'notificable_type' => Ticket::class,
            'notificable_id' => $ticket->id,
            'status' => Notification::STATUS_NEW,
            'user_id' => $admin->id,
        ]);
    }

    /**
     * @param  Request  $request
     * @param  bool  $archive
     * @return mixed
     */
    public function getApiTicketList(Request $request, bool $archive = false)
    {
        /** @var User $user */
        $user = auth()->user();

        $ticketsQuery = Ticket::with('formResult.form.service.window', 'affiliate', 'operator', 'formResult.user');

        $window = session()->get('window');

        if ($archive) {
            $ticketsQuery->where('tickets.status', '=', Ticket::STATUS_DONE);
        } else {
            $ticketsQuery->where('tickets.status', '!=', Ticket::STATUS_DONE);
        }

        if (! is_null($window)) {
            $ticketsQuery->whereHas('formResult.form.service', function ($query) use ($window) {
                $query->where('services.window_id', '=', $window->id);
            });
        }

        if ($request->has('operator')) {
            $ticketsQuery->where('operator_id', '=', $request->input('operator'));
        }

        if ($request->has('status')) {
            $ticketsQuery->where('tickets.status', '=', $request->input('status'));
        }

        if ($request->has('create_from')) {
            $ticketsQuery->where('tickets.create_from', '=', $request->input('create_from'));
        }

        if ($request->has('start_date')) {
            $startDate = Carbon::createFromFormat('Y-m-d', $request->input('start_date'))->startOfDay();
            $ticketsQuery->where('plan_end_date', '>=', $startDate);
        }

        if ($request->has('end_date')) {
            $endDate = Carbon::createFromFormat('Y-m-d', $request->input('end_date'))->endOfDay();
            $ticketsQuery->where('plan_end_date', '<=', $endDate);
        }

        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $tickets = $ticketsQuery
                ->where('tickets.status', '<>', Ticket::STATUS_DRAFT)
                ->sortable()
                ->paginate();

            foreach($tickets as &$ticket) {
                $ticket->color_indication = self::getTicketColorIndication($ticket);
            }

            return $tickets;
        }

        $tickets = $ticketsQuery->whereHas('formResult', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->sortable()
            ->paginate();

        return $tickets;
    }

    private function getTicketColorIndication(Ticket $ticket) {
        $colors = [
            'green' => 'greenrow', 
            'yellow' => 'yellowrow', 
            'red' => 'redrow',
        ];

        $diffInDays = $ticket->updated_at->diffInDays(Carbon::now(), 'absolute');

        return match($ticket->status) {
            Ticket::STATUS_NEW => match($diffInDays) {
                0 => $colors['green'],
                1, 2 => $colors['yellow'],
                default => $colors['red'],
            },
            Ticket::STATUS_IN_WORK => match($diffInDays) {
                0 => $colors['green'],
                1, 2, 3, 4 => $colors['yellow'],
                default => $colors['red'],
            },
            Ticket::STATUS_NEED_DATA => match($diffInDays) {
                0, 1, 2 => $colors['green'],
                3, 4 => $colors['yellow'],
                default => $colors['red'],
            },
            default => '',
        };
    }

    /**
     * @param  Request  $request
     * @param  bool  $archive
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getTicketList(Request $request, bool $archive = false)
    {
        /** @var User $user */
        $user = auth()->user();

        $ticketsQuery = Ticket::with('formResult.form.service.window', 'affiliate', 'operator', 'formResult.user')
            ->orderBy('created_at', 'desc');

        if ($archive) {
            $ticketsQuery->where('tickets.status', '=', Ticket::STATUS_DONE);
        } else {
            $ticketsQuery->where('tickets.status', '!=', Ticket::STATUS_DONE);
        }

        $window = session()->get('window');

        if (! is_null($window)) {
            $ticketsQuery->whereHas('formResult.form.service', function ($query) use ($window) {
                $query->where('services.window_id', '=', $window->id);
            });
        }

        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $affiliates = AffiliateResourse::collection(Affiliate::all());
            $windows = WindowResource::collection(Window::all());
            $tickets = $ticketsQuery
                ->where('tickets.status', '<>', Ticket::STATUS_DRAFT)
                ->paginate();

            foreach($tickets as &$ticket) {
                $ticket->color_indication = self::getTicketColorIndication($ticket);
            }

            $colorIndicationCounts = array_count_values($tickets->pluck('color_indication')->toArray());

            return compact('tickets', 'affiliates', 'windows', 'colorIndicationCounts');
        }

        $tickets = $ticketsQuery->whereHas('formResult', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->orderBy('id', 'desc')->paginate(10);

        return $tickets;
    }
}

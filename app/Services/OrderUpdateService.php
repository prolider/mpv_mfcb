<?php

namespace App\Services;

use App\Models\FormResult;
use App\Models\Order;
use App\Models\Service;
use App\Models\Ticket;
use App\Models\TicketCheckList;
use App\Models\User;
use Illuminate\Http\Request;

class OrderUpdateService
{
    public static function updateOrderWithStatus($request, $status, $order)
    {
        $operator = auth()->user();

        $service = Service::find($request->get('service_id'));

        if ($status == 'CREATE') {
            $orderStatus = Order::STATUSES['CREATED'];
            $ticketStatus = Ticket::STATUS_NEW;

            $user = static::checkValidatedUserFieldsForStore($request, $order);

            $formResult = FormResult::create([
                'form_id' => $service->form_id,
                'user_id' => $user->id ?? $operator->id,
                'result' => [],
            ]);

            $createFrom = $order->is_damask ? Ticket::FROM_DAMASK : Ticket::FROM_ONLINE;

            $ticket = Ticket::create([
                'form_result_id' => $formResult->id,
                'operator_id' => $operator->id,
                'status' => $ticketStatus,
                'create_from' => $createFrom,
                'archived' => 'N',
            ]);

            TicketCheckList::create([
                'ticket_id' => $ticket->id,
                'service_id' => $service->id,
                'check_list' => $service->check_list,
            ]);
        } else {
            $user = static::checkValidatedUserFieldsForUpdate($request, $order);

            $orderStatus = Order::STATUSES['DRAFT'];
        }

        $order->update([
            'user_id' => $user->id ?? $operator->id,
            'service_id' => $service->id,
            'phone' => $user->phone,
            'email' => $user->email,
            'operator_id' => $operator->id,
            'status' => $orderStatus,
            'user_information' => json_encode($user),
            'affiliate_id' => $operator->affiliate_id,
        ]);

        return $ticket ?? $order;
    }

    private static function checkValidatedUserFieldsForStore(Request $request, Order $order): User
    {
        if (! $order->user_id || ((! $request->user_id || $request->user_id == 'false') &&
            $order->is_damask &&
            $order->user_id == User::operator()->where('role_id', 1)->first()->id)) {
            return User::create(
                ['role_id' => '2'] +
                $request->validated()
            );
        } elseif ($request->user_id && $request->user_id !== 'false') {
            return User::find($request->user_id);
        } else {
            return $order->user;
        }
    }

    private static function checkValidatedUserFieldsForUpdate(Request $request, $order): User
    {
        if (! $order->user_id || ((! $request->user_id || $request->user_id == 'false') &&
            $order->is_damask &&
            $order->user_id == User::operator()->where('role_id', 1)->first()->id)) {
            $user = new User;
            $userValidationValues = array_diff_key($request->validated(), array_flip(['user_id', 'service_id', 'coupon']));
            $userValidationValues = $userValidationValues + ['role_id' => '2'];

            foreach ($userValidationValues as $key => $value) {
                $user->$key = $value;
            }

            return $user;
        } elseif ($request->user_id && $request->user_id !== 'false') {
            return User::find($request->user_id);
        } else {
            return $order->user;
        }
    }
}

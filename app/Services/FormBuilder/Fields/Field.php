<?php

namespace App\Services\FormBuilder\Fields;

class Field
{
    protected $type;

    protected $label;

    protected $name;

    protected $values = [];

    protected $required = false;

    protected $multiple = false;

    public function __construct(array $field)
    {
        $this->type = $field['type'];

        if (isset($field['label'])) {
            $this->label = $field['label'];
        }

        if (isset($field['name'])) {
            $this->name = $field['name'];
        }

        if (isset($field['values'])) {
            $this->values = $field['values'];
        }

        if (isset($field['required'])) {
            $this->required = $field['required'];
        }

        if (isset($field['multiple'])) {
            $this->multiple = $field['multiple'];
        }
    }

    public function validationRules()
    {
        if (isset($this->name)) {
            $rules = [];

            if ($this->required) {
                $rules[] = 'required';
            }

            if (count($this->values) > 0) {
                $rules[] = 'in:'.collect($this->values)->map(function ($value) {
                    return $value['value'];
                })->join(',');
            }

            if ($this->type === 'checkbox-group' || ($this->type === 'select' && $this->multiple)) {
                return [
                    "{$this->name}.*" => implode('|', $rules),
                ];
            }

            return [
                $this->name => implode('|', $rules),
            ];
        }

        return [];
    }

    public function message()
    {
        if (isset($this->name) && isset($this->label)) {
            return [
                $this->name => $this->label,
            ];
        }

        return [];
    }
}

<?php

namespace App\Services\FormBuilder;

use App\Services\FormBuilder\Fields\Field;

class FormBuilder
{
    /**
     * @var array
     */
    protected $originForm;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $fields;

    /**
     * Form constructor.
     *
     * @param  array  $form
     */
    public function __construct($form)
    {
        $this->originForm = $form;

        $fields = collect([]);
        foreach ($form as $page) {
            foreach ($page as $field) {
                $fields->push(new Field($field));
            }
        }

        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function validationRules()
    {
        return $this->fields
            ->map(function (Field $field) {
                return $field->validationRules();
            })
            ->filter(function ($f) {
                return ! empty(current($f));
            })
            ->reduce(function ($accum, $field) {
                return array_merge($accum, $field);
            }, []);
    }

    /**
     * @return array
     */
    public function messages()
    {
        return $this->fields
            ->map(function (Field $field) {
                return $field->message();
            })
            ->reduce(function ($accum, $field) {
                return array_merge($accum, $field);
            }, []);
    }
}

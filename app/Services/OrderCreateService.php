<?php

namespace App\Services;

use App\Http\Controllers\ServiceController;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderCreateService
{
    public static function saveOrderWithStatus($request, $status)
    {
        $operator = auth()->user();

        $service = Service::find($request->get('service_id'));

        if ($status == 'CREATE') {
            $orderStatus = Order::STATUSES['CREATED'];

            $user = static::checkValidatedUserFieldsForStore($request);

            $ticket = self::checkServiceType($service, $user, $operator);
        } else {
            $user = static::checkValidatedUserFieldsForUpdate($request);

            $orderStatus = Order::STATUSES['DRAFT'];
        }

        $order = Order::create([
            'user_id' => $user->id ?? $operator->id,
            'service_id' => $service->id,
            'coupon' => Carbon::now()->format('YmdHis'),
            'phone' => $user->phone,
            'email' => $user->email,
            'operator_id' => $operator->id,
            'status' => $orderStatus,
            'user_information' => json_encode($user),
            'affiliate_id' => $operator->affiliate_id,
        ]);

        return $ticket ?? $order;
    }

    private static function checkServiceType($service, $user, $operator)
    {
        if ($service->use_button == 'N') {
            return self::createInformationalTicket($service, $user, $operator);
        }

        return self::createAccompanyingTicket($service, $user, $operator);
    }

    private static function createInformationalTicket($service, $user, $operator)
    {
        return redirect()->action(
            [ServiceController::class, 'showForm'],
            [
                $service,
                'user' => $user,
                'operator' => $operator,
            ]
        );
    }

    private static function createAccompanyingTicket($service, $user, $operator)
    {
        return redirect()->action(
            [ServiceController::class, 'storeTicketFromOrder'],
            [
                $service,
                'user' => $user,
                'operator' => $operator,
            ]
        );
    }

    private static function checkValidatedUserFieldsForStore(Request $request): User
    {
        if ($request->validated()['user_id'] == 'false') {
            return User::create(
                ['role_id' => '2'] +
                $request->validated()
            );
        } else {
            return User::find($request->validated()['user_id']);
        }
    }

    private static function checkValidatedUserFieldsForUpdate(Request $request): User
    {
        if ($request->validated()['user_id'] == 'false') {
            $user = new User;
            $userValidationValues = array_diff_key($request->validated(), array_flip(['user_id', 'service_id', 'coupon']));
            $userValidationValues = $userValidationValues + ['role_id' => '2'];

            foreach ($userValidationValues as $key => $value) {
                $user->$key = $value;
            }

            return $user;
        } else {
            return User::find($request->validated()['user_id']);
        }
    }
}

<?php

namespace App\Services;

use App\Models\FormResult;
use App\Mail\MSPRegistered;
use App\Models\Support;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use ReallySimpleJWT\Token;
use Str;

class MspService
{
    protected const STATUSES_EX = [
        'N' => 'Отправлена',
        'W' => 'Взята в работу',
        'O' => 'Оформление',
        'C' => 'Отменена',
        'D' => 'Услуга оказана',
        'R' => 'Черновик',
        'L' => 'Клиент заполняет',
        'P' => 'Запрос на отмену',
        'E' => 'Требует уточнения',
        'F' => 'Отказ',
        'I' => 'Приглашение',
        'CR' => 'Создана заявка',
    ];

    protected const STATUSES = [
        'N' => 1,
        'W' => 2,
        'O' => 2,
        'C' => 4,
        'D' => 5,
        'R' => 2,
        'L' => 2,
        'P' => 2,
        'E' => 3,
        'F' => 4,
        'I' => 5,
        'CR' => 5,
    ];

    protected const AUTH_PATH = '/Auth/';

    protected const REFERENCE_PATH = '/Regional/Reference/Substitution/';

    protected const SUPPORT_REFERENCES_PATH = '/Regional/Reference/Lists/';

    protected static $token;

    public function getJwtTokenFromMSP()
    {
        if (isset(self::$token)) {
            return self::$token;
        }

        $data = [
            'login' => env('IS_OSP_LOGIN'),
            'password' => env('IS_OSP_PASSWORD'),
        ];

        if (env('IS_TEST_OSP')) {
            $http = Http::withBasicAuth(env('IS_TEST_OSP_BASE_USER'), env('IS_TEST_OSP_BASE_PASSWORD'));
        } else {
            $http = new Http();
        }

        $response = $http->withHeaders(['Content-Type' => 'application/json'])
            ->post(env('MSP_API_URL').self::AUTH_PATH, $data);
        $response = json_decode($response, true);

        if (isset($response['code']) && isset($response['message'])) {
            throw new \Exception(
                'Authorization failed with code: '.$response['code'].
                ', with message: '.$response['message']
            );
        }

        if (isset($response['jwt'])) {
            self::$token = $response['jwt'];

            return self::$token;
        }

        throw new \Exception('Bad request');
    }

    public function getReferenceList($jwtToken)
    {
        if (env('IS_TEST_OSP')) {
            $http = Http::withBasicAuth(env('IS_TEST_OSP_BASE_USER'), env('IS_TEST_OSP_BASE_PASSWORD'));
        } else {
            $http = new Http();
        }

        $response = $http->withHeaders(['X-AUTH-JWT-TOKEN' => $jwtToken])
            ->get(env('MSP_API_URL').self::REFERENCE_PATH);

        return json_decode($response, true);
    }

    public function getSupportReferencesList($jwtToken)
    {
        if (env('IS_TEST_OSP')) {
            $http = Http::withBasicAuth(env('IS_TEST_OSP_BASE_USER'), env('IS_TEST_OSP_BASE_PASSWORD'));
        } else {
            $http = new Http();
        }

        $response = $http->withHeaders(['X-AUTH-JWT-TOKEN' => $jwtToken])
            ->get(env('MSP_API_URL').self::SUPPORT_REFERENCES_PATH);

        return json_decode($response, true);
    }

    public function genereateJwtTokenForMSP()
    {
        $secret = env('MSP_JWT_SECRET');
        $issuedTime = time();
        $expiration = $issuedTime + 600;

        return Token::builder()->setSecret($secret)
            ->setExpiration($expiration)
            ->setIssuedAt(time())
            ->build()
            ->getToken();
    }

    public function validateCredentialsFromMSP(Request $request)
    {
        if (
            $request->input('login') !== env('MSP_LOGIN')
            || $request->input('password') !== env('MSP_PASSWORD')
        ) {
            return ['message' => 'Wrong password or login'];
        }

        return ['jwt' => $this->genereateJwtTokenForMSP()];
    }

    public function checkJwtToken($token)
    {
        $secret = env('MSP_JWT_SECRET');

        if (! Token::validate($token, $secret)) {
            return ['message' => 'Wrong or expired jwt token'];
        }

        return [];
    }

    public function checkAuthentication(Request $request)
    {
        if (! $token = $request->header('X-AUTH-JWT-TOKEN')) {
            return ['message' => 'JWT token is missing'];
        }

        return $this->checkJwtToken($token);
    }

    public function createTicket(Request $request)
    {
        $support = Support::find($request->input('id'));

        if (! $support) {
            return ['message' => 'Support measure is not found'];
        }

        $operator = User::operator()->where('role_id', 1)->first();

        $fields = $request->input('fields');

        $user = User::where('email', $fields['email'])->first();

        if (! $user) {
            $password = Str::random(10);
            $user = User::create([
                'email' => $fields['email'],
                'inn' => $fields['inn'],
                'firstname' => $fields['firstname'],
                'name' => $fields['name'],
                'patronymic' => $fields['patronymic'],
                //'sex' => $fields['sex'],
                'birthday' => Carbon::parse($fields['birthday'])->format('Y-m-d'),
                'phone' => $fields['phone'],
                'snils' => $fields['snils'],
                'citizenship' => $fields['citizenship'],
                'passport_serial' => $fields['passport_serial'],
                'passport_number' => $fields['passport_number'],
                'passport_date' => Carbon::parse($fields['passport_date'])->format('Y-m-d'),
                'passport_subdivision' => $fields['passport_subdivision'],
                'passport_organization' => $fields['passport_organization'],
                'registration_place' => $fields['registration_place'],
                'company_inn' => $fields['company_inn'] ?? null,
                'company_ogrn' => $fields['company_ogrn'] ?? null,
                'company_ogrnip' => $fields['company_ogrnip'] ?? null,
                'company_name' => $fields['company_name'] ?? null,
                'password' => Hash::make($password),
            ]);

            $resetToken = app('auth.password.broker')->createToken($user);

            Mail::to($user->email)->send(new MSPRegistered($user, $resetToken));
        }

        $formResult = FormResult::create([
            'form_id' => $support->service->form->id,
            'user_id' => $user->id,
        ]);

        $ticket = Ticket::create([
            'form_result_id' => $formResult->id,
            'operator_id' => optional($operator)->id,
            'status' => Ticket::STATUS_NEW,
            'archived' => 'N',
            'plan_end_date' => null,
            'fact_end_date' => null,
            'attachments' => null,
            'affiliate_id' => $operator->affiliate_id,
            'create_from' => Ticket::FROM_MSP,
        ]);

        $fields = array_map(function ($value) use ($ticket) {
            if (is_array($value)) {
                $valueFiles = (array) $value[0];
                if (array_key_exists('data', $valueFiles)) {
                    if ($valueFiles['data'] == null) {
                        return null;
                    }

                    $path = "tickets/{$ticket->id}/".$valueFiles['name'];
                    Storage::disk('public')->put($path, base64_decode($valueFiles['data']));

                    return [
                        [
                            'name' => $valueFiles['name'],
                            'path' => $path,
                        ],
                    ];
                }
            }

            if (! is_array($value)) {
                $datetime = \DateTime::createFromFormat('Y-m-d\TH:i:s???????', $value);

                if ($datetime !== false) {
                    return $datetime->format('d-m-Y');
                }
            }

            return $value;
        }, $fields);

        $formResult->update(['result' => $fields]);

        return $this->getTicketStatus($ticket, null);
    }

    public function getTicketStatus(?Ticket $ticket, ?Request $request)
    {
        $response = [
            'id' => 0,
            'idService' => 0,
            'idForm' => 0,
            'statusEx' => 0,
            'status' => 0,
            'description' => '',
            'files' => [],
            'errorFields' => [],
        ];

        if ($ticket) {
            $support = $ticket->formResult->form->service->support;

            $response['id'] = $ticket->id;
            $response['idService'] = $support ? $support->id : 0;
            $response['statusEx'] = self::STATUSES_EX[$ticket->status];
            $response['status'] = self::STATUSES[$ticket->status];

            return $response;
        }

        return ['message' => 'Ticket is not found'];
    }

    public static function getFieldType($field)
    {
        $references = [
            'text' => [
                'type' => 'string',
            ],
            'tel' => [
                'type' => 'phone',
            ],
            'email' => [
                'type' => 'email',
            ],
            'file' => [
                'type' => 'fileBase',
            ],
            'number' => [
                'type' => 'int',
            ],
            'date' => [
                'type' => 'date',
            ],
        ];

        $options = self::getOptions($field->options ?? null);

        $referencesTag = [
            'textarea' => [
                'type' => 'text',
            ],
            'select' => [
                'type' => 'select',
                'values' => $options,
            ],
            'radio' => [
                'type' => 'checkbox',
                'multiple' => false,
                'values' => $options,
            ],
            'checkbox' => [
                'type' => 'checkbox',
                'multiple' => true,
                'values' => $options,
            ],
            'custom' => [
                'type' => 'string',
            ],
            'accordion' => [
                'type' => 'string',
            ],
        ];

        $referencesBind = [
            'birthday' => [
                'type' => 'date',
            ],
            'email' => [
                'type' => 'email',
            ],
            'passport_serial' => [
                'type' => 'int',
            ],
            'passport_number' => [
                'type' => 'int',
            ],
            'passport_date' => [
                'type' => 'date',
            ],
            'inn' => [
                'type' => 'int',
            ],
            'phone' => [
                'type' => 'phone',
            ],
        ];

        $value = $references[$field->type ?? null] ?? null;

        if (is_null($value)) {
            $value = $referencesTag[$field->tag ?? null] ?? null;
        }

        if (is_null($value)) {
            $value = $referencesBind[$field->bind ?? null] ?? ['type' => 'string'];
        }

        return $value;
    }

    private static function getOptions($options)
    {
        if ($options) {
            $options = collect($options);

            $options = $options->mapWithKeys(function ($value) {
                return [$value->value => $value->name];
            });

            return $options;
        }

        return [];
    }
}

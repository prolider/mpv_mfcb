<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class CopyServiceAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Создать копию услуги';
    }

    public function getIcon()
    {
        return 'voyager-documentation';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right edit',
            'style' => 'margin-right: 5px;',
        ];
    }

    public function getDefaultRoute()
    {
        return route('operator.copy.service', ['service' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'services';
    }
}

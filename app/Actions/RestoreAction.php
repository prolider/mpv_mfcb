<?php

namespace App\Actions;

use TCG\Voyager\Actions\RestoreAction as VoyagerRestoreAction;

class RestoreAction extends VoyagerRestoreAction
{
    public function getIcon()
    {
        return 'voyager-refresh';
    }
}

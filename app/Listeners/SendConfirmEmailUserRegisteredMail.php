<?php

namespace App\Listeners;

use App\Mail\ConfirmMail;
use Illuminate\Support\Facades\Mail;

class SendConfirmEmailUserRegisteredMail
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to($event->user->email)->send(new ConfirmMail($event->user));
    }
}

<?php

namespace App\Listeners;

use App\Mail\UserRegistered;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

class SendEmailUserRegisteredNotification
{
    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $admin = User::getAdmin();

        Mail::to($admin->email)->send(new UserRegistered($event->user));
    }
}

<?php

namespace App\Listeners;

use App\Models\Form;
use App\Models\Service;

class ServiceUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->data && $event->data instanceof Service) {
            /** @var Service $service */
            $service = $event->data;
            $formData = request('form_builder');

            if (! is_null($service->form_id)) {
                /** @var Form $form */
                $form = Form::query()->findOrFail($service->form_id);
            } else {
                $form = new Form();
                $form->is_public = Form::IS_PUBLIC;
                $form->status = Form::STATUS_ACTIVE;
                $form->name = $service->name;
                $form->recaptcha = Form::USE_RECAPTCHA;
            }

            $form->form_data = $formData;

            if (request('deleted_fields')) {
                $form->deleted_fields = array_merge($form->deleted_fields ?? [], json_decode(request('deleted_fields')));
            }

            $form->save();

            $service->form_id = $form->id;
            $service->save();
        }
    }
}

<?php

namespace App\Providers;

use App\Events\SendConfirmLink;
use App\Listeners\SendConfirmEmailUserRegisteredMail;
use App\Listeners\SendEmailUserRegisteredNotification;
use App\Listeners\ServiceUpdate;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendEmailUserRegisteredNotification::class,
            SendConfirmEmailUserRegisteredMail::class,
        ],
        SendConfirmLink::class => [
            SendConfirmEmailUserRegisteredMail::class,
        ],
        BreadDataUpdated::class => [
            ServiceUpdate::class,
        ],
        BreadDataAdded::class => [
            ServiceUpdate::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

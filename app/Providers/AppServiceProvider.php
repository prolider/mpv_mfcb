<?php

namespace App\Providers;

use App\Actions\CopyMspSupportAction;
use App\Actions\CopyServiceAction;
use App\Actions\RestoreAction;
use App\Models\Affiliate;
use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Observers\AffiliateObserver;
use App\Observers\DiscussionMessageObserver;
use App\Observers\DiscussionObserver;
use App\Observers\ServiceObserver;
use App\Observers\TicketObserver;
use App\Models\Service;
use App\Models\Ticket;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Actions\RestoreAction as VoyagerRestoreAction;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.env') == 'production') {
            $this->app['request']->server->set('HTTPS', true);
        }

        Voyager::addAction(CopyServiceAction::class);
        Voyager::addAction(CopyMspSupportAction::class);
        Voyager::replaceAction(VoyagerRestoreAction::class, RestoreAction::class);

        Blade::if('auth', function () {
            return auth()->user();
        });

        DiscussionMessage::observe(DiscussionMessageObserver::class);
        Discussion::observe(DiscussionObserver::class);
        Service::observe(ServiceObserver::class);
        Affiliate::observe(AffiliateObserver::class);
        Ticket::observe(TicketObserver::class);
    }
}

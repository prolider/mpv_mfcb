<?php

namespace App\Console\Commands;

use App\Models\Affiliate;
use App\Models\AffiliateService;
use App\Models\Service;
use Illuminate\Console\Command;

class AddAffiliateInServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'affiliate:service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Экспорт параметров Voyager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $affiliates = Affiliate::all();

        foreach (Service::all() as $service) {
            foreach ($affiliates as $affiliate) {
                AffiliateService::firstOrCreate([
                    'service_id' => $service->id,
                    'affiliate_id' => $affiliate->id,
                ]);
            }
        }

        $this->info('DONE!');

        return 0;
    }
}

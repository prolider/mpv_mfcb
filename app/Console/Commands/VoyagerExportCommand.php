<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VoyagerExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voyager:export-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Экспорт параметров Voyager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dataRows = DB::table('data_rows')->get();
        $dataTypes = DB::table('data_types')->get();

        $menuItems = DB::table('menu_items')->get();
        $menus = DB::table('menus')->get();

        $settings = DB::table('settings')->get();

        $permissions = DB::table('permissions')->get();

        $params = json_encode(compact('dataRows', 'dataTypes', 'menuItems', 'menus', 'settings', 'permissions'));

        Storage::put('voyager.json', $params);

        $this->info('DONE!');

        return 0;
    }
}

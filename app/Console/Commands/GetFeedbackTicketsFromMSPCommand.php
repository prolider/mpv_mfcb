<?php

namespace App\Console\Commands;

use App\Models\Service;
use App\Models\Ticket;
use App\Services\MspFeedbackService;
use Illuminate\Console\Command;

class GetFeedbackTicketsFromMSPCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'msp:feedback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create MSP Feedback tickets';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(MspFeedbackService $mspFeedbackService)
    {
        $response = $mspFeedbackService->getFeedback(100);

        try {
            foreach ($response->json('applications') as $application) {
                $this->line('Create ticket from MSP '.'applicationId = '.$application['applicationId']);
                $result = $mspFeedbackService->createTicket($application);
                $this->line($result['message']);
            }

            $response = $mspFeedbackService->commit($response->json('commit'));

            if ($response->json('statusCode') !== 200 && $response->json('data') != null) {
                $this->line(
                    'Commit failed with with message: ' .$response->json('data')
                );
            } else {
                $this->line('Commit successful');
            }

        } catch (\Exception $exception) {
            $this->line($exception->getMessage());
        }

        $this->line('Applications have been completed.');

        return true;
    }
}

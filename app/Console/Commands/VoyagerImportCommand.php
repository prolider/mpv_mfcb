<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VoyagerImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voyager:import-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импорт параметров Voyager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! Storage::exists('voyager.json')) {
            $this->error('Отсутсвует файл для импорта настроек.');

            return 1;
        }

        $params = json_decode(Storage::get('voyager.json'), true);

        DB::table('data_rows')->delete();
        DB::table('data_types')->delete();
        DB::table('menu_items')->delete();
        DB::table('menus')->delete();
        DB::table('settings')->delete();

        DB::table('data_types')->insert($params['dataTypes']);
        DB::table('data_rows')->insert($params['dataRows']);
        DB::table('menus')->insert($params['menus']);
        DB::table('menu_items')->insert($params['menuItems']);
        DB::table('settings')->insert($params['settings']);

        $permissions = DB::table('permissions')->get();

        foreach ($params['permissions'] as $permission) {
            $existsPermission = $permissions->first(function ($p) use ($permission) {
                return $p->key === $permission['key'] && $p->table_name === $permission['table_name'];
            });

            if (is_null($existsPermission)) {
                DB::table('permissions')->insert($permission);
            }
        }

        $this->info('DONE!');

        return 0;
    }
}

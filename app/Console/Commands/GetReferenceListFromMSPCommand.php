<?php

namespace App\Console\Commands;

use App\Models\FormReference;
use App\Services\MspService;
use App\Models\SupportEventName;
use App\Models\SupportRecipientSpecialCategory;
use App\Models\SupportServiceName;
use App\Models\SupportServiceType;
use App\Models\SupportSourceType;
use App\Models\SupportType;
use Illuminate\Console\Command;

class GetReferenceListFromMSPCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'msp:reference';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get MSP user field reference list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(MspService $mspService)
    {
        $jwtToken = $mspService->getJwtTokenFromMSP();

        $formReferences = $mspService->getReferenceList($jwtToken);

        foreach ($formReferences as $formReference) {
            FormReference::updateOrCreate([
                'code' => $formReference['code'],
            ], [
                'name' => $formReference['name'],
                'description' => $formReference['description'],
            ]);
        }

        $supportReferences = $mspService->getSupportReferencesList($jwtToken);

        $recipientSpecialCategories = $supportReferences['recipientSpecialCategory'] ?? [];
        $serviceNames = $supportReferences['serviceName'] ?? [];
        $serviceTypes = $supportReferences['serviceType'] ?? [];
        $sourceTypes = $supportReferences['sourceType'] ?? [];
        $supportEventNames = $supportReferences['supportEventName'] ?? [];
        $supportTypes = $supportReferences['supportType'] ?? [];

        foreach ($supportTypes as $supportType) {
            SupportType::updateOrCreate([
                'code' => $supportType['CODE'] ?? 0,
            ], [
                'name' => $supportType['NAME'] ?? 0,
            ]);
        }

        foreach ($supportEventNames as $supportEventName) {
            SupportEventName::updateOrCreate([
                'code' => $supportEventName['CODE'] ?? 0,
            ], [
                'name' => $supportEventName['NAME'] ?? 0,
            ]);
        }

        foreach ($sourceTypes as $sourceType) {
            SupportSourceType::updateOrCreate([
                'code' => $sourceType['CODE'] ?? 0,
            ], [
                'name' => $sourceType['NAME'] ?? 0,
            ]);
        }

        foreach ($serviceTypes as $serviceType) {
            SupportServiceType::updateOrCreate([
                'code' => $serviceType['CODE'] ?? 0,
            ], [
                'name' => $serviceType['NAME'] ?? 0,
            ]);
        }

        foreach ($serviceNames as $serviceName) {
            SupportServiceName::updateOrCreate([
                'code' => $serviceName['CODE'] ?? 0,
            ], [
                'name' => $serviceName['NAME'] ?? 0,
            ]);
        }

        foreach ($recipientSpecialCategories as $recipientSpecialCategory) {
            SupportRecipientSpecialCategory::updateOrCreate([
                'code' => $recipientSpecialCategory['CODE'] ?? 0,
            ], [
                'name' => $recipientSpecialCategory['NAME'] ?? 0,
            ]);
        }

        return 'Updated';
    }
}

<?php

namespace App\Clients;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class MspFeedbackClient
{
    protected PendingRequest $client;

    protected const AUTH_PATH = '/auth';

    protected const EXPRESS_CONSULTATION = '/feedback';

    protected const CONFIRMATION_OF_MESSAGE_PROCESSING = '/commit/';

    protected const STATUS = '/status';

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->client = Http::baseUrl(config('msp-feedback.is_osp_feedback_api_url'))
            ->withHeaders(['Content-Type' => 'application/json']);

        $token = $this->getJwtToken();

        $this->client->withHeaders(['X-AUTH-JWT-TOKEN' => $token]);
    }

    /**
     * @throws \Exception
     */
    public function getJwtToken()
    {
        $response = $this->client->post(self::AUTH_PATH, [
                'login' => config('msp-feedback.is_osp_feedback_login'),
                'password' => config('msp-feedback.is_osp_feedback_password'),
            ]
        );

        $token = $response->json('data.token');

        if (!$token) {
            throw new \Exception(
                'Authorization failed with code: ' . $response['code'] .
                ', with message: ' . $response['message']
            );
        }

        return $token;
    }

    public function commit($code)
    {
        return $this->client->get(self::CONFIRMATION_OF_MESSAGE_PROCESSING . $code);
    }

    public function getFeedback(?int $limit)
    {
        return $this->client->get(self::EXPRESS_CONSULTATION, ['limit' => $limit]);
    }

    public function setStatus($data)
    {
        return $this->client->post(self::STATUS, $data);
    }
}

<?php

namespace App\Rules;

use App\Models\Service;
use Illuminate\Contracts\Validation\Rule;

class Feedback implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    protected $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $feedback = Service::where('feedback', true)->pluck('id')->toArray();

        if (empty($feedback) || in_array($this->id, $feedback)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Express-услуга должна быть единственной';
    }
}

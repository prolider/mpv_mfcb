<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_owner_id');
    }

    protected $fillable = [
        'id',
        'user_owner_id',
        'company_inn',
        'company_ogrn',
        'company_name',
        'company_registration_date',
        'company_kpp',
        'company_registration_adress',
        'company_fio_head',
        'company_head_position',
        'company_confirmed',
        'company_confirmation_date',
    ];
}

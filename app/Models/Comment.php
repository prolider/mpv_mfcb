<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 *
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property string $text
 * @property string $attachment
 * @property array $attachments
 * @property string $visibility
 * @property string $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Ticket $ticket
 * @property User $user
 */
class Comment extends Model
{
    use SoftDeletes;

    const STATUS_NEW = 'N';

    const STATUS_READED = 'R';

    const VISIBILITY_ALL = 'A';

    const VISIBILITY_OPERATOR = 'O';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'ticket_id', 'user_id', 'text', 'attachment', 'visibility', 'status',
    ];

    protected $casts = [
        'attachment' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return array
     */
    public function getAttachmentsAttribute()
    {
        if ($this->attachment) {
            return is_array($this->attachment) ? $this->attachment : json_decode($this->attachment, true);
        } else {
            return [];
        }
    }

    /**
     * Возвращает является ли комментарий новым для текущего пользователя
     *
     * @return bool
     */
    public function getIsNewAttribute()
    {
        return $this->status === self::STATUS_NEW && $this->user_id !== auth()->id();
    }
}

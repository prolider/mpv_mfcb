<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Service
 *
 * @property int $id
 * @property int $service_category_id
 * @property int $window_id
 * @property int $form_id
 * @property string $name
 * @property string $description
 * @property string $use_button
 * @property int $service_period
 * @property string $service_period_type
 * @property string $official_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Form $form
 * @property ServiceCategory $category
 * @property AffiliateService $affiliateServices
 * @property Window $window
 * @property Collection $documents
 * @property string $check_list
 */
class Service extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'service_category_id', 'window_id', 'use_button', 'status', 'service_period',
        'service_period_type', 'form_id', 'official_name', 'links', 'check_list', 'affiliate_id',
    ];

    /**
     * @return BelongsTo
     */
    public function form(): BelongsTo
    {
        return $this->belongsTo(Form::class)->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function window(): BelongsTo
    {
        return $this->belongsTo(Window::class);
    }

    /**
     * @return HasOne
     */
    public function support(): HasOne
    {
        return $this->hasOne(Support::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id');
    }

    /**
     * @return HasMany
     */
    public function affiliateServices(): HasMany
    {
        return $this->hasMany(AffiliateService::class);
    }

    /**
     * @return HasMany
     */
    public function documents(): HasMany
    {
        return $this->hasMany(ServiceDocument::class)->orderBy('sort');
    }

    /**
     * @return HasMany
     */
    public function ticketCheckLists(): HasMany
    {
        return $this->hasMany(TicketCheckList::class)->orderBy('sort');
    }

    /**
     * @return BelongsTo
     */
    public function affiliate(): BelongsTo
    {
        return $this->belongsTo(Affiliate::class, 'affiliate_id');
    }

    /**
     * @return array
     */
    public function getLinksArrayAttribute(): array
    {
        $links = [];
        if ($this->links) {
            $links = json_decode($this->links, true) ?? [];
        }

        return $links;
    }

    /**
     * @return array
     */
    public function getCheckListArrayAttribute(): array
    {
        $checkList = [];
        if ($this->check_list) {
            $checkList = json_decode($this->check_list, true) ?? [];
        }

        return $checkList;
    }
}

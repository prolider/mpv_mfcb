<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Role;

/**
 * Class Invite
 *
 * @property int $id
 * @property int $window_id
 * @property int $user_id
 * @property int $operator_id
 * @property Carbon $invite_time
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Window $window
 * @property User $user
 * @property User $operator
 */
class Invite extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id', 'window_id', 'ticket_id', 'user_id', 'operator_id', 'invite_time',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'invite_time',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function window()
    {
        return $this->belongsTo(Window::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        $userRoleId = Role::query()
            ->where('name', '=', 'user')
            ->get()
            ->pluck('id')
            ->toArray();

        return $this->belongsTo(User::class)
            ->whereIn('users.role_id', $userRoleId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        $administratorRoles = Role::query()
            ->where('name', '=', 'operator')
            ->orWhere('name', '=', 'admin')
            ->get()
            ->pluck('id')
            ->toArray();

        return $this->belongsTo(User::class)
            ->whereIn('users.role_id', $administratorRoles);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class SupportFieldReference
 *
 * @property int $id
 * @property int $form_reference_id
 * @property int $code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class SupportFieldReference extends Model
{
    public const FIRSTNAME = 1;

    public const NAME = 2;

    public const PATRONYMIC = 3;

    public const INN = 4;

    public const EMAIL = 5;

    public const SNILS = 6;

    public const SEX = 7;

    public const PASSPORT_SERIAL = 8;

    public const PASSPORT_NUMBER = 9;

    public const PASSPORT_DATE = 10;

    public const PASSPORT_SUBDIVISION = 11;

    public const PASSPORT_ORGANIZATION = 12;

    public const REGISTRATION_PLACE = 13;

    public const CITIZENSHIP = 14;

    public const PHONE = 15;

    public const BIRTHDAY = 16;

    public const COMPANY_NAME = 17;

    public const COMPANY_INN = 18;

    public const COMPANY_OGRN = 19;

    public const COMPANY_OGRNIP = 20;

    protected $fillable = [
        'id', 'code', 'form_reference_id',
    ];

    public static function getFieldList()
    {
        return [
            self::FIRSTNAME => 'Фамилия',
            self::NAME => 'Имя',
            self::PATRONYMIC => 'Отчество',
            self::INN => 'ИНН',
            self::EMAIL => 'E-mail',
            self::SNILS => 'Снилс',
            self::SEX => 'Пол',
            self::PASSPORT_SERIAL => 'Серия паспорта',
            self::PASSPORT_NUMBER => 'Номер паспорта',
            self::PASSPORT_DATE => 'Дата выдачи пасспорта',
            self::PASSPORT_SUBDIVISION => 'Код подразделения',
            self::PASSPORT_ORGANIZATION => 'Выдан',
            self::REGISTRATION_PLACE => 'Адрес регистрации',
            self::CITIZENSHIP => 'Гражданство',
            self::PHONE => 'Номер телефона',
            self::BIRTHDAY => 'Дата рождения',
            self::COMPANY_NAME => 'Наименование организации',
            self::COMPANY_INN => 'ИНН организации',
            self::COMPANY_OGRN => 'ОГРН',
            self::COMPANY_OGRNIP => 'ОГРНИП',
        ];
    }

    public function getReferenceCodeAttribute()
    {
        return $this->formReference->code;
    }

    public function getFieldNameAttribute()
    {
        return Arr::get(self::getFieldList(), $this->code, null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formReference()
    {
        return $this->belongsTo(FormReference::class);
    }
}

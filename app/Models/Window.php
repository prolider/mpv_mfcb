<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Window extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'affiliate_id',
    ];

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function forSelect()
    {
        return Window::all()->pluck('name', 'id');
    }

    public static function forSelectChange()
    {
        return Window::query()->with('categories')->get()->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(ServiceCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'affiliate_id');
    }

    public function scopeFilterByAffiliate(Builder $query, $affiliates)
    {
        return $query->whereAffiliateId($affiliates);
    }
}

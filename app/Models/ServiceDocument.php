<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceDocument
 *
 * @property int $id
 * @property int $service_id
 * @property string $name
 * @property string $file
 * @property int $sort
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Service $service
 */
class ServiceDocument extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'service_id', 'name', 'file', 'sort',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function getDownloadNameAttribute()
    {
        $ext = last(explode('.', $this->file));

        return "{$this->name}.$ext";
    }
}

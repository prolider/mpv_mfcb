<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 *
 * @property int $id
 * @property int $user_id
 * @property string $phone
 * @property string $email
 * @property string $slug_name // Название улуги из ДАМАСК
 * @property int $service_id
 * @property int $operator_id
 * @property string $status
 * @property json $user_information
 */
class Order extends Model
{
    use SoftDeletes;

    const STATUSES = [
        'IN_WORK' => 'W',
        'DRAFT' => 'R',
        'CREATED' => 'CR',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'phone', 'email', 'slug_name', 'service_id',
        'operator_id', 'status', 'coupon', 'user_information', 'affiliate_id', 'is_damask',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliate()
    {
        return $this->BelongsTo(Affiliate::class, 'affiliate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->BelongsTo(Service::class, 'service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->BelongsTo(User::class, 'operator_id');
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saving(function ($order) {
            if (! $order->affiliate_id) {
                $userAffiliateId = User::find($order->operator_id)->affiliate_id;

                $order->affiliate_id = $userAffiliateId;
            } else {
                $userAffiliateId = User::find($order->operator_id)->affiliate_id;

                if ($order->affiliate_id !== $userAffiliateId && !$order->is_damask) {
                    $order->affiliate_id = $userAffiliateId;
                }
            }
        });
    }
}

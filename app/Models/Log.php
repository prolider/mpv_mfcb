<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'operator_id', 'ticket_id', 'change_type', 'change_value',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * @return string
     */
    public function getChangeValueAttribute($value)
    {
        return $this->change_type === 'operator'
            ? User::find($value)->fullName
            : $value;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class DiscussionMessage extends Model
{
    public const STATUS_UNREAD = 0;

    public const STATUS_READ = 1;

    protected $guarded = [
        'id',
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'filenames' => 'array',
    ];

    public function getFilesUrlsAttribute()
    {
        return collect($this->filenames)->mapWithKeys(function ($file) {
            return [$file['name'] => Storage::url($file['download_link'])];
        });
    }

    public function discussion(): BelongsTo
    {
        return $this->belongsTo(Discussion::class, 'discussion_group_id');
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_owner_id');
    }

    public static function formFilename(array $filenameArray, int $number = 1): string
    {
        return $filenameArray[0].'('.$number.').'.$filenameArray[1];
    }

    public function setStatus($status)
    {
        $this->update(
            [
                'status' => $status,
                'status_changed_date' => now(),
            ]
        );
    }
}

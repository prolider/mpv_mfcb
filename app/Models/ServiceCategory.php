<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class ServiceCategory
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property string $image
 * @property string $class_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Collection $windows
 * @property Collection $services
 */
class ServiceCategory extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'A';

    const STATUS_DISABLED = 'D';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'status', 'image', 'class_name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function windows()
    {
        return $this->belongsToMany(Window::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
        return $this->hasMany(Service::class);
    }
}

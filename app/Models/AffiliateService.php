<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Affiliate
 *
 * @property int $id
 * @property int $name
 */
class AffiliateService extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'service_id', 'affiliate_id', 'damask_service_code',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class);
    }
}

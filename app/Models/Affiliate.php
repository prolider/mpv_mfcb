<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Affiliate
 *
 * @property int $id
 * @property int $name
 * @property AffiliateService $affiliateServices
 */
class Affiliate extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'code',
    ];

    /**
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return HasMany
     */
    public function windows(): HasMany
    {
        return $this->hasMany(Window::class);
    }

    /**
     * @return HasMany
     */
    public function affiliateServices(): HasMany
    {
        return $this->hasMany(AffiliateService::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Support extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id', 'form_id', 'service_id', 'active', 'name', 'preview',
        'short_description', 'start_date', 'end_date',
        'support_type', 'support_count', 'support_amount_from', 'support_amount_till',
        'full_description', 'opf',
    ];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'opf' => 'array',
    ];

    public const OPF_LIST = [
        'individual' => 'ИП',
        'legal' => 'Юр.лицо',
        'physical' => 'Физ.лицо',
        'self' => 'Самозанятый',
        'selfindividual' => 'Самозанятый ИП',
    ];

    public const RECIPIENT_CATEGORY = [
        'micro' => 'Микропредприятие',
        'small' => 'Малое предприятие',
        'medium' => 'Среднее предприятие',
        'other' => 'Не определено (все)',
    ];

    /**
     * @return BelongsTo
     */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class)->withTrashed();
    }

    public function supportType(): BelongsTo
    {
        return $this->belongsTo(SupportType::class, 'support_type', 'id');
    }

    public function supportServiceName(): BelongsTo
    {
        return $this->belongsTo(SupportServiceName::class, 'support_service_name', 'id');
    }

    public function supportServiceType(): BelongsTo
    {
        return $this->belongsTo(SupportServiceType::class, 'support_service_type', 'id');
    }

    public function supportEventName(): BelongsTo
    {
        return $this->belongsTo(SupportEventName::class, 'support_event_name', 'id');
    }

    public function sourceType(): BelongsTo
    {
        return $this->belongsTo(SupportSourceType::class, 'source_type', 'id');
    }

    public function recipientSpecialCategory(): BelongsTo
    {
        return $this->belongsTo(SupportRecipientSpecialCategory::class, 'recipient_special_category', 'id');
    }
}

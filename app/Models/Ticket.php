<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Ticket
 *
 * @property int $id
 * @property int $form_result_id
 * @property int $operator_id
 * @property string $status
 * @property Carbon $plan_end_date
 * @property Carbon $fact_end_date
 * @property string $archived
 * @property array $attachments
 * @property FormResult $formResult
 * @property User $operator
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Collection $comments
 * @property Invite $invite
 */
class Ticket extends Model
{
    use SoftDeletes, Sortable;

    const STATUS_DRAFT = 'R';

    const STATUS_CLIENT_WRITE = 'L';

    const STATUS_NEW = 'N';

    const STATUS_IN_WORK = 'W';

    const STATUS_FORMALIZATION = 'O';

    const STATUS_CANCEL_REQUEST = 'P';

    const STATUS_CANCELED = 'C';

    const STATUS_DONE = 'D';

    const STATUS_NEED_DATA = 'E';

    const STATUS_REJECT = 'F';

    const STATUS_INVITE = 'I';

    const MSP_STATUS_REGISTERED = 'registered';

    const MSP_STATUS_RENDERED = 'rendered';

    const MSP_STATUS_REJECT = 'rejected';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'form_result_id', 'operator_id', 'status', 'plan_end_date',
        'fact_end_date', 'archived', 'attachments', 'affiliate_id', 'create_from', 'application_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'plan_end_date',
        'fact_end_date',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'attachments' => 'array',
    ];

    /**
     * @var array
     */
    public $sortable = [
        'id',
        'status',
        'affiliate.name',
        'created_at',
        'updated_at',
        'plan_end_date',
        'fact_end_date',
    ];

    public const FROM_MSP = 1;

    public const FROM_DAMASK = 2;

    public const FROM_ONLINE = 3;

    public const CREATED_FROM = [
        self::FROM_MSP => 'МСП',
        self::FROM_DAMASK => 'Дамаск',
        self::FROM_ONLINE => 'Онлайн',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formResult()
    {
        return $this->belongsTo(FormResult::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'affiliate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        $user = auth()->user();
        $user->load('role');

        if ($user->role->name === 'user') {
            return $this->hasMany(Comment::class)
                ->orderBy('created_at')
                ->where('visibility', '=', Comment::VISIBILITY_ALL);
        } else {
            return $this->hasMany(Comment::class)->orderBy('created_at');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function checkList()
    {
        return $this->hasOne(TicketCheckList::class)->orderBy('created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function newComments()
    {
        return $this->hasMany(Comment::class)
            ->where('status', '=', Comment::STATUS_NEW)
            ->where('user_id', '<>', auth()->id());
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeUser($query)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user->role->name === 'operator') {
            return $query;
//            return $query->where('operator_id', '=', $user->id);
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invite()
    {
        return $this->hasOne(Invite::class);
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeWithService(Builder $query): Builder
    {
        return $query->with('formResult.form.service');
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeWindow(Builder $query): Builder
    {
        $window = session()->get('window');

        return $window
            ? $query->whereHas('formResult.form.service', function ($query) use ($window) {
                $query->where('services.window_id', '=', $window->id);
            })
            : $query;
    }

    /**
     * @param  Builder  $query
     * @param  string|null  $dateOpen
     * @param  string|null  $dateClose
     * @return Builder
     */
    public function scopeFilterByDate(Builder $query, ?string $dateOpen, ?string $dateClose): Builder
    {
        return $query
            ->when($dateOpen, function ($query, $dateOpen) {
                return $query->whereDate('created_at', '>=', $dateOpen);
            })
            ->when($dateClose, function ($query, $dateClose) {
                return $query->whereDate('created_at', '<=', $dateClose);
            });
    }

    /**
     * @return Collection
     */
    public static function getFilteredByRequestTicketsReport(Request $request): Collection
    {
        return Ticket::query()
            ->withService()
            ->window()
            ->filterByDate($request->date_open, $request->date_close)
            ->get();
    }

    /**
     * @return Collection
     */
    public static function getFilteredByRequestTicketsReportForRegularUser(Request $request): Collection
    {
        $user = auth()->user();

        return Ticket::query()
            ->withService()
            ->window()
            ->whereHas('formResult', function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            })
            ->filterByDate($request->date_open, $request->date_close)
            ->get();
    }

    /**
     * @return bool
     */
    public function checkStatusChange()
    {
        return isset($this->getChanges()['status']);
    }

    /*
     * @param Builder $query
     * @param string $direction
     * @return Builder
     */
    public function serviceSortable($query, $direction): Builder
    {
        return $query->join('form_results', 'form_results.id', '=', 'tickets.form_result_id')
            ->join('forms', 'form_results.form_id', '=', 'forms.id')
            ->join('services', 'forms.id', '=', 'services.form_id')
            ->orderBy('services.name', $direction)
            ->select('tickets.*');
    }

    /**
     * @param  Builder  $query
     * @param  string  $direction
     * @return Builder
     */
    public function operatorSortable($query, $direction): Builder
    {
        return $query->leftJoin('users', 'users.id', '=', 'operator_id')
            ->orderBy('users.firstname', $direction)
            ->orderBy('users.name', $direction)
            ->select('tickets.*');
    }

    /**
     * @param  Builder  $query
     * @param  string  $direction
     * @return Builder
     */
    public function windowSortable($query, $direction): Builder
    {
        return $query->join('form_results', 'form_results.id', '=', 'tickets.form_result_id')
            ->join('forms', 'form_results.form_id', '=', 'forms.id')
            ->join('services', 'forms.id', '=', 'services.form_id')
            ->leftJoin('windows', 'windows.id', '=', 'services.window_id')
            ->orderBy('windows.name', $direction)
            ->select('tickets.*');
    }
}

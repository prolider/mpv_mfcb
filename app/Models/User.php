<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request as FormRequest;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Role;

/**
 * Class User
 *
 * @method static Builder operator()
 */
class User extends \TCG\Voyager\Models\User
{
    use Notifiable, SoftDeletes;

    const SEX_MALE = 'M';

    const SEX_FEMALE = 'F';

    const ROLE_ID_ADMIN = 1;

    const ROLE_ID_OPERATOR = 2;

    const ROLE_ID_REGULAR = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'patronymic', 'sex', 'birthday', 'phone', 'inn', 'snils',
        'citizenship', 'passport_serial', 'passport_number', 'passport_date', 'passport_subdivision',
        'passport_organization', 'registration_place', 'company_inn', 'company_ogrn', 'company_ogrnip', 'company_name',
        'affiliate_id', 'bornplace', 'two_factor_code', 'two_factor_expires_at', 'is_confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'two_factor_expires_at' => 'datetime',
    ];

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeOperator($query)
    {
        $administratorRoles = Role::query()
            ->where('name', '=', 'operator')
            ->orWhere('name', '=', 'admin')
            ->get()
            ->pluck('id')
            ->toArray();

        if (count($administratorRoles) > 0) {
            return $query->whereIn('role_id', $administratorRoles);
        }

        return $query;
    }

    public function scopeUser($query)
    {
        $userRole = Role::where('name', '=', 'user')->first();

        if ($userRole) {
            return $query->where('role_id', $userRole->id);
        }

        return $query;
    }

    public function getNewMessagesCountAttribute()
    {
        return Ticket::whereHas('formResult', function (Builder $query) {
            return $query->where('user_id', '=', $this->id);
        })
            ->join('comments', 'comments.ticket_id', '=', 'tickets.id')
            ->where('comments.status', '=', Comment::STATUS_NEW)
            ->where('comments.user_id', '<>', $this->id)
            ->count();
    }

    public function getNewDiscussionsMessagesCountAttribute()
    {
        return Discussion::where(function ($query) {
            $query->where('discussions.user_owner_id', $this->id);
            $query->orWhere('discussions.user_recipient_id', $this->id);
        })
            ->join('discussion_messages', 'discussion_messages.discussion_group_id', '=', 'discussions.id')
            ->where('discussion_messages.status', '=', 0)
            ->where('discussion_messages.user_owner_id', '<>', $this->id)
            ->count();
    }

    public function getNotificationsCountAttribute()
    {
        return Notification::user()
            ->where('status', \App\Models\Notification::STATUS_NEW)
            ->whereHasMorph(
                'notificable',
                [\App\Models\Ticket::class, \App\Models\Discussion::class, \App\Models\DiscussionMessage::class]
            )
            ->count();
    }

    public function getInvitesCountAttribute()
    {
        return Invite::where('user_id', $this->id)
            ->where('invite_time', '>=', now())
            ->count();
    }

    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->name} {$this->patronymic}";
    }

    public function scopeUserFilter(Builder $query, FormRequest $request): Builder
    {
        if ($search = $request->get('name')) {
            $query->where(DB::raw('CONCAT_WS(" " ,inn, firstname)'), 'LIKE', '%'.$search.'%');
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'affiliate_id');
    }

    /**
     * @return bool
     */
    public function isNotRegularUser()
    {
        return $this->role_id !== User::ROLE_ID_REGULAR;
    }

    /**
     * @return mixed
     */
    public static function getAdmin()
    {
        return self::query()->whereRoleId(1)->get()->first();
    }

    /**
     * @return bool
     */
    public function isOperator()
    {
        return $this->role_id == User::ROLE_ID_OPERATOR;
    }

    public function generateTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = rand(100000, 999999);
        $this->two_factor_expires_at = now()->addMinutes(10);
        $this->save();
    }

    public function resetTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = null;
        $this->two_factor_expires_at = null;
        $this->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    protected static function newFactory()
    {
        return UserFactory::new();
    }
}

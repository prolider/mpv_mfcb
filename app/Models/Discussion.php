<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Discussion extends Model
{
    public const STATUS_CLOSE = 0;

    public const STATUS_OPEN = 1;

    protected $fillable = [
        'name',
        'text',
        'filenames',
        'is_alert',
        'user_owner_id',
        'user_recipient_id',
    ];

    protected $casts = [
        'is_alert' => 'bool',
    ];

    public function setIsAlert($is_alert)
    {
        $this->update(
            [
                'is_alert' => $is_alert,
            ]
        );
    }

    public function messages(): HasMany
    {
        return $this->hasMany(DiscussionMessage::class, 'discussion_group_id');
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_owner_id');
    }

    public function recipient(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_recipient_id');
    }

    public function getUnreadAttribute()
    {
        $user = auth()->user();

        return $this->messages()->getQuery()
                ->where('user_owner_id', '!=', $user->id)
                ->where('status', DiscussionMessage::STATUS_UNREAD)
                ->get();
    }

    public function getFilesUrlsAttribute()
    {
        $filesUrls = [];
        $files = json_decode($this->filenames);

        if ($files) {
            foreach ($files as $file) {
                $filesUrls[$file->name] = Storage::url($file->download_link);
            }
        }

        return $filesUrls;
    }
}

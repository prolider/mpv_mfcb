<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class FormReference
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class FormReference extends Model
{
    protected $fillable = [
        'id', 'code', 'name', 'description',
    ];

    /**
     * @return HasOne
     */
    public function supportFieldReference(): HasOne
    {
        return $this->hasOne(SupportFieldReference::class);
    }
}

<?php

namespace App\Http\Resources;

use App\Http\Resources\DiscussionMessages\AuthorResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscussionMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'userOwnerId' => $this->user_owner_id,
            'status' => $this->status,
            'filesUrls' => $this->filesUrls,
            'author' => AuthorResource::make($this->author),
            'created_at' => $this->created_at->format('d.m.Y, m:h'),
            'updated_at' => $this->updated_at->format('d.m.Y, m:h'),
        ];
    }
}

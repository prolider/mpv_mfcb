<?php

namespace App\Http\Resources;

use App\Services\MspService;
use App\Models\SupportFieldReference;
use Illuminate\Http\Resources\Json\JsonResource;

class SupportFormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fieldData = collect(json_decode($this->service->form->form_data)->steps);

        $fieldDataGrouped = $fieldData->mapWithKeys(function ($value, $key) {
            return [$value->name => collect($value->data)];
        });

        $deletedData = collect($this->service->form->deleted_fields);

        $fields = [];
        $sort = 0;

        $supportFieldReferences = SupportFieldReference::with('formReference')->get();
        $firstnameReference = $supportFieldReferences->where('code', SupportFieldReference::FIRSTNAME)->first();
        $nameReference = $supportFieldReferences->where('code', SupportFieldReference::NAME)->first();
        $patronymicReference = $supportFieldReferences->where('code', SupportFieldReference::PATRONYMIC)->first();
        $innReference = $supportFieldReferences->where('code', SupportFieldReference::INN)->first();
        $emailReference = $supportFieldReferences->where('code', SupportFieldReference::EMAIL)->first();
        $sexReference = $supportFieldReferences->where('code', SupportFieldReference::SEX)->first();
        $snilsReference = $supportFieldReferences->where('code', SupportFieldReference::SNILS)->first();
        $passportSerialReference = $supportFieldReferences->where('code', SupportFieldReference::PASSPORT_SERIAL)->first();
        $passportNubmerReference = $supportFieldReferences->where('code', SupportFieldReference::PASSPORT_NUMBER)->first();
        $passportDateReference = $supportFieldReferences->where('code', SupportFieldReference::PASSPORT_DATE)->first();
        $passportSubdivisionReference = $supportFieldReferences->where('code', SupportFieldReference::PASSPORT_SUBDIVISION)->first();
        $passportOrganizationsReference = $supportFieldReferences->where('code', SupportFieldReference::PASSPORT_ORGANIZATION)->first();
        $registrationPlaceReference = $supportFieldReferences->where('code', SupportFieldReference::REGISTRATION_PLACE)->first();
        $citizenshipReference = $supportFieldReferences->where('code', SupportFieldReference::CITIZENSHIP)->first();
        $phoneReference = $supportFieldReferences->where('code', SupportFieldReference::PHONE)->first();
        $birthdayReference = $supportFieldReferences->where('code', SupportFieldReference::BIRTHDAY)->first();
        $companyNameReference = $supportFieldReferences->where('code', SupportFieldReference::COMPANY_NAME)->first();
        $companyInnReference = $supportFieldReferences->where('code', SupportFieldReference::COMPANY_INN)->first();
        $companyOgrnReference = $supportFieldReferences->where('code', SupportFieldReference::COMPANY_OGRN)->first();
        $companyOgrnipReference = $supportFieldReferences->where('code', SupportFieldReference::COMPANY_OGRNIP)->first();

        $fields[] = collect([
            'code' => 'firstname',
            'name' => 'Фамилия',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'Фамилия',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $firstnameReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'name',
            'name' => 'Имя',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'Имя',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $nameReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'patronymic',
            'name' => 'Отчество',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'Отчество',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $patronymicReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'email',
            'name' => 'Почта',
            'category' => 'Персональные данные',
            'type' => 'email',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Почта',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $emailReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'inn',
            'name' => 'ИНН',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'ИНН',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $innReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'sex',
            'name' => 'Пол',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Пол',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $sexReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'snils',
            'name' => 'Снилс',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Снилс',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $snilsReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'passport_serial',
            'name' => 'Серия паспорта',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Серия паспорта',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $passportSerialReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'passport_number',
            'name' => 'Номер паспорта',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Номер паспорта',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $passportNubmerReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'passport_date',
            'name' => 'Дата выдачи паспорта',
            'category' => 'Персональные данные',
            'type' => 'date',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Дата выдачи паспорта',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $passportDateReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'passport_subdivision',
            'name' => 'Код подразделения паспорт',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Код подразделения паспорт',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $passportSubdivisionReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'passport_organization',
            'name' => 'Кем выдан паспорт',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Кем выдан паспорт',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $passportOrganizationsReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'registration_place',
            'name' => 'Адрес регистрации',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Адрес регистрации',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $registrationPlaceReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'citizenship',
            'name' => 'Гражданство',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Гражданство',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $citizenshipReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'phone',
            'name' => 'Номер телефона',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => null,
            'validateDescription' => 'Номер телефона',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $phoneReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'birthday',
            'name' => 'День рождения',
            'category' => 'Персональные данные',
            'type' => 'date',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'День рождения',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $birthdayReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'company_name',
            'name' => 'Название организации',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'Название организации',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $companyNameReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'company_inn',
            'name' => 'ИНН организации',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => true,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'ИНН организации',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $companyInnReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'company_ogrn',
            'name' => 'ОГРН',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => false,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'ОГРН',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $companyOgrnReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $fields[] = collect([
            'code' => 'company_ogrnip',
            'name' => 'ОГРНИП',
            'category' => 'Персональные данные',
            'type' => 'string',
            'required' => false,
            'multiple' => false,
            'values' => [],
            'validate' => '',
            'validateDescription' => 'ОГРНИП',
            'description' => '',
            'defaultValue' => null,
            'substitution' => $companyOgrnipReference->reference_code,
            'sort' => ++$sort,
            'documents' => [],
            'is_active' => true,
        ]);

        $deletedData = $deletedData->mapWithKeys(function ($value, $key) use (&$fields) {
            $fieldType = MspService::getFieldType($value);

            return $fields[] = collect([
                'code' => $value['name'] ?? '',
                'name' => $value['title'] ?? '',
                'type' => $fieldType['type'],
                'is_active' => false,
            ]);
        });

        $this->service->form->deleted_fields = null;
        $this->service->form->save();

        $fieldDataGrouped = $fieldDataGrouped->mapWithKeys(function ($value, $key) use (&$sort, &$fields) {
            return $value->map(function ($value) use ($key, &$sort, &$fields) {
                return collect($value->cols)->map(function ($value) use ($key, &$sort, &$fields) {
                    return collect($value->data)->map(function ($value) use ($key, &$sort, &$fields) {
                        $fieldType = MspService::getFieldType($value);

                        return $fields[] = collect([
                            'code' => $value->name ?? '',
                            'name' => $value->title ?? '',
                            'category' => $key,
                            'type' => $fieldType['type'],
                            'required' => $value->required ?? false,
                            'multiple' => $fieldType['multiple'] ?? false,
                            'values' => $fieldType['values'] ?? [],
                            'validate' => null,
                            'validateDescription' => $value->placeholder ?? '',
                            'description' => $value->hint ?? '',
                            'defaultValue' => null,
                            'substitution' => $value->code ?? '',
                            'sort' => ++$sort,
                            'documents' => [],
                            'is_active' => true,
                        ]);
                    });
                });
            });
        });

        return [
            'id' => $this->id,
            'idForm' => 0,
            'fields' => $fields,
        ];
    }
}

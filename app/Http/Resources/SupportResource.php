<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fieldData = collect(json_decode($this->service->form->form_data)->steps);

        if ($fieldData) {
            $fieldCategories = $fieldData
                ->map(function ($value) {
                    return $value->name;
                })->prepend('Персональные данные');
        }

        return [
            'id' => $this->id,
            'idForm' => 0,
            'active' => (bool) $this->active,
            'name' => $this->name,
            'preview' => $this->preview,
            'shortDescription' => $this->short_description,
            'startDate' => $this->start_date->toAtomString(),
            'endDate' => $this->end_date->toAtomString(),
            'supportType' => $this->supportType->code,
            'supportCount' => $this->support_count,
            'requirements' => [
                'opf' => implode(',', json_decode($this->opf, true) ?? []),
                'okwed' => $this->okwed,
                'notOkwed' => $this->not_okwed,
                'ukep' => (bool) $this->ukep,
                'scoring' => (bool) $this->scoring,
                'minEmplAmount' => $this->min_empl_amount,
                'maxEmplAmount' => $this->max_empl_amount,
                'minExistTerm' => $this->min_exist_term,
                'okato' => $this->okato ?? 0,
            ],
            'supportAmountFrom' => $this->support_amount_from,
            'supportAmountTill' => $this->support_amount_till,
            'fullDescription' => $this->full_description,
            'documents' => DocumentResourse::collection($this->service->documents),
            'fieldCategories' => $fieldCategories,
            'serviceName' => $this->supportServiceName?->code,
            'serviceType' => $this->supportServiceType?->code,
            'organizerContacts' => [
                'phone' => $this->support_phone,
                'email' => $this->support_email,
            ],
            'sourceType' => $this->sourceType?->code,
            'supportEventName' => $this->supportEventName->code,
            'complexServiceInfo' => $this->complex_service_info,
            'reviewDeclineReason' => $this->review_decline_reason,
            'offerDeclineReason' => $this->offer_decline_reason,
            'supportAmountPercent' => $this->support_amount_percent,
            'supportCost' => [
                'isPaid' => (bool) $this->support_is_paid,
                'minCost' => $this->support_min_cost,
                'maxCost' => $this->support_max_cost,
            ],
            'provisionWaitPeriod' => $this->provision_wait_period,
            'provisionStartTime' => $this->provision_start_time,
            'loanTerms' => [
                'min' => $this->loan_terms_min,
                'max' => $this->loan_terms_max,
            ],
            'loanPercent' => [
                'min' => $this->loan_percent_min,
                'max' => $this->loan_percent_max,
            ],
            'recipientCategory' => array_values(json_decode($this->recipient_category, true)) ?: ['other'],
            'recipientSpecialCategory' => $this->recipientSpecialCategory?->code,
            'additionalRequirements' => $this->additional_requirements,
            'preferentialRecipients' => $this->preferential_recipients,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Exception;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = explode('.', $this->file);
        $file = '/storage/'.$this->file;

        try {
            $filePath = 'app/public/'.$this->file;
            $fileSize = filesize(storage_path($filePath));
        } catch (Exception $e) {
            $fileSize = 0;
        }

        return [
            'link' => url($file),
            'type' => array_pop($name),
            'size' => $fileSize,
            'name' => $this->name,
            'description' => $this->name,
        ];
    }
}

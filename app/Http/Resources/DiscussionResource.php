<?php

namespace App\Http\Resources;

use App\Http\Resources\Discussion\OwnerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscussionResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'discussion';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'text' => $this->text,
            'isAlert' => $this->is_alert,
            'filesUrls' => $this->filesUrls,
            'created_at' => $this->created_at->format('d.m.Y, m:h'),
            'owner' => OwnerResource::make($this->owner),
            'messages' => DiscussionMessageResource::collection($this->messages),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'name' => $this->name,
            'patronymic' => $this->patronymic,
            'sex' => $this->sex,
            'birthday' => $this->birthday,
            'phone' => $this->phone,
            'email' => $this->email,
            'bornplace' => $this->bornplace,
            'passport_serial' => $this->passport_serial,
            'passport_number' => $this->passport_number,
            'passport_date' => $this->passport_date,
            'passport_subdivision' => $this->passport_subdivision,
            'passport_organization' => $this->passport_organization,
            'registration_place' => $this->registration_place,
            'inn' => $this->inn,
            'snils' => $this->snils,
            'citizenship' => $this->citizenship,
            'company_inn' => $this->company_inn,
            'company_ogrn' => $this->company_ogrn,
            'company_kpp' => $this->company_kpp,
            'organization_date' => $this->organization_date,
            'company_name' => $this->company_name,
            'company_adress' => $this->company_adress,
            'company_ceo' => $this->company_ceo,
            'company_ceo_fio' => $this->company_ceo_fio,
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var \App\Services\SearchService
     */
    private SearchService $service;

    public function __construct(SearchService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $search = $request->get('search', '');

        $services = $this->service->searchByService($search) ?? collect();

        return view('search.index', compact('search', 'services'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Affiliate;
use App\Models\Comment;
use App\Exports\TicketsExport;
use App\Http\Resources\AffiliateResourse;
use App\Http\Resources\WindowResource;
use App\Services\TicketService;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Window;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TicketController extends Controller
{
    /**
     * @var TicketService
     */
    protected $ticketService;

    /**
     * TicketController constructor.
     *
     * @param  TicketService  $ticketService
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @param  Request  $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tickets = $this->ticketService->getTicketList($request);

        if ($tickets instanceof Paginator) {
            return view('tickets.index', compact('tickets'));
        }

        $tickets['ticketFrom'] = Ticket::CREATED_FROM;

        return view('tickets.operator-lk', $tickets);
    }

    /**
     * @param  Request  $request
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive(Request $request)
    {
        $tickets = $this->ticketService->getTicketList($request, true);

        if ($tickets instanceof Paginator) {
            return view('tickets.index', compact('tickets'));
        }

        $tickets['ticketFrom'] = Ticket::CREATED_FROM;

        return view('tickets.operator-lk', $tickets);
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user->isNotRegularUser()) {
            $affiliates = AffiliateResourse::collection(Affiliate::all());
            $windows = WindowResource::collection(Window::all());
            $tickets = Ticket::getFilteredByRequestTicketsReport($request);

            return view('reports.index', compact('tickets', 'affiliates', 'windows'));
        }

        $tickets = Ticket::getFilteredByRequestTicketsReportForRegularUser($request);

        return view('reports.index', compact('tickets'));
    }

    public function ticketsExport(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $tickets = $user->isNotRegularUser()
            ? Ticket::getFilteredByRequestTicketsReport($request)
            : Ticket::getFilteredByRequestTicketsReportForRegularUser($request);

        return Excel::download(new TicketsExport(compact('tickets')), 'tickets.xlsx');
    }

    /**
     * @param  Ticket  $ticket
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('show-ticket', $ticket->formResult->user->id);

        $ticket->load(['formResult.form.service.documents', 'comments.user', 'checkList']);

        /** @var User $user */
        $user = auth()->user();

        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $affiliates = AffiliateResourse::collection(Affiliate::all());
            $windows = WindowResource::collection(Window::all());

            $response = view('tickets.operator-lk', [
                'tickets' => [],
                'affiliates' => $affiliates,
                'windows' => $windows,
                'ticketFrom' => Ticket::CREATED_FROM,
                'colorIndicationCounts' => [],
            ]);
        } else {
            $response = view('tickets.detail', compact('ticket'));
        }

        Comment::query()
            ->where('ticket_id', '=', $ticket->id)
            ->where('user_id', '<>', auth()->id())
            ->update([
                'status' => Comment::STATUS_READED,
            ]);

        return $response;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function window(Request $request)
    {
        if ($request->has('window_id')) {
            $window = Window::query()
                ->where('id', '=', $request->input('window_id'))
                ->first();

            if (! is_null($window)) {
                session()->put('window', $window);
            }
        }

        return response()->json([]);
    }
}

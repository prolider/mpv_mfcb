<?php

namespace App\Http\Controllers;

use App\Events\SendConfirmLink;
use App\Models\Invite;
use App\Models\Notification;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('home');
    }

    public function faq()
    {
        return view('faq.index');
    }

    public function policyFile()
    {
        if (setting('site.policy_file') !== null) {
            $policyFile = json_decode(setting('site.policy_file'))[0];
            $policyOriginalName = $policyFile->original_name;

            return  Storage::disk(config('voyager.storage.disk'))->download($policyFile->download_link, $policyOriginalName);
        }

        return response()->redirectTo(route('index'));
    }

    public function processingPersonalDataFile()
    {
        if (setting('site.processing_personal_data_file') !== null) {
            $processingPersonalDataFile = json_decode(setting('site.processing_personal_data_file'))[0];
            $processingPersonalDataOriginalName = $processingPersonalDataFile->original_name;

            return  Storage::disk(config('voyager.storage.disk'))->download($processingPersonalDataFile->download_link, $processingPersonalDataOriginalName);
        }

        return response()->redirectTo(route('index'));
    }

    public function userAgreementFile()
    {
        if (setting('site.user_agreement_file') !== null) {
            $userAgreementFile = json_decode(setting('site.user_agreement_file'))[0];
            $userAgreementFileOriginalName = $userAgreementFile->original_name;

            return  Storage::disk(config('voyager.storage.disk'))->download($userAgreementFile->download_link, $userAgreementFileOriginalName);
        }

        return response()->redirectTo(route('index'));
    }

    public function profile()
    {
        $user = auth()->user();

        return view('profile.index', compact('user'));
    }

    public function mesages()
    {
        $tickets = Ticket::query()
            ->whereHas('formResult', function (Builder $query) {
                return $query->where('user_id', '=', auth()->id());
            })
            ->whereHas('newComments')
            ->with(['formResult.form.service'])
            ->withCount('newComments')
            ->get();

        return view('mesages.index', compact('tickets'));
    }

    public function notices()
    {
        $notifications = Notification::user()
            ->with('notificable')
            ->whereHasMorph('notificable', [\App\Models\Ticket::class, \App\Models\Discussion::class, \App\Models\DiscussionMessage::class])
            ->orderBy('created_at', 'desc')
            ->get();

        return view('notices.index', compact('notifications'));
    }

    public function invites()
    {
        $invites = Invite::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();

        return view('invites.index', compact('invites'));
    }

    /**
     * Обновление профиля
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        foreach ($user->getFillable() as $field) {
            if ($request->has($field)) {
                $user->{$field} = $request->input($field);
            }
        }

        $result = $user->save();

        return redirect()
            ->back()
            ->with('profile_update', $result ? 'success' : 'failure');
    }

    public function confirmation()
    {
        $user = Auth::user();

        //event(new SendConfirmLink($user));

        return redirect()->route('confirm');
    }

    public function confirm()
    {
        $user = Auth::user();

        event(new SendConfirmLink($user));

        return view('auth.confirm');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\StoreOrderDraftRequest;
use App\Http\Resources\ServiceResourse;
use App\Models\Order;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Services\OrderCreateService;
use App\Services\OrderUpdateService;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $ordersQuery = Order::query()
            ->with('service', 'user', 'operator', 'affiliate')
            ->whereDate('created_at', '=', today());

        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $orders = $ordersQuery->paginate();

            return view('orders.operator-lk', compact('orders', 'user'));
        }

        $orders = $ordersQuery->whereHas('user', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->paginate();

        return view('orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $service = (new ServiceResourse($order->service))->toJson();
        $affiliateServices = $order->service->affiliateServices()
            ->where('affiliate_id', $order->affiliate->id)
            ->get(['id', 'damask_service_code as label'][0]);

        $services = ServiceResourse::collection(Service::orderBy('name')->get());

        $categories = ServiceCategory::orderBy('name')->get(['id', 'name as label']);
        $category = $order->service->category()->get(['id', 'name as label']);

        return view(
            'orders.show',
            compact('order', 'service', 'services', 'categories', 'category', 'affiliateServices')
        );
    }

    public function create(Request $request)
    {
        $services = ServiceResourse::collection(Service::orderBy('name')->get());

        $categories = ServiceCategory::orderBy('name')->get(['id', 'name as label']);

        return view('orders.create', compact('services', 'categories'));
    }

    public function store(CreateOrderRequest $request)
    {
        return OrderCreateService::saveOrderWithStatus($request, 'CREATE');
    }

    public function storeDraft(StoreOrderDraftRequest $request)
    {
        $property = OrderCreateService::saveOrderWithStatus($request, 'DRAFT');

        return redirect()->route('order.show', $property->id);
    }

    public function update(CreateOrderRequest $request, Order $order)
    {
        $property = OrderUpdateService::updateOrderWithStatus($request, 'CREATE', $order);

        return redirect()->route('ticket.show', $property->id);
    }

    public function updateDraft(StoreOrderDraftRequest $request, Order $order)
    {
        $property = OrderUpdateService::updateOrderWithStatus($request, 'DRAFT', $order);

        return redirect()->route('order.show', $property->id);
    }
}

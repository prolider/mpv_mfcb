<?php

namespace App\Http\Controllers\Api;

use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Discussion\MessageRequest;
use App\Http\Resources\DiscussionMessageResource;
use App\Services\FileService;
use Illuminate\Http\Request;

class DiscussionController extends Controller
{
    /**
     * @var FileService
     */
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function read(DiscussionMessage $message)
    {
        $message->setStatus(DiscussionMessage::STATUS_READ);

        return response()->json(['message' => DiscussionMessageResource::make($message)]);
    }

    public function changeIsAlert(Discussion $discussion)
    {
        $discussion->setIsAlert(Discussion::STATUS_CLOSE);

        return response('OK', 200);
    }

    public function paginatedIndex(Request $request)
    {
        $user = auth()->user();

        $discussions = Discussion::withCount('messages');

        if (in_array($user->role->name, ['user', 'invited'])) {
            $discussions = $discussions->where('user_owner_id', $user->id);
        } else {
            $discussions = $discussions->where('user_recipient_id', $user->id);
        }

        $discussions = $discussions->paginate(
            $request->get('per_page', 50),
        );

        return response()->json($discussions);
    }

    public function sendMessage(MessageRequest $request, Discussion $discussion)
    {
        $validatedData = $request->validated();

        $files = $validatedData['files'] ?? null;
        $loadedFiles = [];

        if ($files) {
            $filenames = $this->service->saveFiles('discussions', $files);

            foreach ($filenames as $filename) {
                $loadedFiles[] = [
                    'name' => $filename,
                    'original_name' => $filename,
                    'download_link' => 'discussions/'.$filename,
                ];
            }
        }

        $message = $discussion->messages()->create([
            'text' => $validatedData['text'] ?? null,
            'filenames' => $loadedFiles,
            'user_owner_id' => auth()->user()->id,
        ]);

        return response()->json(['message' => DiscussionMessageResource::make($message)]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Affiliate;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Http\Resources\AffiliateResourse;
use App\Models\Invite;
use App\Models\Notification;
use App\Services\TicketService;
use App\Models\Ticket;
use App\Models\TicketCheckList;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * @var TicketService
     */
    protected $ticketService;

    /**
     * TicketController constructor.
     *
     * @param  TicketService  $ticketService
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $tickets = $this->ticketService->getApiTicketList($request);

        return response()->json(compact('tickets'));
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function archive(Request $request)
    {
        $tickets = $this->ticketService->getApiTicketList($request, true);

        return response()->json(compact('tickets'));
    }

    /**
     * @param  Ticket  $ticket
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Ticket $ticket)
    {
        $ticket->load([
            'formResult.form.service.documents', 'comments.user', 'formResult.user',
            'operator', 'invite', 'checkList', 'logs.operator',
        ]);

        $affiliates = AffiliateResourse::collection(Affiliate::all());

        $response = response()->json(compact('ticket', 'affiliates'));

        Comment::query()
            ->where('ticket_id', '=', $ticket->id)
            ->where('user_id', '<>', auth()->id())
            ->update([
                'status' => Comment::STATUS_READED,
            ]);

        return $response;
    }

    public function createComments(Request $request)
    {
        foreach ($request->get('tickets', []) as $ticket) {
            $ticket = Ticket::find($ticket);

            if ($ticket) {
                $this->comment($ticket, $request);
            }
        }

        $token = csrf_token();

        return response()->json(compact('token'));
    }

    /**
     * Создание комментария пользователем
     *
     * @param  Ticket  $ticket
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Ticket $ticket, Request $request)
    {
        $comment = [
            'ticket_id' => $ticket->id,
            'user_id' => auth()->id(),
            'status' => Comment::STATUS_NEW,
            'visibility' => $request->input('visibility', Comment::VISIBILITY_ALL),
            'text' => strip_tags(
                $request->input('text'), '<p><br>'),
        ];

        if ($request->hasFile('attachment')) {
            $files = $request->file('attachment');
            $attachments = [];

            foreach ($files as $file) {
                $filePath = "comments/ticket/{$ticket->id}/";
                $path = $file->store($filePath, ['disk' => 'public']);
                $attachments[] = [
                    'path' => $path,
                    'name' => $file->getClientOriginalName(),
                ];
            }

            $comment['attachment'] = $attachments;
        }
        Comment::create($comment);

        $ticket->load(['formResult.form.service', 'comments.user', 'logs.operator']);

        $token = csrf_token();

        return response()->json(compact('ticket', 'token'));
    }

    public function changeStatuses(Request $request)
    {
        foreach ($request->get('tickets', []) as $ticket) {
            $ticket = Ticket::find($ticket);

            if ($ticket) {
                $this->changeStatus($ticket, $request);
            }
        }

        $token = csrf_token();

        return response()->json(compact('token'));
    }

    public function changeStatus(Ticket $ticket, Request $request)
    {
        if ($request->input('status') === Ticket::STATUS_INVITE) {
            if (! $request->input('invite.time') || ! $request->input('invite.window')) {
                return response()->json(['error' => 'Приглашение не заполнено.'], 422);
            }

            /** @var Invite $invite */
            $invite = Invite::query()
                ->where('ticket_id', '=', $ticket->id)
                ->first();

            if (is_null($invite)) {
                Invite::create([
                    'window_id' => $request->input('invite.window'),
                    'user_id' => $ticket->formResult->user_id,
                    'operator_id' => auth()->id(),
                    'invite_time' => Carbon::createFromFormat('Y-m-d H:i', $request->input('invite.time')),
                    'ticket_id' => $ticket->id,
                ]);
            } else {
                $invite->invite_time = Carbon::createFromFormat('Y-m-d H:i', $request->input('invite.time'));
                $invite->window_id = $request->input('invite.window');
                $invite->save();
            }
        }

        if ($request->input('operator_id')) {
            $ticket->operator_id = $request->input('operator_id');
        }

        if ($request->input('status')) {
            if ($request->status === 'D') {
                $ticket->fact_end_date = Carbon::now();
            }

            $ticket->status = $request->input('status');
        }

        if ($request->input('affiliate_id')) {
            $ticket->affiliate_id = (int) $request->input('affiliate_id');
        }

        $ticket->save();
        $ticket->load([
            'formResult.form.service', 'comments.user', 'formResult.user', 'operator',
            'invite', 'checkList', 'logs.operator',
        ]);
        $token = csrf_token();

        if ($ticket->status === Ticket::STATUS_INVITE) {
            // Оповещение для пользователя
            Notification::create([
                'name' => 'Изменен статус заявки',
                'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"".__('ticket.status.'.$ticket->status)."\". Время приглашения: {$ticket->invite->invite_time->format('d.m.Y H:i')}",
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'status' => Notification::STATUS_NEW,
                'user_id' => $ticket->formResult->user_id,
            ]);

            // Оповещение для оператора
            User::operator()->get()->each(function (User $user) use ($ticket) {
                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"".__('ticket.status.'.$ticket->status)."\". Время приглашения: {$ticket->invite->invite_time->format('d.m.Y H:i')}",
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $user->id,
                ]);
            });
        } else {
            // Оповещение для пользователя
            Notification::create([
                'name' => 'Изменен статус заявки',
                'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"".__('ticket.status.'.$ticket->status).'"',
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'status' => Notification::STATUS_NEW,
                'user_id' => $ticket->formResult->user_id,
            ]);

            // Оповещение для оператора
            User::operator()->get()->each(function (User $user) use ($ticket) {
                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"".__('ticket.status.'.$ticket->status).'"',
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $user->id,
                ]);
            });
        }

        if ($request->ajax()) {
            return response()->json(compact('ticket', 'token'));
        } else {
            return redirect()->back();
        }
    }

    public function updateCheckList(Request $request)
    {
        TicketCheckList::where('id', $request->get('ticket_check_list'))->update(['check_list' => $request->get('check_list')]);
    }
}

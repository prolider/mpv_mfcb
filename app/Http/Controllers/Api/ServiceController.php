<?php

namespace App\Http\Controllers\Api;

use App\Models\AffiliateService;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceCategorySelectResource;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function store(Request $request)
    {
        foreach ($request->get('affiliate_services', []) as $id => $value) {
            AffiliateService::find($id)
                ->update(['damask_service_code' => $value]);
        }

        return response('ok', 200);
    }

    public function getServiceCategories(Request $request)
    {
        $serviceCategories = ServiceCategory::where('name', 'LIKE', '%'.$request->get('search', '').'%')->get();

        return response()->json(['results' => ServiceCategorySelectResource::collection($serviceCategories)]);
    }
}

<?php

namespace App\Http\Controllers\Api\MSP;

use App\Http\Controllers\Controller;
use App\Http\Resources\SupportFormResource;
use App\Http\Resources\SupportResource;
use App\Services\MspService;
use App\Models\Support;
use App\Models\Ticket;
use Illuminate\Http\Request;

class MspController extends Controller
{
    protected $mspTokenService;

    /**
     * MspController constructor.
     */
    public function __construct(MspService $mspTokenService)
    {
        $this->mspTokenService = $mspTokenService;
    }

    public function generateToken(Request $request)
    {
        $result = $this->mspTokenService->validateCredentialsFromMSP($request);

        return response()->json($result);
    }

    public function supportDescriptions(Request $request)
    {
        $errors = $this->mspTokenService->checkAuthentication($request);

        if (! empty($errors)) {
            return response()->json($errors);
        }

        $supports = Support::all();

        return response()->json(SupportResource::collection($supports));
    }

    public function supportTicketFields(Request $request)
    {
        $errors = $this->mspTokenService->checkAuthentication($request);

        if (! empty($errors)) {
            return response()->json($errors);
        }

        if ($request->input('id')) {
            $support = Support::where('id', $request->input('id'))->get();

            return response()->json(SupportFormResource::collection($support));
        }

        $supports = Support::all();

        return response()->json(SupportFormResource::collection($supports));
    }

    public function createTicket(Request $request)
    {
        $errors = $this->mspTokenService->checkAuthentication($request);

        if (! empty($errors)) {
            return response()->json($errors);
        }

        $status = $this->mspTokenService->createTicket($request);

        return response()->json($status);
    }

    public function createdTicketStatus(Request $request)
    {
        $errors = $this->mspTokenService->checkAuthentication($request);

        if (! empty($errors)) {
            return response()->json($errors);
        }

        $ticket = Ticket::find($request->input('id'));
        $status = $this->mspTokenService->getTicketStatus($ticket, $request);

        return response()->json($status);
    }
}

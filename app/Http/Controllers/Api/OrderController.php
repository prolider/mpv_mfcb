<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceResourse;
use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();

        $orderQuery = Order::query()
            ->with('service', 'user', 'operator', 'affiliate');

        if (
            $request->has('created_at')
            && $date = strtotime($request->input('created_at'))
        ) {
            $orderQuery->whereDate('created_at', '=', date('Y-m-d', $date));
        }

        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $orders = $orderQuery->paginate();

            return response()->json(compact('orders'));
        }
    }

    public function changeStatus(Order $order, Request $request)
    {
        $operator = auth()->user();

        if (
            $order->status === Order::STATUSES['DRAFT']
            && $request->input('status') === Order::STATUSES['IN_WORK']
        ) {
            $order->update([
                'status' => Order::STATUSES['IN_WORK'],
                'operator_id' => $operator->id,
            ]);
        }

        $token = csrf_token();

        if ($request->ajax()) {
            return response()->json(compact('order', 'token'));
        } else {
            return redirect()->back();
        }
    }

    public function filteredUsers(Request $request)
    {
        $users = User::user()
            ->userFilter($request)
            ->select('id', DB::raw('CONCAT_WS(" " ,inn, firstname) as label'))
            ->limit(10)
            ->get();

        return response()->json($users);
    }

    public function selectUser(Request $request)
    {
        $user = new UserResource(User::find($request->get('user_id')));

        return response()->json($user);
    }

    public function selectCategory(Request $request)
    {
        if ($id = $request->get('category_id')) {
            $category = ServiceCategory::with('services')->find($id);
            $services = ServiceResourse::collection($category->services);
        } else {
            $services = ServiceResourse::collection(Service::all());
        }

        return response()->json($services);
    }

    public function selectService(Request $request)
    {
        if ($id = $request->get('service_id')) {
            $category = Service::with('category')
                ->find($id)
                ->category()
                ->get(['id', 'name as label']);
        } else {
            $category = null;
        }

        return response()->json($category);
    }

    public function updateService(Order $order, Request $request)
    {
        if ($request->has('service_id')) {
            $order->update(['service_id' => $request->service_id]);
        }
    }
}

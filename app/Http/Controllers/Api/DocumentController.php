<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Добавление документа
     *
     * @param  Request  $request
     * @param  Service  $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Service $service, Request $request)
    {
        if ($request->hasFile('file')) {
            $filePath = $request->file('file')
                ->store('service/documents', ['disk' => 'public']);

            $service_document = ServiceDocument::create([
                'service_id' => $service->id,
                'name' => $request->input('name'),
                'file' => $filePath,
                'sort' => $request->input('sort'),
            ]);

            $service->load('documents');

            $table = view('vendor.voyager.services.partials.documents-table', [
                'documents' => $service->documents,
            ])->render();

            return response()->json(compact('service_document', 'table'));
        }

        return response()->json(['error' => 'Отсутствует файл'], 422);
    }

    /**
     * Обновление документа
     *
     * @param  Service  $service
     * @param  ServiceDocument  $serviceDocument
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Service $service, ServiceDocument $serviceDocument, Request $request)
    {
        if ($request->hasFile('file')) {
            $serviceDocument->file = $request->file('file')
                ->store('service/documents', ['disk' => 'public']);
        }

        $serviceDocument->name = $request->input('name');
        $serviceDocument->sort = $request->input('sort');

        $serviceDocument->save();

        $service->load('documents');

        $table = view('vendor.voyager.services.partials.documents-table', [
            'documents' => $service->documents,
        ])->render();

        return response()->json(['service_document' => $serviceDocument, 'table' => $table]);
    }

    /**
     * Удаление документа
     *
     * @param  Service  $service
     * @param  ServiceDocument  $serviceDocument
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function delete(Service $service, ServiceDocument $serviceDocument)
    {
        if (Storage::disk('public')->delete($serviceDocument->file)) {
            $serviceDocument->delete();
        }

        return response()->json();
    }
}

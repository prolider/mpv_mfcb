<?php

namespace App\Http\Controllers\Api;

use App\Models\Affiliate;
use App\Http\Controllers\Controller;
use App\Http\Resources\OperatorResourse;
use App\Http\Resources\WindowResource;
use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    public function selectOperators(Request $request)
    {
        $operators = OperatorResourse::collection(Affiliate::find($request->get('affiliate_id'))->users);

        return response()->json($operators);
    }

    public function selectWindows(Request $request)
    {
        $windows = WindowResource::collection(Affiliate::find($request->get('affiliate_id'))->windows);

        return response()->json($windows);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OperatorResourse;
use App\Models\User;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query();

        if ($request->input('role') === 'operator') {
            $operatorRoles = Role::query()
                ->where('name', '=', 'operator')
                ->orWhere('name', '=', 'admin')
                ->get();

            if ($operatorRoles->count() > 0) {
                $query->whereIn('role_id', $operatorRoles->pluck('id')->toArray());
            } else {
                return response()->json(['users' => []]);
            }
        }

        $users = OperatorResourse::collection($query->get());

        return response()->json(compact('users'));
    }
}

<?php

namespace App\Http\Controllers\Api\Damask;

use App\Models\Affiliate;
use App\Models\AffiliateService;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function create(Request $request)
    {
        $operator = User::operator()->where('role_id', 1)->first();

        $coupon = $request->get('code');

        $damaskServiceCode = $request->get('operations')[0]['id'];
        $affiliate = Affiliate::where('code', $request->get('affiliate'))->first();

        $service = AffiliateService::where([
            'damask_service_code' => $damaskServiceCode,
            'affiliate_id' => $affiliate->id,
        ])->first()->service;

        $order = Order::create([
            'user_id' => $operator->id,
            'service_id' => $service->id,
            'affiliate_id' => $affiliate->id,
            'coupon' => $coupon,
            'is_damask' => true,
            'operator_id' => $operator->id,
            'status' => Order::STATUSES['DRAFT'],
        ]);

        return $order;
    }
}

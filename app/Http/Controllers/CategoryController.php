<?php

namespace App\Http\Controllers;

use App\Models\ServiceCategory;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ServiceCategory $serviceCategory)
    {
        $serviceCategory->load('services');
        $categories = ServiceCategory::all();

        return view('categories.detail', compact('serviceCategory', 'categories'));
    }

    /**
     * @param  Category  $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category()
    {
        $categories = ServiceCategory::all();

        return view('categories.index', compact('categories'));
    }
}

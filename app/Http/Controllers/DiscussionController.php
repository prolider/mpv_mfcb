<?php

namespace App\Http\Controllers;

use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Http\Requests\Discussion\AlertedRequest;
use App\Http\Requests\Discussion\DiscussionRequest;
use App\Http\Resources\Discussion\UserResource;
use App\Http\Resources\DiscussionResource;
use App\Services\FileService;
use Illuminate\Http\Request;

class DiscussionController extends Controller
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $user = auth()->user();

        $discussions = Discussion::withCount(['messages' => function ($query) use ($user) {
            return $query->where('user_owner_id', '!=', $user->id)
                    ->where('status', DiscussionMessage::STATUS_UNREAD);
        }]);

        if ($user->role->name == 'user') {
            $discussions = $discussions->where('user_owner_id', $user->id)->orderBy('created_at', 'desc');
        } else {
            $discussions = $discussions->where('user_recipient_id', $user->id)->orderBy('created_at', 'desc');
        }

        $discussions = $discussions->paginate(
            $request->get('per_page', 50)
        );

        return view('discussions.index', compact('discussions'));
    }

    public function create(AlertedRequest $request)
    {
        $data = $request->validated();
        $head = $data['name'] ?? '';
        $isAlert = $data['is_alert'] ?? false;

        return view('discussions.create', compact('head', 'isAlert'));
    }

    public function store(DiscussionRequest $request)
    {
        $data = $request->validated();

        $files = $data['files'] ?? null;
        $loadedFiles = [];

        if ($files) {
            $filenames = $this->service->saveFiles('discussions', $files);

            foreach ($filenames as $filename) {
                $loadedFiles[] = [
                    'name' => $filename,
                    'original_name' => $filename,
                    'download_link' => 'discussions/'.$filename,
                ];
            }
        }

        $data['filenames'] = json_encode($loadedFiles);
        $data['is_alert'] = isset($data['is_alert']);

        $discussion = Discussion::create($data);

        return redirect()->route('discussions.show', $discussion);
    }

    public function show(Discussion $discussion)
    {
        $discussion = DiscussionResource::make($discussion);
        $user = UserResource::make(auth()->user());

        return view('discussions.operator-lk', compact('discussion', 'user'));
    }
}

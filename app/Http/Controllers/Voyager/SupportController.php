<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Support;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class SupportController extends VoyagerBaseController
{
    public function copy(Support $support)
    {
        $supportReplicate = $support->replicate(['name']);
        $supportReplicate->name = "{$support->name} 'Koпия'";

        $supportServiceReplicate = $support->service->replicate();
        $supportServiceReplicate->save();

        $supportReplicate->service()->associate($supportServiceReplicate);

        $supportReplicate->save();

        return back();
    }
}

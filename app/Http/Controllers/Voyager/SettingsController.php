<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerSettingsController;

class SettingsController extends VoyagerSettingsController
{
    protected const SUBSTITUTION_FILES_KEYS = [
        "site_policy_file", 
        "site_processing_personal_data_file", 
        "site_user_agreement_file"
    ];
    private $old_files_paths = [];

    public function __construct() {
        $settings = Voyager::model('Setting')->all()->mapWithKeys(function ($item, $key) {
            return [str_replace('.', '_', $item['key']) => $item['value']];
        });

        foreach($settings as $field => $fileValues) {
            $value = json_decode($fileValues)[0] ?? [];
                
            if(in_array($field, static::SUBSTITUTION_FILES_KEYS) && ($value->download_link ?? false)) {
                $this->old_files_paths[$field] = $value->download_link;
            }
        }
    }

    public function update(Request $request)
    {
        if (static::getValidator($request)->fails()) {
            return back()->with([
                'message'    => 'Поля [' . implode(', ', static::SUBSTITUTION_FILES_KEYS) . '] должны быть pdf!',
                'alert-type' => 'error',
            ]);
        }

        return parent::update($request);
    }

    public function delete_value($id)
    {
        $setting = Voyager::model('Setting')->find($id);

        // Check permission
        $this->authorize('delete', $setting);

        if (isset($setting->id)) {
            // If the type is an image... Then delete it
            $field = str_replace('.', '_', $setting->key);
            if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->value)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->value);
                } 
            } elseif (in_array($field, static::SUBSTITUTION_FILES_KEYS)) {
                $this->deleteFileByField($field);
            }
            $setting->value = '';
            $setting->save();
        }

        request()->session()->flash('setting_tab', $setting->group);

        return back()->with([
            'message'    => __('voyager::settings.successfully_removed', ['name' => $setting->display_name]),
            'alert-type' => 'success',
        ]);
    }

    public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
    {
        if(in_array($row->field, static::SUBSTITUTION_FILES_KEYS) && $request[$row->field]) {
            $this->deleteFileByField($row->field);
            $fileName = $request[$row->field]->getClientOriginalName();
            Storage::disk(config('voyager.storage.disk'))->putFileAs('settings', $request[$row->field], $fileName);

            return json_encode([[
                'download_link' => "settings/$fileName",
                'original_name' => $fileName
            ]]);
        } else {
            return parent::getContentBasedOnType($request, $slug, $row, $options);
        }
    }

    public function deleteFileByField($field) {
        $oldFilePath = $this->old_files_paths[$field] ?? null;

        if(isset($oldFilePath) && Storage::disk(config('voyager.storage.disk'))->exists($oldFilePath)) {
            Storage::disk(config('voyager.storage.disk'))->delete($oldFilePath);
        }
    }

    public static function getValidator(Request $request) {
        return Validator::make($request->all(), [
            'site_policy_file' => 'file|mimes:pdf',
            'site_processing_personal_data_file' => 'file|mimes:pdf',
            'site_user_agreement_file' => 'file|mimes:pdf',
        ]);
    }
}

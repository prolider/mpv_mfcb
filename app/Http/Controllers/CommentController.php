<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Ticket;
use Illuminate\Http\Request;

class CommentController
{
    /**
     * Доступные теги
     *
     * @var string
     */
    protected $allowedTags = '<a><p><div><span><em><ul><li><strong><code><sub><sup><blockquote><table><tr><td><tbody><h1><h2><h3><h4><h5><h6><pre>';

    /**
     * Создание комментария пользователем
     *
     * @param  Ticket  $ticket
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Ticket $ticket, Request $request)
    {
        $comment = [
            'ticket_id' => $ticket->id,
            'user_id' => auth()->id(),
            'status' => Comment::STATUS_NEW,
            'visibility' => Comment::VISIBILITY_ALL,
            'text' => strip_tags(
                $request->input('text'), $this->allowedTags),
        ];

        if ($request->hasFile('attachment')) {
            $files = $request->file('attachment');
            $attachments = [];
            foreach ($files as $file) {
                $filePath = "comments/ticket/{$ticket->id}/";
                $path = $file->store($filePath, ['disk' => 'public']);
                $attachments[] = [
                    'path' => $path,
                    'name' => $file->getClientOriginalName(),
                ];
            }

            $comment['attachment'] = $attachments;
        }
        Comment::create($comment);

        return redirect()
            ->back()
            ->with('comment', 'success');
    }

    /**
     * Создание комментария администратором или оператором
     *
     * @param  Ticket  $ticket
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function operatorStore(Ticket $ticket, Request $request)
    {
        $comment = [
            'ticket_id' => $ticket->id,
            'user_id' => auth()->id(),
            'status' => Comment::STATUS_NEW,
            'visibility' => $request->input('visibility'),
            'text' => strip_tags(
                $request->input('text'), $this->allowedTags),
        ];

        if ($request->hasFile('attachment')) {
            $files = $request->file('attachment');
            $attachments = [];
            foreach ($files as $file) {
                $filePath = "comments/ticket/{$ticket->id}/";
                $path = $file->store($filePath, ['disk' => 'public']);
                $attachments[] = [
                    'path' => $path,
                    'name' => $file->getClientOriginalName(),
                ];
            }

            $comment['attachment'] = $attachments;
        }
        Comment::create($comment);

        return redirect()
            ->back()
            ->with('comment', 'success');
    }

    /**
     * @param  Ticket  $ticket
     * @return \Illuminate\Http\JsonResponse
     */
    public function setAllReaded(Ticket $ticket)
    {
        Comment::query()
            ->where('ticket_id', '=', $ticket->id)
            ->where('user_id', '<>', auth()->id())
            ->update([
                'status' => Comment::STATUS_READED,
            ]);

        return response()->json();
    }
}

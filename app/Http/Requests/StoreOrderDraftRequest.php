<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderDraftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable',
            'service_id' => 'required',
            'category_id' => 'required',
            'firstname' => 'exclude_unless:user_id,false|nullable',
            'name' => 'exclude_unless:user_id,false|nullable',
            'patronymic' => 'exclude_unless:user_id,false|nullable',
            'sex' => 'exclude_unless:user_id,false|nullable',
            'birthday' => 'exclude_unless:user_id,false|nullable',
            'phone' => 'exclude_unless:user_id,false|nullable',
            'email' => 'exclude_unless:user_id,false|nullable',
            'bornplace' => 'exclude_unless:user_id,false|nullable',
            'passport_serial' => 'exclude_unless:user_id,false',
            'passport_number' => 'exclude_unless:user_id,false',
            'passport_date' => 'exclude_unless:user_id,false',
            'passport_subdivision' => 'exclude_unless:user_id,false',
            'passport_organization' => 'exclude_unless:user_id,false',
            'registration_place' => 'exclude_unless:user_id,false',
            'inn' => 'exclude_unless:user_id,false',
            'snils' => 'exclude_unless:user_id,false',
            'citizenship' => 'exclude_unless:user_id,false',
            'company_inn' => 'exclude_unless:user_id,false',
            'company_ogrn' => 'exclude_unless:user_id,false',
            'company_kpp' => 'exclude_unless:user_id,false',
            'organization_date' => 'exclude_unless:user_id,false',
            'company_name' => 'exclude_unless:user_id,false',
            'company_adress' => 'exclude_unless:user_id,false',
            'company_ceo' => 'exclude_unless:user_id,false',
            'company_ceo_fio' => 'exclude_unless:user_id,false',
        ];
    }
}

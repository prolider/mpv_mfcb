<?php

namespace App\Http\Requests\Discussion;

use Illuminate\Foundation\Http\FormRequest;

class DiscussionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'text' => 'required|string',
            'is_alert' => 'nullable|sometimes|string',
            'files' => 'nullable|array',
            'files.*' => 'file|max:5120|mimes:png,jpeg,jpg,docx,doc,xlsx,xls,sig,pdf',
        ];
    }
}

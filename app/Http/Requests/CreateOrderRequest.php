<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable',
            'service_id' => 'required',
            'category_id' => 'required',
            'firstname' => 'exclude_unless:user_id,false|required',
            'name' => 'exclude_unless:user_id,false|required',
            'patronymic' => 'exclude_unless:user_id,false|required',
            'sex' => 'exclude_unless:user_id,false|required',
            'birthday' => 'exclude_unless:user_id,false|required',
            'phone' => 'exclude_unless:user_id,false|required',
            'email' => 'exclude_unless:user_id,false|required',
            'password' => 'exclude_unless:user_id,false|required',
            'bornplace' => 'exclude_unless:user_id,false|required',
            'passport_serial' => 'exclude_unless:user_id,false|required',
            'passport_number' => 'exclude_unless:user_id,false|required',
            'passport_date' => 'exclude_unless:user_id,false|required',
            'passport_subdivision' => 'exclude_unless:user_id,false|required',
            'passport_organization' => 'exclude_unless:user_id,false|required',
            'registration_place' => 'exclude_unless:user_id,false|required',
            'inn' => 'exclude_unless:user_id,false|required',
            'snils' => 'exclude_unless:user_id,false|required',
            'citizenship' => 'exclude_unless:user_id,false|required',
            'company_inn' => 'exclude_unless:user_id,false',
            'company_ogrn' => 'exclude_unless:user_id,false',
            'company_kpp' => 'exclude_unless:user_id,false',
            'organization_date' => 'exclude_unless:user_id,false',
            'company_name' => 'exclude_unless:user_id,false',
            'company_adress' => 'exclude_unless:user_id,false',
            'company_ceo' => 'exclude_unless:user_id,false',
            'company_ceo_fio' => 'exclude_unless:user_id,false',
        ];
    }
}

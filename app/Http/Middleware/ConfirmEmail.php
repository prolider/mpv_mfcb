<?php

namespace App\Http\Middleware;

use Closure;

class ConfirmEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if (! $user) {
            return redirect()->route('login');
        }

        if (! $user->is_confirmed) {
            return redirect()->route('confirm');
        }

        return $next($request);
    }
}

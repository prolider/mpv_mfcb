<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');

Route::get('/privacy-policy', 'HomeController@policyFile')->name('policy');
Route::get('/processing-personal-data', 'HomeController@processingPersonalDataFile')->name('processing-personal-data');
Route::get('/user-agreement', 'HomeController@userAgreementFile')->name('user-agreement');

Route::get('/faq', 'HomeController@faq')->name('faq');

Route::prefix('search')->name('search.')->group(function () {
    Route::get('', 'SearchController@index')->name('index');
});

Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
    Route::get('', 'ServiceController@index')->name('index');
    Route::get('{service}', 'ServiceController@show')->name('detail');
    Route::get('{service}/detail-admin', 'ServiceController@adminDetail')->name('detail-admin')->middleware(['admin.user']);
    Route::get('{service}/form/{ticket?}', 'ServiceController@showForm')->name('form');
    Route::post('{service}/form/{ticket?}', 'ServiceController@storeForm')->name('store-form');
    Route::get('{service}/form-order', 'ServiceController@storeTicketFromOrder')->name('form-order');
    Route::post('{service}/draft/{ticket?}', 'ServiceController@storeDraft')->name('store-draft');
});

Route::prefix('discussions')->name('discussions.')->middleware('invited.forbid')->group(function () {
    Route::get('', 'DiscussionController@index')->name('index');
    Route::get('new', 'DiscussionController@create')->name('create');
    Route::get('{discussion}', 'DiscussionController@show')->name('show');
    Route::post('', 'DiscussionController@store')->name('store');
});

Route::group(['middleware' => ['auth', 'confirm.email', 'twofactor']], function () {
    Route::get('home', 'HomeController@home')->name('home');
    Route::get('profile', 'HomeController@profile')->name('profile.index');
    Route::post('profile', 'HomeController@updateProfile')->name('profile.update');
    Route::get('mesages', 'HomeController@mesages')->name('mesages.index');
    Route::get('notices', 'HomeController@notices')->name('notices.index');
    Route::get('invites', 'HomeController@invites')->name('invites.index');

    Route::get('report', 'TicketController@report')->name('report');
    Route::get('report/export', 'TicketController@ticketsExport')->name('report-export');

    //Route::prefix('search')->name('search.')->group(function () {
    //    Route::get('', 'SearchController@index')->name('index');
    //});

    Route::group(['prefix' => 'ticket', 'as' => 'ticket.'], function () {
        Route::get('', 'TicketController@index')->name('index');
        Route::get('/archive', 'TicketController@archive')->name('archive');
        Route::post('', 'TicketController@window')->name('window');
        Route::get('{ticket}', 'TicketController@show')->name('show');
        Route::post('{ticket}/comment', 'CommentController@store')->name('comment.new');
    });

    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('', 'OrderController@index')->name('index');
        Route::get('/create', 'OrderController@create')->name('create');
        Route::post('/update/{order}', 'OrderController@update')->name('update');
        Route::post('/store', 'OrderController@store')->name('store');
        Route::post('/store-draft', 'OrderController@storeDraft')->name('store_draft');
        Route::post('/update/{order}', 'OrderController@update')->name('update');
        Route::post('/update-draft/{order}', 'OrderController@updateDraft')->name('update_draft');
        Route::get('/{order}', 'OrderController@show')->name('show');
    });

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
        Route::get('ticket', 'TicketController@index');
        Route::get('ticket/archive', 'TicketController@archive')->name('api.ticket.archive');
        Route::get('ticket/{ticket}', 'TicketController@show');
        Route::post('ticket/{ticket}/comment', 'TicketController@comment');
        Route::post('ticket/{ticket}/status', 'TicketController@changeStatus')->name('api.ticket.status');
        Route::post('tickets/comment', 'TicketController@createComments');
        Route::post('tickets/status', 'TicketController@changeStatuses');

        Route::get('order', 'OrderController@index');
        Route::post('order/filtered-users', 'OrderController@filteredUsers');
        Route::post('order/select-user', 'OrderController@selectUser');
        Route::post('order/select-category', 'OrderController@selectCategory');
        Route::post('order/select-service', 'OrderController@selectService');
        Route::get('order/{order}', 'OrderController@show');
        Route::post('order/{order}/status', 'OrderController@changeStatus')->name('api.order.status');
        Route::post('order/{order}/update-service', 'OrderController@updateService');

        Route::post('ticket/{ticket}/updateCheckList', 'TicketController@updateCheckList')->name('api.ticket.updateCheckList');

        Route::get('user', 'UserController@index');

        Route::delete('notification/{notification}', 'NotificationController@destroy')->name('api.notification.delete');
        Route::delete('invite/{invite}', 'InviteController@delete')->name('api.invite.delete');

        Route::get('invite', 'InviteController@index')->name('api.invite.index');
        Route::get('invite/available', 'InviteController@available')->name('api.invite.available');

        Route::post('affiliate/select-operators', 'AffiliateController@selectOperators');
        Route::post('affiliate/select-windows', 'AffiliateController@selectWindows');

        Route::prefix('discussions')->group(function () {
            Route::get('{discussion}/change-is-alert', 'DiscussionController@changeIsAlert')->name('changeisalert');

            Route::prefix('messages')->group(function () {
                Route::post('{discussion}', 'DiscussionController@sendMessage')->name('send');
                Route::get('{message}/read', 'DiscussionController@read')->name('read');
            });
        });

        Route::get('service-categories', 'ServiceController@getServiceCategories')->name('api.service-category');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => 'confirm.email', 'twofactor'], function () {
    Voyager::routes();
    Route::put('settings', 'Voyager\SettingsController@update')->name('voyager.settings.update');
    Route::put('settings/{id}/delete_value', 'Voyager\SettingsController@delete_value')->name('voyager.settings.delete_value');

    Route::group(['middleware' => 'admin.user'], function () {
        Route::post('tickets/{ticket}/comment', 'CommentController@operatorStore')->name('operator.add.ticket');
        Route::post('tickets/{ticket}/comment/set-all-readed', 'CommentController@setAllReaded')->name('operator.read.tickets');

        Route::post('services/{service}/document', 'Api\\DocumentController@store');
        Route::put('services/{service}/document/{serviceDocument}', 'Api\\DocumentController@update');
        Route::delete('services/{service}/document/{serviceDocument}', 'Api\\DocumentController@delete');
        Route::get('services/{service}/copy', 'Voyager\\ServiceController@copy')->name('operator.copy.service');
        Route::get('supports/{support}/copy', 'Voyager\\SupportController@copy')->name('operator.copy.support');

        Route::post('services/affiliate-services', 'Api\\ServiceController@store');
    });
});

Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
    Route::get('', 'CategoryController@category')->name('index');
    Route::get('{serviceCategory}', 'CategoryController@show')->name('detail');
});

Route::get('verify-authentication/resend', 'Auth\TwoFactorController@resend')
    ->middleware('twofactor')
    ->name('verify-authentication.resend');
Route::resource('verify-authentication', 'Auth\TwoFactorController')
    ->middleware('twofactor')
    ->only(['index', 'store']);

Route::get('login/registered', 'HomeController@confirm')->middleware('auth')->name('confirm');
Route::get('login/confirm', 'HomeController@confirmation')->middleware('auth')->name('confirmation');
Route::get('auth/confirm/{user}', 'Voyager\UserController@confirmation')->name('confirmation.auth');

<?php

use App\Http\Controllers\Api\MSP\MspController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'damask', 'as' => 'damask.', 'namespace' => 'Api\Damask'], function () {
    Route::get('create-ticket-from-damask', 'TicketController@create');
});

Route::group(['prefix' => 'msp', 'as' => 'msp.'], function () {
    Route::post('login', [MspController::class, 'generateToken']);
    Route::get('support-description-fields', [MspController::class, 'supportDescriptions']);
    Route::get('support-ticket-field', [MspController::class, 'supportTicketFields']);
    Route::post('create-ticket', [MspController::class, 'createTicket']);
    Route::get('created-ticket-status', [MspController::class, 'createdTicketStatus']);
});

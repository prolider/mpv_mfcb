<div id="form-builder"></div>
<input type="hidden" name="{{ isset($row) ? $row->field : 'form_builder' }}" value="">
<input type="hidden" name="deleted_fields" value="">

@push('javascript')
    <script>
        window.formBuilderData = {!! $content !!}
        window.supportFields = {!! json_encode(\App\Models\FormReference::all()) !!}
        $('form').on('submit', function (e) {
            $('input[name="{{ isset($row) ? $row->field : 'form_builder' }}"]').val(JSON.stringify(window.formBuilder.getData()));
            $('input[name="{{ 'deleted_fields' }}"]').val(JSON.stringify(window.deletedFields));
        });
    </script>

    <script src="/js/forms/form-builder.js"></script>
@endpush

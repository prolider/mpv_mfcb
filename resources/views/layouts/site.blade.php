<!DOCTYPE html>
<html lang="ru">
<head>
    <title>@yield('title')</title>
    <meta name="description" content="{{ setting('site.description') }}">
    <meta name="keywords" content="">
    <meta name="twitter:title" content="{{ setting('site.title') }}"/>
    <meta name="twitter:description" content="{{ setting('site.description') }}"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/css/titan.css">
    <link rel="stylesheet" href="/css/datatables.min.css"/>
    <script src="/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
    <script src="https://kit.fontawesome.com/1b8e599b2e.js" crossorigin="anonymous"></script>
    <script src="/js/datatables.min.js"></script>
    @stack('styles')
</head>
<body class="d-flex flex-column h-100">
    <header>
        <div class="top">
            <div class="fluid-container">
            </div>
            @include('menu.menu')
        </div>
    </header>
    <warning>
        @include('toasts.warning')
    </warning>
    <search>
        @include('search.searchform')
    </search>
    <main id="section-main" class="py-5">
        @yield('content')
    </main>
    <footer id="section-footer" class="footer mt-auto pt-4 pb-2">
            <div class="text-center menu-bottom">
                <div class="col-sm-12 pb-3 border-bottom">
                    <span class="d-inline-block mx-3 small"><a target="_self" href="{{ route('policy') }}">Политика конфиденциальности</a></span>
                    <span class="d-inline-block small"><a target="_self" href="{{ route('processing-personal-data') }}">Согласие на обработку персональных данных</a></span>
                    <span class="d-inline-block mx-3 small"><a target="_self" href="{{ route('user-agreement') }}">Пользовательское соглашение</a></span>
                </div>
            </div>
            <div class="text-center pt-3 copyright">
                <div class="col-sm-12 small text-muted">
                    {!! setting('site.official_name') !!}
                </div>
                <div class="col-sm-12 small text-muted">
                    {!! setting('site.official_adress') !!}
                </div>
                <div class="col-sm-12 small text-muted">
                    {!! setting('site.official_requisites') !!}
                </div>
                <div class="col-sm-12 small text-muted">
                    <span class="">©&nbsp;{{ date('Y') }}&nbsp; {!! setting('site.abbreviated_name') !!}</span>
                </div>
            </div>
    </footer>
    @yield('js')
    <!--Notifications-->
    <div aria-live="polite" aria-atomic="true" class="position-relative">
        <div class="toast-container position-fixed bottom-0 end-0 p-3" style="z-index:99999">
            @yield('toasts')
            @error('email', 'login')
                @include('toasts.auth_error_toast')
            @enderror
        </div>
    </div>
    <!--Offcanvas -->
    @include('auth.offcanvaslogin')

</body>
    @stack('javascript')
</html>
<!--Add tooltips-->
<script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>
<!--Add notifications-->
<script>
    var toastElList = [].slice.call(document.querySelectorAll('.toast'))
    var toastList = toastElList.map(function (toastEl) {
    return new bootstrap.Toast(toastEl, option)
    })
</script>

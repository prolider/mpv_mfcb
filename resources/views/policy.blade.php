@extends('layouts.site')

@section('title')
    Политика конфиденциальности и обработки персональных данных
@endsection

@section('content')
<div class="container content p-5 shadow-sm">
    <span>
        <a href="{{ route('policy-file') }}"> Скачать </a>
    </span>
</div>
@endsection

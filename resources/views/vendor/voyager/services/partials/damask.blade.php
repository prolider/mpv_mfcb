<div class="modal" tabindex="-1" role="dialog" id="affiliate-services-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Номера услуг в филиалах</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="affiliate-services-form" action="">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Филиал</th>
                            <th>Номер услуги</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $affiliateServices = $dataTypeContent->affiliateServices;
                        @endphp
                        @forelse($affiliateServices as $affiliateService)
                            <tr>
                                <td>
                                    {{ $affiliateService->affiliate->name }}
                                </td>
                                <td>
                                    <input class="form-control"
                                           name="affiliate_services[{{ $affiliateService->id }}]"
                                           value="{{ $affiliateService->damask_service_code }}">
                                </td>
                            </tr>
                        @empty
                            <tr class="empty-checkboxes">
                                <td colspan="3" class="text-center">Пока нет элементов'</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-affiliate-services">Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#add-affiliate-services-modal').on('click', function () {
        $('#affiliate-services-modal').modal('show');
    });

    $(document).on('click', '#save-affiliate-services', function () {
        $('#affiliate-services-form').submit();
    });

    $(document).on('submit', '#affiliate-services-form', function (e) {
        e.preventDefault();
        let _this = $(this);
        let fd = new FormData(e.target);
        fd.append('_token', $('meta[name="csrf-token"]').attr('content'));

        console.log(_this.serializeArray())
        $.ajax({
            url: `/admin/services/affiliate-services`,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                toastr.success('Номера услуг в филиалах успешно изменены.');
                $('#affiliate-services-modal').modal('hide');
            }
        });
    });
</script>
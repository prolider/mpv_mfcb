<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th>Название</th>
        <th>Файл</th>
        <th>Позиция</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    @forelse($documents as $document)
        <tr>
            <td>{{ $document->name }}</td>
            <td>
                <a href="/storage/{{ $document->file }}" download>
                    /storage/{{ $document->file }}
                </a>
            </td>
            <td>{{ $document->sort }}</td>
            <td class="text-right">
                <button type="button" class="btn btn-warning" data-id="{{ $document->id }}">Изменить</button>
                <button type="button" class="btn btn-danger" data-id="{{ $document->id }}">Удалить</button>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="4" class="text-center">Пока нет документов</td>
        </tr>
    @endforelse
    </tbody>
</table>

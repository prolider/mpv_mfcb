<div class="panel-body">
    <div class="checkbox-table" style="margin-bottom: 50px;">
        <div style="display: flex; justify-content: space-between;">
            <h4>Список действий с заявкой</h4>
            <button type="button" class="btn btn-success add-checkbox" id="add_checkbox">Добавить</button>
        </div>

        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $checkboxes = json_decode($dataTypeContent->check_list, true) ?? [];
                @endphp
                @forelse($checkboxes as $key => $checkbox)
                <tr>
                    <td>
                        <input class="form-control" name="check_list[{{ $key }}][name]" value="{{ $checkbox['name'] }}">
                    </td>
                    <td>
                        <button class="btn btn-danger remove-checkbox" type="button">Удалить</button>
                    </td>
                </tr>
                @empty
                    <tr class="empty-checkboxes">
                        <td colspan="3" class="text-center">Пока нет элементов'</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>


</div>

@push('javascript')

    <script>
        var serviceId = +'{{ $dataTypeContent->id }}';
        var nextcheckboxIdx = {{ count($checkboxes) }};

          $('.checkbox-table .add-checkbox').on('click', function () {
            $('.checkbox-table table tbody').append(`
                <tr>
                    <td>
                        <input class="form-control" name="check_list[${nextcheckboxIdx}][name]" value="">
                    </td>
                    <td>
                        <button class="btn btn-danger remove-checkbox" type="button">Удалить</button>
                    </td>
                </tr>
            `);
            $('.empty-checkboxes').hide();
            nextcheckboxIdx++;
          });

          $('.checkbox-table').on('click', '.remove-checkbox', function (e) {
            e.preventDefault();
            $(e.target).parent().parent().remove();
            if ($('.checkbox-table table tbody tr').length === 1) {
              $('.empty-checkbox').show();
            }
          });
    </script>
@endpush

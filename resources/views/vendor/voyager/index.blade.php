@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-activity"></i> Сводные данные
        </h1>        
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <h4>Заявки</h4>
                        <p>Всего заявок:</p>
                        <p>Открытых заявок:</p>
                        <p>Закрытых заявок:</p>
                        <p>Заявок за месяц:</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <h4>Обращения</h4> 
                        <p>Всего обращений:</p>
                        <p>Открытых обращений:</p>
                        <p>Закрытых обращений:</p>
                        <p>Обращений за месяц:</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <h4>Услуги</h4>
                        <p>Всего услуг:</p>
                        <p>Информационных услуг:</p>
                        <p>Сопроводительных услуг:</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')

@stop

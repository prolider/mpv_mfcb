@if(isset($options->model) && isset($options->type))

    @if(class_exists($options->model))

        @php $relationshipField = $row->field; @endphp

        @if(isset($view) && ($view == 'browse' || $view == 'read'))

            @php
                $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                $model = app($options->model);
                $query = $model::where($options->key,$relationshipData->{$options->column})->first();
            @endphp

            @if(isset($query))
                <p>{{ $query->{$options->label} }}</p>
            @else
                <p>{{ __('voyager::generic.no_results') }}</p>
            @endif

        @else

            <select
                class="form-control select2-ajax-window" name="{{ $options->column }}"
                data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                data-get-items-field="{{$row->field}}"
                @if(!is_null($dataTypeContent->getKey())) data-id="{{$dataTypeContent->getKey()}}" @endif
                data-method="{{ !is_null($dataTypeContent->getKey()) ? 'edit' : 'add' }}"
            >
                @php
                    $model = app($options->model);
                    $query = $model::where($options->key, old($options->column, $dataTypeContent->{$options->column}))->get();
                @endphp

                @if(!$row->required)
                    <option value="">{{__('voyager::generic.none')}}</option>
                @endif

                @foreach($query as $relationshipData)
                    <option value="{{ $relationshipData->{$options->key} }}" @if(old($options->column, $dataTypeContent->{$options->column}) == $relationshipData->{$options->key}) selected="selected" @endif>{{ $relationshipData->{$options->label} }}</option>
                @endforeach
            </select>

        @endif
    @endif
@endif

@push('javascript')
    <script>
        $('select.select2-ajax-window').each(function() {
            $(this).select2({
                width: '100%',
                tags: $(this).hasClass('taggable'),
                createTag: function(params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        newTag: true
                    }
                },
                ajax: {
                    url: $(this).data('get-items-route'),
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: $(this).data('get-items-field'),
                            method: $(this).data('method'),
                            id: $(this).data('id'),
                            affiliates: $('select[name="affiliate_id"]').val(),
                            page: params.page || 1
                        }
                        return query;
                    }
                }
            });

            $(this).on('select2:select',function(e){
                var data = e.params.data;
                if (data.id == '') {
                    // "None" was selected. Clear all selected options
                    $(this).val([]).trigger('change');
                } else {
                    $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected','selected');
                }
            });

            $(this).on('select2:unselect',function(e){
                var data = e.params.data;
                $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected',false);
            });

            $(this).on('select2:selecting', function(e) {
                if (!$(this).hasClass('taggable')) {
                    return;
                }
                var $el = $(this);
                var route = $el.data('route');
                var label = $el.data('label');
                var errorMessage = $el.data('error-message');
                var newTag = e.params.args.data.newTag;

                if (!newTag) return;

                $el.select2('close');

                $.post(route, {
                    [label]: e.params.args.data.text,
                    _tagging: true,
                }).done(function(data) {
                    var newOption = new Option(e.params.args.data.text, data.data.id, false, true);
                    $el.append(newOption).trigger('change');
                }).fail(function(error) {
                    toastr.error(errorMessage);
                });

                return false;
            });
        });

        $('select[name="affiliate_id"]').on('change', (event) => {
            $('select.select2-ajax-window').val('').trigger('change');
        });
    </script>
@endpush

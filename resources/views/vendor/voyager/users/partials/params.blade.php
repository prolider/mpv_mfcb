<div class="col-md-9">

    <div class="panel panel-bordered">
        {{-- <div class="panel"> --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-body row">
            <div class="form-group col-md-4">
                <label for="name">Фамилия</label>
                <input type="text" class="form-control" id="name" name="firstname" placeholder="Фамилия"
                       value="{{ old('firstname', $dataTypeContent->firstname ?? '') }}">
            </div>

            <div class="form-group  col-md-4">
                <label for="name">{{ __('voyager::generic.name') }}</label>
                <input type="text" class="form-control" id="name" name="name"
                       placeholder="{{ __('voyager::generic.name') }}"
                       value="{{ old('name', $dataTypeContent->name ?? '') }}">
            </div>

            <div class="form-group  col-md-4">
                <label for="name">Отчество</label>
                <input type="text" class="form-control" id="name" name="patronymic" placeholder="Отчество"
                       value="{{ old('patronymic', $dataTypeContent->patronymic ?? '') }}">
            </div>

            <div class="form-group  col-md-4">
                <label for="name">Пол</label>
                <ul class="radio">
                    <li>
                        <input type="radio"
                               id="option-M"
                               name="sex"
                               value="{{ \App\Models\User::SEX_MALE }}"
                               @if($dataTypeContent->sex == \App\Models\User::SEX_MALE || $dataTypeContent->sex === NULL) checked @endif>
                        <label for="option-M">Мужской</label>
                        <div class="check"></div>
                    </li>
                    <li>
                        <input type="radio"
                               id="option-F"
                               name="sex"
                               value="{{ \App\Models\User::SEX_FEMALE }}"
                               @if($dataTypeContent->sex == \App\Models\User::SEX_FEMALE) checked @endif>
                        <label for="option-F">Женский</label>
                        <div class="check"></div>
                    </li>
                </ul>

            </div>

            <div class="form-group  col-md-4">
                <label for="name">Дата рождения</label>
                <input type="date" class="form-control" name="birthday"
                       placeholder="Дата рождения"
                       value="@if(isset($dataTypeContent->birthday)){{ \Carbon\Carbon::parse(old('birthday', $dataTypeContent->birthday))->format('Y-m-d') }}@else{{old('birthday')}}@endif">
            </div>

            <div class="form-group col-md-4">
                <label for="name">Телефон</label>
                <input type="tel" class="form-control" id="phone" name="phone" placeholder="Телефон"
                       value="{{ old('phone', $dataTypeContent->phone ?? '') }}">
            </div>

            <div class="form-group col-md-4">
                <label for="email">{{ __('voyager::generic.email') }}</label>
                <input type="email" class="form-control" id="email" name="email"
                       placeholder="{{ __('voyager::generic.email') }}"
                       value="{{ old('email', $dataTypeContent->email ?? '') }}">
            </div>

            <div class="form-group col-md-4">
                <label for="password">{{ __('voyager::generic.password') }}</label>
                @if(isset($dataTypeContent->password))
                    <br>
                    <small>{{ __('voyager::profile.password_hint') }}</small>
                @endif
                <input type="password" class="form-control" id="password" name="password" value=""
                       autocomplete="new-password">
            </div>

            @can('editRoles', $dataTypeContent)
                <div class="form-group col-md-4">
                    <label for="default_role">{{ __('voyager::profile.role_default') }}</label>
                    @php
                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                        $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                        $options = $row->details;
                    @endphp
                    @include('voyager::formfields.relationship')
                </div>

                {{--<div class="form-group col-md-4">
                    <label for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                    @php
                        $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                        $options = $row->details;
                    @endphp
                    @include('voyager::formfields.relationship')
                </div>--}}
            @endcan
            <div class="form-group col-md-4">
                    <label for="default_role">Филиал</label>
                    @php
                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                        $row     = $dataTypeRows->where('field', 'user_belongsto_affiliate_relationship')->first();
                        $options = $row->details;
                    @endphp
                    @include('voyager::formfields.relationship')
                </div>
            {{--            @php
                            if (isset($dataTypeContent->locale)) {
                                $selected_locale = $dataTypeContent->locale;
                            } else {
                                $selected_locale = config('app.locale', 'en');
                            }

                        @endphp
                        <div class="form-group col-md-4">
                            <label for="locale">{{ __('voyager::generic.locale') }}</label>
                            <select class="form-control select2" id="locale" name="locale">
                                @foreach (Voyager::getLocales() as $locale)
                                    <option value="{{ $locale }}"
                                            {{ ($locale == $selected_locale ? 'selected' : '') }}>{{ $locale }}</option>
                                @endforeach
                            </select>
                        </div>--}}
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="panel panel panel-bordered panel-warning">
        <div class="panel-body">
            <div class="form-group">
                @if(isset($dataTypeContent->avatar))
                    <img src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}"
                         style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                @endif
                <input type="file" data-name="avatar" name="avatar">
            </div>
        </div>
    </div>
</div>

<div class="panel panel-bordered">
    <div class="panel-body row">
        <div class="form-group col-md-3">
            <label for="name">ОГРН (ГОРНИП)</label>
            <input type="text" class="form-control" id="company_ogrn" name="company_ogrn" placeholder="ОГРН (ГОРНИП)"
                   value="{{ old('company_ogrn', $dataTypeContent->company_ogrn ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">ИНН</label>
            <input type="text" class="form-control" id="company_inn" name="company_inn" placeholder="ИНН"
                   value="{{ old('company_inn', $dataTypeContent->company_inn ?? '') }}">
        </div>

        <div class="form-group col-md-6">
            <label for="name">Полное наименование</label>
            <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Полное наименование"
                   value="{{ old('company_name', $dataTypeContent->company_name ?? '') }}">
        </div>
    </div>
</div>

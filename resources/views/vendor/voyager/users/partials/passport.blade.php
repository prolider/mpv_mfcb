<div class="panel panel-bordered">
    <div class="panel-body row">
        <div class="form-group col-md-3">
            <label for="name">ИНН</label>
            <input type="text" class="form-control" id="inn" name="inn" placeholder="ИНН"
                   value="{{ old('inn', $dataTypeContent->inn ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">СНИЛ</label>
            <input type="text" class="form-control" id="snils" name="snils" placeholder="СНИЛ"
                   value="{{ old('snils', $dataTypeContent->snils ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Гражданство</label>
            <input type="text" class="form-control" id="citizenship" name="citizenship" placeholder="Гражданство"
                   value="{{ old('citizenship', $dataTypeContent->citizenship ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Серия паспорта</label>
            <input type="text" class="form-control" id="passport_serial" name="passport_serial" placeholder="Серия паспорта"
                   value="{{ old('passport_serial', $dataTypeContent->passport_serial ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Номер паспорта</label>
            <input type="text" class="form-control" id="passport_number" name="passport_number" placeholder="Номер паспорта"
                   value="{{ old('passport_number', $dataTypeContent->passport_number ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Дата выдачи</label>
            <input type="date" class="form-control" name="passport_date"
                   placeholder="Дата выдачи"
                   value="@if(isset($dataTypeContent->passport_date)){{ \Carbon\Carbon::parse(old('passport_date', $dataTypeContent->passport_date))->format('Y-m-d') }}@else{{old('passport_date')}}@endif">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Код подразделения</label>
            <input type="text" class="form-control" id="passport_subdivision" name="passport_subdivision" placeholder="Код подразделения"
                   value="{{ old('passport_subdivision', $dataTypeContent->passport_subdivision ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Кем выдан</label>
            <input type="text" class="form-control" id="passport_organization" name="passport_organization" placeholder="Кем выдан"
                   value="{{ old('passport_organization', $dataTypeContent->passport_organization ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Адрес прописки</label>
            <input type="text" class="form-control" id="registration_place" name="registration_place" placeholder="Адрес прописки"
                   value="{{ old('registration_place', $dataTypeContent->registration_place ?? '') }}">
        </div>
    </div>
</div>

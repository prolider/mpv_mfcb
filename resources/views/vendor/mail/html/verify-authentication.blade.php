@component('vendor.mail.html.message')
<p>Проверочный код: {{ $two_factor_code }}</p>
<p>Срок действия кода — 10 минут</p>
<p>Если вы не пытались войти на портал "МойБизнес73", проигнорируйте это сообщение.</p>
@endcomponent
@component('vendor.mail.html.message')
<p>Пользователь {{$user->fullName}} создал новое обращение.</p>
<p>Для просмотра обращения необходимо войти в личный кабинет администратора портала.</p>
@endcomponent
@extends('layouts.site')

@section('title')
    Приглашения
@endsection

@section('content')
    <div class="container">
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <div class="title">    
                <h1>Приглашения</h1>
            </div>
        </div>
        @if($invites->count() > 0)
            @foreach($invites as $invite)
                <div class="mb-3 block shadow-sm p-4">                    
                    <div class="d-flex justify-content-between">
                        <div class="text-muted">
                            <small>Заявка №{{$invite->ticket_id}}</small> от <small>{{ $invite->updated_at->format('d.m.Y, m:h') }}</small>
                        </div>
                        <div class="mb-2 text-end">                                
                            @if ($invite->invite_time >= now())                     
                                <span class="badge rounded-pill badge-corall w-auto">Предстоящее</span>
                            @else
                                <span class="badge rounded-pill badge-light w-auto">Прошедшее</span>
                            @endif
                            
                        </div>
                    </div>                    
                    <div class="d-block align-items-center">
                        <h5 class="d-block mb-3">
                            <a href="{{ route('ticket.show', $invite->ticket_id) }}">{{$invite->ticket->formResult->form->service->name}}</a>
                        </h5>
                        <div class="d-flex align-items-center mb-2">
                            <div class="d-flex icon-invite me-2">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div>{{ $invite->invite_time->format('d.m.Y') }}</div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="d-flex icon-invite me-2">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div>{{ $invite->invite_time->format('H:i') }}</div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="d-flex icon-invite me-2">
                                <i class="fa fa-user-circle"></i>
                            </div>
                            <div>{{ $invite->window->name ?? ''}}</div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="d-flex icon-invite me-2">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div>{{ $invite->window->affiliate->description ?? ''}}</div>
                        </div>
                    </div>
                    <div class="d-block button-more text-end mt-3">
                        <a href="{{ route('ticket.show', $invite->ticket_id) }}" class="btn btn-sm btn-blue">Подробнее <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            @endforeach
        @else
            <div class="content"> 
                <p>Пока у Вас нет приглашений.</p>
            </div>
         @endif
  </div>
@stop



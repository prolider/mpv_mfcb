@extends('layouts.site')

@section('content')
    <div class="container">
        <h1 class="mb-4">Результаты поиска</h1>
        <div class="row">
            <div class="col-xl-12 col-12">
                <div class="row">        
                    @foreach ($services as $service) 
                        <!-- <div class="content shadow-sm p-5">
                            <div class="row"> -->
                                <!-- <div class="card card-custom gutter-b col-4">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="la la-filter text-primary"></i>
                                            </span>
                                            <h3 class="card-label">Область поиска</h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form action="{{ route('search.index') }}" method="get">
                                            <div class="form-group">
                                                <label class="form-label" for="search">Поиск</label>
                                                <input name="search" class="form-control" id="search" type="text" value="{{ old('search') ?? $search ?? '' }}">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Найти</button>
                                        </form>
                                    </div>
                                </div> -->
                                <div class="col-md-4 mb-4">
                                    <div class="category card border-0 shadow-sm p-4">
                                        <div class="card-title mb-3">
                                            <h5 class="fw-bold">
                                                <a href="{{ route('service.detail', $service) }}" class="category-link fw-bold">
                                                    {{ $service->name }}
                                                </a>
                                            </h5>
                                        </div> 
                                        <div class="card-body text-muted">
                                            {!! $service->predescription !!}
                                        </div>   
                                        <div class="card-footer d-flex align-items-center justify-content-between border-0 bg-transparent px-0 pt-4">
                                            <div class="text-start">
                                                @if($service->use_button === 'Y')
                                                    <small class="rounded-pill bg-warning text-sm text-dark py-1 px-3">Сопроводительная</small>    
                                                @elseif ($service->use_button === 'N')
                                                    <small class="rounded-pill bg-light text-sm text-dark py-1 px-3">Информационная</small>    
                                                @endif
                                            </div>
                                            <div class="text-end">
                                                <a href="{{ route('service.detail', $service) }}" class="stretched-link"> <i class="fa fa-chevron-circle-right fa-2x"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                

                                <!-- <h3 class=""><a href="{{ route('service.detail', $service) }}">{{ $service->name}}</a></h3>
                                <div class="card-body">
                                    <div class="w-100"> {!! $service->predescription !!}</div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('service.detail', $service) }}"
                                        class="btn btn-outline-blue rounded-pill px-4 mt-3">
                                            Подробнее
                                        </a>
                                    </div>
                                </div> -->
                            <!-- </div>
                                    

                            </div>
                        </div> -->
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

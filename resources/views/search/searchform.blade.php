<div class="container mt-4">
    <div class="search_form" id="">
        <form id="search_form" action="{{ route('search.index') }}" method="get">
            <div class="input-group">
                <input type="text" name="search" class="form-control form-control-lg" placeholder="введите текст для поиска">
                <button class="ml-5 btn btn-blue"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
</div>
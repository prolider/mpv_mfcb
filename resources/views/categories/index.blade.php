@extends('layouts.site')

@section('title')
    Направления услуг МФЦ для Бизнеса Ульяновской области
@endsection

@section('content')
    <div class="container">
        <h1  class="mb-4">Направления услуг</h1>
        <div class="row">
            @forelse($categories as $category)
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 mb-4">                    
                    <div class="category card border-0 shadow-sm p-4 {{ $category->class_name }}">
                        <!-- @if ($category->image)
                            <div class="category-image">
                                <img src="{{ asset('storage/'.$category->image) }}" alt="{{ $category->name }}">
                            </div>
                        @else
                            <div class="icon-block">
                                <span class="fa fa-check-circle-o" aria-hidden="true"></span>
                            </div>
                        @endif -->
                        <div class="card-title mb-3">
                            <h3><a class="category-link fw-bold" href="{{ route('categories.detail', $category) }}">{{ $category->name }}</a></h3>
                        </div>
                        <div class="card-body сategory-description px-0">
                            <div class="category-description text-muted">{!! $category->description !!}</div>
                        </div>
                        
                        <div class="card-footer bg-transparent border-0 category-readmore px-0 pt-4 text-end">
                            <a class="stretched-link" href="{{ route('categories.detail', $category) }}">
                                <!-- Подробнее -->
                                <i class="fa fa-chevron-circle-right fa-2x"></i>
                            </a> 
                        </div>
                    </div>                        
                </div>
            @empty
                <p>Нет категорий</p>
            @endforelse
        </div>
    </div>
@endsection

@extends('layouts.site')

@section('title')
    {{ $serviceCategory->name ?? 'Default' }}
@endsection

@section('content')
    <div class="container">
        <h1  class="mb-4">{{ $serviceCategory->name ?? 'Default' }}</h1>
        <div class="row">
            <div class="col-xl-9 col-12">
                <div class="row">
                    @forelse($serviceCategory->services as $service)                        
                        <div class="col-md-6 mb-4">
                            <div class="category card border-0 shadow-sm p-4">
                                <div class="card-title mb-3">
                                    <h5 class="fw-bold">
                                        <a href="{{ route('service.detail', $service) }}" class="category-link fw-bold">
                                            {{ $service->name }}
                                        </a>
                                    </h5>
                                </div> 
                                <div class="card-body text-muted">
                                    {!! $service->predescription !!}
                                </div>   
                                <div class="card-footer d-flex align-items-center justify-content-between border-0 bg-transparent px-0 pt-4">
                                    <div class="text-start">
                                        @if($service->use_button === 'Y')
                                            <small class="rounded-pill bg-warning text-sm text-dark py-1 px-3">Сопроводительная</small>    
                                        @elseif ($service->use_button === 'N')
                                            <small class="rounded-pill bg-light text-sm text-dark py-1 px-3">Информационная</small>    
                                        @endif
                                    </div>
                                    <div class="text-end">
                                        <a href="{{ route('service.detail', $service) }}" class="stretched-link"> <i class="fa fa-chevron-circle-right fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                                             
                    @empty 
                        <div class="col-md-12 mb-4">               
                            <div class="content shadow-sm p-4">
                                <p>По данному направлению услуги не представлены на портале.</p>
                                <p>Вы можете обратиться к специалисту при личном посещении любого удобного для Вас офиса Многофункционально Центра для Бизнеса Ульяновской области.</p>
                            </div>
                        </div>
                    @endforelse 
                </div>               
            </div>
            <div class="col-xl-3 col-12">
                <div class="side card mb-3 p-4 shadow-sm">
                    <div class="card-header">
                        <h5>Направления услуг</h5>
                    </div>
                    
                    <div class="card-body">
                        @forelse($categories as $category)                                                        
                            <div class="py-2 mb-3">                                                                
                                <div class="">
                                    <a class="category-link
                                        @if(route('categories.detail', $category) === url()->current()) fw-bold @endif"
                                        href="{{ route('categories.detail', $category) }}">
                                        {{ $category->name }}
                                    </a>                                    
                                </div>
                            </div>                            
                        @empty
                            <div class="py-2 mb-3">Нет категорий</div>
                        @endforelse
                    </div>
                </div>   
            </div>
        </div>
    </div>
@endsection

@section('toasts')
    @include('toasts.auth_toast')
@endsection
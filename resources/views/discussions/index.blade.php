@extends('layouts.site')

@section('title')
    Обращения
@endsection

@section('content')
    <div class="container">
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <div class="title">    
                <h1>Обращения</h1>
            </div>
            <div class="text-end">
                @if (!in_array(auth()->user()->role->name, ['admin', 'operator']))
                    <a href="{{ route('discussions.create') }}" class="btn btn-blue" role="button">
                        Cоздать обращение
                    </a>
                @endif
            </div>
        </div>
            @forelse($discussions as $discussion)
                @php
                    $isRead = $discussion->messages_count > 0 ? "font-weight-bolder" : "";
                @endphp
                <div class="mb-4 block shadow-sm p-4">
                    <div class="d-flex justify-content-between">
                        <div class="text-muted fs-6">
                            <small>№{{ $discussion->id }} от</small> <small>{{ $discussion->updated_at->format('d.m.Y, H:m:s') }}</small>
                        </div>
                        <div class="mb-2 text-end">                                
                            @if ($discussion->messages_count > 0)                     
                                <span class="badge rounded-pill badge-corall w-auto">Непрочитано {{ $discussion->messages_count }}</span>
                            @endif
                            @if ($discussion->is_alert)                           
                                <span class="badge rounded-pill badge-blue w-auto">Открыто</span>
                            @else
                                <span class="badge rounded-pill badge-light w-auto">Закрыто</span>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex">                            
                        <a href="{{ route('discussions.show', $discussion)}}" class="fs-5">{{ $discussion->name }}</a>
                    </div>                                           
                    <div class="d-block button-more text-end mt-3">
                        <a href="{{ route('discussions.show', $discussion)}}"><i class="fa fa-chevron-circle-right fa-2x"></i></a>
                    </div>
                </div>
            @empty
                <div class="content">
                    У вас нет сообщений
                </div>
            @endforelse                
        </div>
    </div>
@endsection
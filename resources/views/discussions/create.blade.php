@extends('layouts.site')

@section('title')
    Создание нового обращения
@endsection

@section('content')
    <div class="container">
        <h1 class="mb-4">Новое обращение</h1>
        <div class="row">
            <div class="col-md-9">
                <div class="content p-4 shadow-sm">
                    <form id="kt_inbox_reply_form" action="{{ route('discussions.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="d-block">
                            @if ($isAlert)
                                <input type="hidden" name="is_alert" value="true">
                            @endif
                            <div class="mb-3">
                                <input class="form-control border-0" name="name" placeholder="Введите тему обращения" value="{{ $head ?? '' }}" required>
                            </div>
                            <hr>
                            <textarea class="form-control  border-0" name="text" placeholder="Введите текст сообщения" style="height: 200px;" required></textarea>
                        </div>
                        <div class="d-flex align-items-center mt-5">
                            <div class="mb-3">                        
                                <input type="file" multiple id="files_id" class="form-control custom-file-input" name="files[]" />
                                <span id="files-error"></span>
                            </div>
                            @error('files.*')
                                <span class="label label-light-danger font-weight-bold label-inline">{{ $message }}</span>
                            @enderror
                        </div>
                        <hr>
                        <div class="text-end">
                                <button type="submit" class="btn btn-blue">Отправить</button>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="side card mb-3">
                            <div class="card-header">
                                <h5>Часто задаваемые вопросы</h5>
                            </div>
                            <div class="card-body">
                                Вы можете ознакомится с наиболее часто задаваемыми вопросами в разделе
                                <a href=" {{ route('faq') }} ">Часто задоваемые вопросы</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#files_id').change(function (e) {
            var resultingSize = 0;
            let fileNames = '';

            this.files.forEach(element => {
                if (this.value === "") {
                    return;
                }

                fileNames += element.name + '<br>';

                if (element.size > 2097152) {
                    this.value = "";

                    $('#files-error').text("Размер одного файла не должен превышать 2 мегабайт");

                    return;
                }

                resultingSize += element.size;

                let maxSize = 5242880;

                if (maxSize < resultingSize) {
                    this.value = "";

                    $('#files-error').text("Общий размер файлов не должен превышать 5 мегабайт");

                    return;
                }
            });

            $('#files-error').html(fileNames);
        });
    </script>
@endsection

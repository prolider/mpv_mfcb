@extends('layouts.site')

@section('title')
    Обращение №{{ $discussion->id }}
@endsection

@section('content')
    <div class="container">
        <div id="app"></div>
    </div>
@endsection

@section('js')
    <script>
        window.discussion = {!! json_encode($discussion) !!};
        window.user = {!! json_encode($user) !!};
        window.csrf = $('meta[name="csrf-token"]').attr('content');
    </script>
    <script src="{{ asset('js/operator-lk.js') }}"></script>
@endsection

@extends('layouts.site')

@section('title')
    Обращение №{{ $discussion->id }}
@endsection

@section('content')
    <div class="container">
        <h1 class="mb-4">
            Обращение №{{ $discussion->id }}
        </h1>
        <div class="content p-4 shadow-sm">         
            <div class="d-flex justify-content-between mb-3">
                <div class="d-block">
                    <div class="d-block text-muted small">{{ $discussion->owner->name }} {{ $discussion->owner->firstname }}</div>            
                    <div class="d-block text-muted small">
                        {{ $discussion->created_at->format('d.m.Y, m:h') }}
                    </div>
                </div>
                <div class="d-block">
                    @if ($discussion->is_alert)    
                        <div class="mb-2 text-end">                            
                            <span class="badge rounded-pill badge-corall w-auto">Открыто</span>
                        </div>
                        @if (in_array(auth()->user()->role->name, ['admin', 'operator']))
                            <div class="btn-group" role="group">
                                <button id="dropdownAlertButton" type="button" class="btn btn-blue btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                    Открыто
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownAlertButton">
                                    <li><a class="dropdown-item" href="{{ route('discussions.changeisalert', $discussion ) }}">Закрыть</a></li>
                                    <li><a class="dropdown-item" href="{{ route('discussions.changeisalert', $discussion ) }}">Закрыть и создать заявку</a></li>
                                </ul>
                            </div>
                        @endif
                    @else
                        <div class="mb-2 text-end">                            
                            <span class="badge rounded-pill badge-light w-auto">Закрыто</span>
                        </div>    
                    @endif
                </div>
            </div>
            <div class="d-flex mb-3">                     
                <h4>{{ $discussion->name }}</h4>
            </div>
            <div class="d-block">{!! $discussion->text !!}</div>
            @if (is_array($discussion->filesUrls))
                <div class="d-block align-items-center py-3">
                    @foreach ($discussion->filesUrls as $key => $url)
                        <div class="mb-1 d-flex align-items-center">
                            <a href="{{ $url }}" target="_blank"><i class="icon-xl fa fa-file-download"></i> {{ $key }}</a>
                        </div>
                    @endforeach
                </div>
            @endif
            <hr>
            @foreach ($discussion->messages as $message)
                <div class="d-flex align-items-center justify-content-between pb-3" data-inbox="message">
                    <div class="d-block">
                        <div class="d-block text-muted small">
                            {{ auth()->user()->id === $message->user_owner_id ? 'Вы' : $message->author->name }} {{ auth()->user()->id === $message->user_owner_id ? '' : $message->author->firstname }}
                        </div>
                        <div class="d-block text-muted small">
                            <div class="font-weight-bold text-muted mr-2">
                                {{ $message->created_at->format('d.m.Y, m:h') }}
                            </div>
                        </div>                        
                    </div>    
                    <div class="d-flex text-end">
                        @if ($user->id !== $message->user_owner_id && $message->status === App\Models\DiscussionMessage::STATUS_UNREAD) 
                            <div class="btn-group" role="group">
                                <button id="dropdownMenuButton" type="button" class="btn btn-blue btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                    Не прочитано
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <li><a class="dropdown-item" href="{{ route('discussions.read', $message) }}">Прочитано</a></li>
                                </ul>
                            </div>
                        @elseif ($message->status === App\Models\DiscussionMessage::STATUS_READ)
                            <div class="d-block text-end">
                                <div class="d-block fw-bold text-muted small">Прочитано</div>
                                <div class="d-block fw-bold text-muted small">{{ $message->updated_at->format('d.m.Y, m:h') }}</div>
                            </div>
                        @endif
                    </div>                   
                </div>                
                <div>{{ $message->text }}</div>
                @if ($message->filenames)
                    <div class="mt-2">
                        @foreach ($message->filesUrls as $key => $url)
                            <div class="mb-1 d-flex align-items-center">
                                <a href="{{ $url }}" target="_blank"><i class="icon-xl fa fa-file-download"></i> {{ $key }}</a>
                            </div>
                        @endforeach
                    </div>
                @endif
                <hr>
            @endforeach
        </div>
        @if ($discussion->is_alert)     
            <div class="content mt-5">
                <form id="kt_inbox_reply_form" action="{{ route('discussions.send', $discussion) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="d-block">
                        <textarea placeholder="Введите сообщение ..." style="height: 100px;" name="text" class="form-control p-10" id="text"></textarea>
                    </div>
                    @error('text')
                        <span class="label label-light-danger font-weight-bold label-inline">{{ $message }}</span>
                    @enderror
                    <div class="d-flex align-items-center mt-5">                       
                        <div class="mb-3">
                            <input type="file" class="form-control custom-file-input" id="customFile" name="files[]" multiple>
                        </div>
                        @error('files.*')
                            <span class="label label-light-danger font-weight-bold label-inline">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-blue btn-shadow font-weight-bold px-6">Отправить</button>
                    </div>
                </form>
            </div>
        @endif
    </div>

@endsection

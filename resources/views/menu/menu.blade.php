<nav class="navbar navbar-expand-xl navbar-light p-0">
    <div class="container-fluid d-flex align-items-baseline">
        <a class="navbar-brand" href="/">
            <!--<img src="{{ setting('site.logo') }}" alt="logo" height="82px">-->
            <img class="logo" src="/images/new_logo.png" alt="logo" >
        </a>
        <div class="justify-content-end">
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="offcanvas offcanvas-end flex-grow-1" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel">
                    @auth
                        {{ auth()->user()->name }} {{ auth()->user()->firstname }}
                    @else
                        Меню
                    @endauth
                </h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body justify-content-between align-items-center">
                {{ menu('site') }}
                @auth
                    @if (in_array(auth()->user()->role->name, ['admin', 'operator']) && auth()->user()->is_confirmed && !auth()->user()->two_factor_code)
                        {{ menu('operator') }}
                    @elseif(auth()->user()->is_confirmed && !auth()->user()->two_factor_code)
                        {{ menu('user') }}
                    @endif
                @endauth
                @auth
                    @if (!auth()->user()->two_factor_code && auth()->user()->is_confirmed)
                    <ul class="navbar-nav justify-content-end mt-0 align-items-center ml-3">

                        @php
                            $newMessagesCount = auth()->user()->newMessagesCount;
                            $notifications = auth()->user()->notificationsCount;
                            $invites = auth()->user()->invitesCount;
                            $newDiscussionsMessagesCount = auth()->user()->newDiscussionsMessagesCount;
                        @endphp
                        @if(auth()->user()->can('browse_admin'))
                            <li class="nav-item my-2"><a class="menu-link mx-3" href="{{ route('voyager.dashboard') }}"> {{ auth()->user()->name }} {{ auth()->user()->firstname }}</a></li>
                        @elseif (in_array(auth()->user()->role->name, ['operator']))
                            <li class="nav-item my-2"><a class="menu-link mx-3" href="{{ route('voyager.dashboard') }}">{{ auth()->user()->name }} {{ auth()->user()->firstname }}</a></li>
                        @endif
                        @if (!in_array(auth()->user()->role->name, ['admin', 'operator']))
                            <li class="nav-item my-2"><a class="menu-link mx-3" href="{{ route('profile.index') }}">{{ auth()->user()->name }} {{ auth()->user()->firstname }}</a></li>
                        @endif
                        <li class="nav-item my-2">
                            <a class="menu-link d-flex mx-3" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Выйти
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    <div class="row align-items-center text-center my-2">
                        <div class="col-12">
                            <a class="btn position-relative btn-outline-blue btn-menu mx-1" href="{{ route('notices.index') }}" alt="Уведомления" data-toggle="tooltip" data-placement="bottom" title="Уведомления">
                                <i class="fa fa-bell"></i>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill @if($notifications > 0) badge-corall @else badge-outline-blue @endif">@if($notifications > 0) {{ $notifications }} @elseif($notifications > 99) 99+ @else {{ $notifications }} @endif</span>
                            </a>
                            <a class="btn position-relative btn-outline-blue btn-menu mx-1" href="{{ route('mesages.index') }}" alt="Сообщения" data-toggle="tooltip" data-placement="bottom" title="Комментарии">
                                <i class="fa fa-envelope-open"></i>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill @if($newMessagesCount > 0) badge-corall @else badge-outline-blue @endif">@if($newMessagesCount > 0) {{ $newMessagesCount }} @elseif($newMessagesCount > 99) 99+ @else {{ $newMessagesCount }} @endif</span>
                            </a>
                            <a class="btn position-relative btn-outline-blue btn-menu mx-1" href="{{ route('invites.index') }}" alt="Приглашения" data-toggle="tooltip" data-placement="bottom" title="Приглашения">
                                <i class="fa fa-calendar"></i>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill @if($invites > 0) badge-corall @else badge-outline-blue @endif">@if($invites > 0) {{ $invites }} @elseif ($invites > 99) 99+ @else {{ $invites }} @endif</span>
                            </a>
                            <a class="btn position-relative btn-outline-blue btn-menu mx-1" href="{{ route('discussions.index') }}" alt="Обращения" data-toggle="tooltip" data-placement="bottom" title="Обращения">
                                <i class="fa fa-comments"></i>
                                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill @if($newDiscussionsMessagesCount > 0) badge-corall @else badge-outline-blue @endif">@if($newDiscussionsMessagesCount > 0) {{ $newDiscussionsMessagesCount }} @elseif ($newDiscussionsMessagesCount > 99) 99+ @else {{ $newDiscussionsMessagesCount }} @endif</span>
                            </a>
                        </div>
                    </div>
                    @else
                        <ul class="navbar-nav justify-content-end mt-0 align-items-center ml-3">
                        <li class="nav-item my-0">
                            <a class="position-relative btn-menu mx-1" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Сменить аккаунт
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                        </ul>
                    @endif
                @else
                    <ul class="navbar-nav justify-content-end mt-0 align-items-center">
                        <button class="d-block btn btn-corall px-5 rounded-pill mx-3 my-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvaslogin" aria-controls="offcanvaslogin">Вход</button>
                    </ul>
                @endauth
            </div>
        </div>
    </div>
</nav>

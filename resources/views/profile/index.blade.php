@extends('layouts.site')

@section('title')
  Профиль пользователя
@endsection

@section('content')
    <div class="container">
        <h1 class="mb-4">Профиль пользователя</h1>
        <div class="content p-4 shadow-sm">
            <h4>{{ ucwords(Auth::user()->firstname) }}&nbsp;{{ ucwords(Auth::user()->name) }}&nbsp;{{ ucwords(Auth::user()->patronymic) }}</h4>
            <ul class="nav nav-pills corall " id="myTab" role="tablist">
                <li class="nav-item mb-3">
                    <button class="nav-link active" id="personal-tab" data-bs-toggle="pill" data-bs-target="#personal" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Персональные данные</button>
                </li>
                <li class="nav-item mb-3">
                    <button class="nav-link" id="pasport-tab" data-bs-toggle="pill" data-bs-target="#pasport" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Паспортные данные</button>
                </li>
                <li class="nav-item mb-3">
                    <button class="nav-link" id="organization-tab" data-bs-toggle="pill" data-bs-target="#organization" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Организация</button>
                </li>
                <li class="nav-item mb-3">
                <button class="nav-link" id="password-tab" data-bs-toggle="pill" data-bs-target="#password" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Смена пароля</button>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane col-md-12 fade show active" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                    <p>{{ Auth::user()->bio }}</p>
                    <form class="mfcb" method="POST" action="{{ route('profile.update') }}">
                        {{ csrf_field() }}
                        <div class="row mb-3">
                            <div class="form-group col-md-4">
                                <label class="form-label">Фамилия</label>
                                <input type="text" class="form-control" placeholder="Фамилия"
                                    name="firstname" value="{{ $user->firstname }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Имя</label>
                                <input type="text" class="form-control" placeholder="{{ ucwords(Auth::user()->name) }}" name="name"
                                    value="{{ $user->name }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Отчество</label>
                                <input type="text" class="form-control" placeholder="Отчество"
                                    name="patronymic" value="{{ $user->patronymic }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-md-2">
                                <label class="form-label">Пол</label>
                                <select type="select" class="form-control" placeholder="Пол" name="sex">
                                    <option value="{{ \App\Models\User::SEX_MALE }}"
                                            @if($user->sex === \App\Models\User::SEX_MALE) selected @endif>
                                        Мужской
                                    </option>
                                    <option value="{{ \App\Models\User::SEX_FEMALE }}"
                                            @if($user->sex === \App\Models\User::SEX_FEMALE) selected @endif>
                                        Женский
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label">Дата рождения</label>
                                <input type="date" class="form-control" name="birthday" placeholder="Дата рождения"
                                    value="{{ \Carbon\Carbon::parse($user->birthday)->format('Y-m-d') }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Телефон</label>
                                <input type="tel" class="mask-phone form-control" placeholder="+7-XXX-XXX-XX-XX" name="phone" value="{{ $user->phone }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Email</label>
                                <input type="email" class="form-control" placeholder="{{ Auth::user()->email }}" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-md-12">
                                <label class="form-label">Место рождения</label>
                                <input type="text" class="form-control" placeholder="Место рождения" name="bornplace"
                                    value="{{ $user->bornplace }}">
                            </div>
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn btn-corall rounded-pill px-4">Сохранить</button>
                        </div>

                    </form>
                </div>
                <div class="tab-pane col-md-12 fade" id="pasport" role="tabpanel">
                    <form class="mfcb" method="POST" action="{{ route('profile.update') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row mb-3">
                                    <div class="form-group col-md-3 mb-3">
                                        <label class="form-label">Серия паспорта</label>
                                        <input type="text" class="mask-pas-number form-control" placeholder="Серия паспорта"
                                            name="passport_serial" value="{{ $user->passport_serial }}">
                                    </div>
                                    <div class="form-group col-md-3 mb-3">
                                        <label class="form-label">Номер паспорта</label>
                                        <input type="text" class="mask-pas-seria form-control" placeholder="Номер паспорта"
                                            name="passport_number" value="{{ $user->passport_number }}">
                                    </div>
                                    <div class="form-group col-md-3 mb-3">
                                        <label class="form-label">Дата выдачи</label>
                                        <input type="date" class="form-control" placeholder="01.01.1980"
                                            name="passport_date" value="{{ \Carbon\Carbon::parse($user->passport_date)->format('Y-m-d') }}">
                                    </div>
                                    <div class="form-group col-md-3 mb-3">
                                        <label class="form-label">Код подразделения</label>
                                        <input type="text" class="mask-subdivision form-control" placeholder="000-000"
                                            name="passport_subdivision" value="{{ $user->passport_subdivision }}">
                                    </div>
                                    <div class="form-group col-md-12 mb-3">
                                        <label class="form-label">Кем выдан</label>
                                        <input type="text" class="form-control" placeholder="Кем выдан" name="passport_organization"
                                            value="{{ $user->passport_organization }}">
                                    </div>
                                    <div class="form-group col-md-12 mb-3">
                                        <label class="form-label">Адрес прописки</label>
                                        <input type="text" class="form-control" placeholder="Адрес прописки" name="registration_place"
                                            value="{{ $user->registration_place }}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row mb-3">
                                    <div class="form-group col-md-12 mb-3">
                                        <label class="form-label">ИНН</label>
                                        <input type="text" class="mask-inn form-control" placeholder="ИНН" name="inn"
                                            value="{{ $user->inn }}">
                                    </div>
                                    <div class="form-group col-md-12 mb-3">
                                        <label class="form-label">СНИЛС</label>
                                        <input type="text" class="mask-snils form-control" placeholder="СНИЛС" name="snils"
                                            value="{{ $user->snils }}">
                                    </div>
                                    <div class="form-group col-md-12 mb-3">
                                        <label class="form-label">Гражданство</label>
                                        <input type="text" class="form-control" placeholder="Гражданство" name="citizenship" value="{{ $user->citizenship }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn btn-corall rounded-pill px-4">Сохранить</button>
                        </div>

                    </form>
                </div>
                <div class="tab-pane col-md-12 fade" id="organization" role="tabpanel" aria-labelledby="organization-tab">

                    <form class="mfcb" method="POST" action="{{ route('profile.update') }}">
                        {{ csrf_field() }}
                        <div class="row mb-3">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="company_inn">ИНН (для ООО, АО)</label>
                                <input type="text" class="mask-company-inn form-control" id="company_inn" placeholder="ИНН" name="company_inn"
                                    value="{{ $user->company_inn }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="company_ogrn">ОГРН (ОРГНИП)</label>
                                <input type="text" class="mask-company-ogrn form-control" id="company_ogrn" placeholder="ОГРН (ОГРНИП)" name="company_ogrn"
                                    value="{{ $user->company_ogrn }}">
                            </div>
                            <!--<div class="form-group col-md-3">
                                <label class="form-label" for="company_kpp">КПП</label>
                                <input type="text" class="mask-company_kpp form-control" id="company_kpp" placeholder="КПП" name="company_kpp"
                                    value="{{ $user->company_kpp }}">
                            </div>
                            <div class="form-group col-md-3">
                                <label class="form-label" for="organization_date">Дата регистрации</label>
                                <input type="date" class="form-control" id="organization_date" placeholder="01.01.1945"
                                    name="organization_date" value="{{ \Carbon\Carbon::parse($user->organization_date)->format('Y-m-d') }}">
                            </div>-->
                        </div>
                        <div class="row mb-3">
                            <div class="form-group col-md-12 mb-3">
                                <label class="form-label" for="company_name">Полное наименование</label>
                                <input type="text" class="form-control" id="company_name" aria-describedby="info" placeholder="Полное наименование" name="company_name" value="{{ $user->company_name }}">
                            </div>
                            <!--<div class="form-group col-md-6 mb-3">
                                <label class="form-label" for="company_adress">Юридический адрес</label>
                                <input type="text" class="form-control" id="company_adress" placeholder="Юридический адрес" name="company_adress" value="{{ $user->company_adress }}">
                            </div>
                            <div class="form-group col-md-6 mb-3">
                                <label class="form-label" for="company_ceo">Должность руководителя</label>
                                <input type="text" class="form-control" id="company_ceo" placeholder="Генеральный диретор / Директор" name="company_ceo" value="{{ $user->company_ceo }}">
                            </div>
                            <div class="form-group col-md-6 mb-3">
                                <label class="form-label" for="company_ceo_fio">ФИО Руководителя</label>
                                <input type="text" class="form-control" id="company_ceo_fio"
                                    placeholder="Фамилия Имя Отчество" name="company_ceo_fio" value="{{ $user->company_ceo_fio }}">
                            </div>-->
                        </div>

                        <div class="text-end">
                            <button type="submit" class="btn btn-corall rounded-pill px-4">Сохранить</button>
                        </div>

                    </form>

                </div>
                <div class="tab-pane col-md-12 fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                    Для изменнеия пароля воcпользуйтесь формой сброса пароля. Вам потребуется указать email учетной записи, на который придет сслыка с инструкцией.
                    <div class="mt-3 text-end">
                        <a href="{{ route('password.request') }}" class="btn btn-corall rounded-pill px-4">Сброс пароля</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    $('.mask-phone').mask('+7 (999) 999-99-99');
    $('.mask-subdivision').mask('999-999');
    $('.mask-pas-number').mask('9999');
    $('.mask-pas-seria').mask('999999');
    $('.mask-snils').mask('999-999-999 99');
    $('.mask-inn').mask('999999999999');
    $('.mask-company-inn').mask('9999999999999');
    $('.mask-company-ogrn').mask('9999999999');
    $('.mask-company_kpp').mask('999999999');
</script>

<script>
    $(document).ready(function() {
        $("#show_hide_password_3 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_3 input').attr("type") == "text"){
                $('#show_hide_password_3 input').attr('type', 'password');
                $('#show_hide_password_3 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_3 i').removeClass( "fa-eye" );
            }
            else if($('#show_hide_password_3 input').attr("type") == "password"){
                $('#show_hide_password_3 input').attr('type', 'text');
                $('#show_hide_password_3 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_3 i').addClass( "fa-eye" );
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#show_hide_password_4 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_4 input').attr("type") == "text"){
                $('#show_hide_password_4 input').attr('type', 'password');
                $('#show_hide_password_4 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_4 i').removeClass( "fa-eye" );
            }
            else if($('#show_hide_password_4 input').attr("type") == "password"){
                $('#show_hide_password_4 input').attr('type', 'text');
                $('#show_hide_password_4 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_4 i').addClass( "fa-eye" );
            }
        });
    });
</script>
@stop
@section('toasts')
    @if(session('profile_update') == 'success')
        @include('toasts.profile_toast_success')
    @elseif(session('profile_update') == 'failure')
        @include(('toasts.profile_toast_error'))
    @endif
@endsection

@extends('layouts.site')

@php
    $formData = $ticket->formResult->result;
    $formData = is_array($formData) ? $formData : json_decode($formData, true);

    $form = $ticket->formResult->form->form_data;
    $form = is_array($form) ? $form : json_decode($form, true);
    $form = isset($form[0][0]) ? array_reduce($form, function ($a, $c) {
        return array_merge($a, $c);
    }, []) : $form;
@endphp

@section('title')
    Заявка №{{ $ticket->id }}
@endsection

@section('toasts')
	@if(session('comment') === 'success')
		<div class="toast fade show bg-success" role="status" data-bs-autohide="true" data-bs-delay="2000" aria-live="polite" aria-atomic="true">
            <div class="toast-header">
				<strong class="me-auto">Комментарий добавлен</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body text-white">
				Комментарий успешно добавлен. Дождитесь ответа оператора.
			</div>
        </div>
	@endif
@endsection

@section('content')
    <div class="container ticket-detail">
		<div class="mb-4 d-flex align-items-center justify-content-between">
			<div class="title">
				<h1>{{ $ticket->formResult->form->service->name }}</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="content p-4 shadow-sm">
					<div class="d-flex justify-content-between">
						<div class="text-muted fs-6">
							<small>№{{ $ticket->id }} от {{ $ticket->created_at ? $ticket->created_at->format('d.m.Y, H:i'): 'Неизвестно' }}</small>
						</div>
						<div class="mb-2 text-end">
							<span class="badge rounded-pill badge-corall w-auto">{{ __("ticket.status.$ticket->status") }}</span>
						</div>
					</div>
					<small>{{ $ticket->formResult->form->service->official_name }}</small>
					<ul class="nav nav-pills corall" id="profileTab" role="tablist">
            			<li class="nav-item mb-3">
                			<button class="nav-link active" id="anketa-tab" data-bs-toggle="pill" data-bs-target="#anketa" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Анкетные данные</button>
            			</li>
            			<li class="nav-item mb-3">
							<button class="nav-link" id="personal-tab" data-bs-toggle="pill" data-bs-target="#personal" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Данные заявителя</button>
            			</li>
            		</ul>
					<div class="tab-content" id="profileTabContent">
            			<div class="tab-pane col-md-12 fade show active" id="anketa" role="tabpanel" aria-labelledby="anketa-tab">
					        <div class="row mb-4">
					            <div class="col-12">
									<div class="accordion accordion-flush" id="accordionFlushExample">
                                        @foreach($form['steps'] as $stepKey => $step)
                                            @foreach($step['data'] as $rowKey => $row)
                                                @foreach($row['cols'] as $colKey => $col)
                                                    @foreach($col['data'] as $key => $field)
														@if (isset($field['value']))
															<div class="accordion-item">

																<h2 class="accordion-header" id="flush-heading-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}">
																	<button class="accordion-button collapsed
																		@if(!isset($formData[$field['name']]))
																			text-danger
																		@endif"
																		type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}" aria-expanded="false" aria-controls="flush-collapse-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}">
																		{{ $field['title'] }}
																	</button>
																</h2>
																@if (isset($formData[$field['name']]))
																	<div id="flush-collapse-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}" class="accordion-collapse collapse" aria-labelledby="flush-heading-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}" data-bs-parent="#accordionFlushExample">
																		<div class="accordion-body">
																			@php
																			$res = [];
																			if (isset($field['options'])) {
																				$res = collect($formData[$field['name']])
																					->map(function ($val) use ($field) {

																						$r = collect($field['options'])
																							->first(function ($v) use ($val) {
																								return $v['value'] === $val;
																							});
																						return $r['name'];
																					})
																					->toArray();
																			}
																			@endphp
																			@if (isset($field['options']))
																				@foreach($res as $opt)
																					<p class="user-selected-options" style="margin-bottom: 1rem;">{{ $opt }}</p>
																				@endforeach
																			@else
																				@if (isset($field['type']) && $field['type'] === 'file')
																					@foreach($formData[$field['name']] as $file)
																						<p class="user-file" style="margin-bottom: 1rem;">
																							<a href="/storage/{{ $file['path'] }}" target="_blank">{{ $file['name'] }}</a>
																						</p>
																					@endforeach
																				@else
																					{{ $formData[$field['name']] }}
																				@endif
																			@endif
																		</div>
																	</div>
																@else
																	<div id="flush-collapse-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}" class="accordion-collapse collapse" aria-labelledby="flush-heading-{{ $key }}-{{ $colKey }}-{{ $rowKey }}-{{ $stepKey }}" data-bs-parent="#accordionFlushExample">
																		<div class="accordion-body">Не заполнено</div>
																	</div>
																@endif
																</div>
														@endif
													@endforeach
												@endforeach
											@endforeach
										@endforeach
									</div>
					            </div>
					        </div>
				    	</div>
            			<div class="tab-pane col-md-12 fade" id="personal" role="tabpanel"
                		aria-labelledby="personal-tab">
					        <div class="table-responsive">
								<table class="table table-bordered">
									<tr>
										<td>ФИО Заявителя</td>
										<td>{{ $ticket->formResult->user->firstname }} {{ $ticket->formResult->user->name }} {{ $ticket->formResult->user->patronymic }}</td>
									</tr>
									<tr>
										<td>Дата рождения</td>
										<td>{{ \Carbon\Carbon::parse($ticket->formResult->user->birthday)->format('d.m.Y') }}</td>
									</tr>
									<tr>
										<td>ИНН</td>
										<td>{{ $ticket->formResult->user->inn }}</td>
									</tr>
									<tr>
										<td>СНИЛС</td>
										<td>{{ $ticket->formResult->user->snils }}</td>
									</tr>
									<tr>
										<td>Гражданство</td>
										<td>{{ $ticket->formResult->user->citizenship }}</td>
									</tr>
									<tr>
										<td>Серия паспорта</td>
										<td>{{ $ticket->formResult->user->passport_serial }}</td>
									</tr>
									<tr>
										<td>Номер паспорта</td>
										<td>{{ $ticket->formResult->user->passport_number }}</td>
									</tr>
									<tr>
										<td>Орган выдавший паспорт</td>
										<td>{{ $ticket->formResult->user->passport_organization }}</td>
									</tr>
									<tr>
										<td>Код подразделения</td>
										<td>{{ $ticket->formResult->user->passport_subdivision }}</td>
									</tr>
									<tr>
										<td>Дата выдачи паспорта</td>
										<td>{{ \Carbon\Carbon::parse($ticket->formResult->user->passport_date)->format('d.m.Y') }}</td>
									</tr>
									<tr>
										<td>Адрес регистрации</td>
										<td>{{ $ticket->formResult->user->registration_place }}</td>
									</tr>
									<tr>
										<td>Телефон</td>
										<td>{{ $ticket->formResult->user->phone }}</td>
									</tr>
									<tr>
										<td>E-mail</td>
										<td>{{ $ticket->formResult->user->email }}</td>
									</tr>
								</table>
							</div>
					    </div>
				    </div>
			    </div>
			    <div class="content mt-4 p-4 shadow-sm">
					<h5>Комментарии</h5>
					@forelse($ticket->comments as $comment)
						<div class="mb-4 comment">
							<div class="mb-2">@if($comment->isNew) <span class="badge rounded-pill badge-blue w-auto">Новый</span> @endif</div>
							<div class="d-flex justify-content-between">
								<div class="text-muted fs-6"><small>{{ $comment->created_at->format('d.m.Y H:i') }}</small></div>
								<div class="text-end mb-3">
									<div><small>{{ $comment->user->firstname }} {{ $comment->user->name }} {{ $comment->user->patronymic }}</small></div>
								</div>
							</div>
							<div class="d-block">
								<div class="d-block comment-text mb-3">
									{!! $comment->text !!}
								</div>
								<div class="d-block">
									<div class="comment-attachments">
										@if($comment->attachments)
											<p>Вложенные файлы:</p>
											@foreach($comment->attachments as $attachment)
												<p>
													<a href="{{ asset("storage/{$attachment['path']}") }}" target="_blank">
														<i class="fa fa-download"></i> {{ $attachment['name'] }}
													</a>
												</p>
											@endforeach
										@endif
									</div>
								</div>
							</div>
						</div>
						<hr>
					@empty
						<p>Пока нет комментариев по текущей заявке.</p>
					@endforelse
				</div>
				<div class="content mt-4 p-4 shadow-sm">
			        <div class="row mb-4">
			            <div class="col-md-6">
			                <h5>Добавить комментарий</h5>
			                <form action="{{ route('ticket.comment.new', $ticket) }}" method="POST" enctype="multipart/form-data">
			                    {{ csrf_field() }}
			                    <textarea name="text" class="form-control mb-3" id="comment" required></textarea>

								<div class="mb-3">
									<input class="form-control custom-file-input" type="file" id="customFile" name="attachment[]" multiple>
								</div>
								<div class="text-end">
			                    	<button type="submit" class="btn btn-blue rounded-pill px-4">Отправить</button>
								</div>
			                </form>
			            </div>
			            <div class="col-md-6">
			                <div class="bs-callout bs-callout-coral" id="callout-alerts-no-default">
			                    <h4>Обработка комментариев</h4>
			                    <p>Уважаемый пользователь, комментарии обрабатываются операторами в течение 24 часов. После получения ответа от оператора Вам придет уведомление в личный кабинет. Пожалуйста, не дублируйте комментарии.</p>
			                </div>
			            </div>
			        </div>
			    </div>
	    	</div>
	    	<div class="col-md-3">
				@if($ticket->status === \App\Models\Ticket::STATUS_INVITE && !is_null($ticket->invite))
					<div class="side card mb-3 p-4 shadow-sm">
						<div class="card-header">
							<h5>Приглашение</h5>
						</div>
						<div class="card-body">
							<p>Дата визита {{ $ticket->invite->invite_time->format('d.m.Y') }}</p>
							<p>Время визита {{ $ticket->invite->invite_time->format('H:i') }}</p>
							<p>{{ $ticket->invite->window->name }}</p>
						</div>
					</div>
				@endif
		    	<div class="side card mb-3 p-4 shadow-sm">
			    	<div class="card-header">
			    		<h5>История рассмотрения</h5>
		    		</div>
			    	<div class="card-body">
				    	<p>Дата создания<br/> {{ $ticket->created_at ? $ticket->created_at->format('d.m.Y'): 'Неизвестно' }}</p>
				    	<p>Дата изменения<br/> {{ $ticket->updated_at ? $ticket->updated_at->format('d.m.Y'): 'Неизвестно' }}</p>
				    	<p>Дата отправки<br/> {{ $ticket->plan_end_date ? $ticket->plan_end_date->format('d.m.Y'): 'Неизвестно' }}</p>
				    	<p>Дата принятия в работу<br/> {{ $ticket->plan_end_date ? $ticket->plan_end_date->format('d.m.Y'): 'Неизвестно' }}</p>
				    	<p>Плановая дата закрытия<br/> {{ $ticket->plan_end_date ? $ticket->plan_end_date->format('d.m.Y'): 'Неизвестно' }}</p>
				    	<p>Фактическая дата закрытия<br/>
				    		@if (!is_null($ticket->fact_end_date))
	                            {{ $ticket->fact_end_date->format('d.m.Y') }}
	                        @endif
	                    </p>
	                </div>
		    	</div>
		    </div>
	    </div>
	</div>

    <script>
      $('input[type="file"]').on('change', function (event) {
        var fileList = Array.from(event.target.files).map(f => f.name).join(', ');
        $(event.target).siblings('label').text(fileList);
      });
    </script>

    <style>
        .ticket-detail .custom-file-label::after {
            content: 'Выбрать';
        }

        .comment.new .card-header {
            background: var(--success);
        }
    </style>
@endsection

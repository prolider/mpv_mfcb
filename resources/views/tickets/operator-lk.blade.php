@extends('layouts.site')

@section('title')
    Список заявок
@endsection

@section('content')
    <div class="container-fluid">
        <div id="app"></div>
    </div>
@endsection

@section('js')
    <script>
        window.affiliates = {!! json_encode($affiliates) !!};
        window.tickets = {!! json_encode($tickets) !!};
        window.user = {!! json_encode(auth()->user()) !!};
        window.windows = {!! json_encode($windows) !!};
        window.currentWindow = {!! json_encode(session()->get('window')) !!};
        window.ticketFrom = {!! json_encode($ticketFrom) !!};
        window.colorIndicationCounts = {!! json_encode($colorIndicationCounts) !!};
    </script>
    <script src="{{ asset('js/operator-lk.js') }}"></script>
@endsection

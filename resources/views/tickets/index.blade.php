@extends('layouts.site')

@section('title')
    Мои заявки
@endsection

@section('content')
    <div class="container">
        @if(session('formSend') === 'success')
            <div id="ticketadd-alert" class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    Заявка успешно создана!
            </div>
        @endif
        <h1 class="mb-4">Мои заявки</h1>
        @forelse($tickets as $ticket)
            <div class="mb-4 block p-4 shadow-sm">
                <div class="d-flex justify-content-between">
                    <div class="text-muted fs-6">
                        <small>№{{ $ticket->id }} от {{ $ticket->created_at ? $ticket->created_at->format('d.m.Y, H:i'): 'Неизвестно' }}</small>
                    </div>
                    <div class="mb-2 text-end">
                        <span class="badge rounded-pill badge-corall w-auto">{{ __("ticket.status.$ticket->status") }}</span>
                    </div>
                </div>
                <div class="d-flex mb-3">
                    @if ($ticket->status === \App\Models\Ticket::STATUS_DRAFT || $ticket->status === \App\Models\Ticket::STATUS_CLIENT_WRITE)
                        <h5>{{ $ticket->formResult->form->service->name }}</h5>
                    @else
                        <h5><a href="{{ route('ticket.show', $ticket) }}">
                            {{ $ticket->formResult->form->service->name }}
                        </a></h5>
                    @endif
                </div>
                @if (!is_null($ticket->fact_end_date))
                    <div class="d-flex">
                        Дата завершения: {{ $ticket->fact_end_date->format('d.m.Y') }}
                    </div>
                @endif
                @if ($ticket->status === \App\Models\Ticket::STATUS_INVITE && !is_null($ticket->invite))
                    <div class="d-flex">
                        Приглашение назначено на {{ $ticket->invite->invite_time->format('d.m.Y H:i') }} в {{ @$ticket->invite->window->name }} {!! @$ticket->invite->window->affiliate->description !!}
                    </div>
                @endif

                @if ($ticket->status === \App\Models\Ticket::STATUS_NEW)
                    <div class="d-block button-more text-end mb-3">
                        <button value="{{$ticket->id}}" type="button" class="btn btn-corall rounded-pill px-4 popup-open" data-bs-toggle="modal" data-bs-target="#cancelmodal">Запросить отмену</button>
                        <!-- Modal -->
                        <div class="modal fade" id="cancelmodal" tabindex="-1" aria-labelledby="cancelmodallLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Подтвердите запрос на отмену заявки</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        Вы действительно хотите запросить отмену заявки №{{ $ticket->id }} на получение услуги "{{ $ticket->formResult->form->service->name }}"?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-corall rounded-pill px-4" data-bs-dismiss="modal">Отменить</button>
                                        <form  method="POST"  action="{{ route('api.ticket.status', $ticket) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="status" value="{{ \App\Models\Ticket::STATUS_CANCEL_REQUEST }}">
                                            <button type="submit" class="btn btn-coral">Подтвердить</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (in_array($ticket->status, [\App\Models\Ticket::STATUS_DRAFT, \App\Models\Ticket::STATUS_NEED_DATA, \App\Models\Ticket::STATUS_CLIENT_WRITE]))
                    <div class="d-block button-more text-end mb-3">
                        <a class="btn btn-corall rounded-pill px-4" href="{{ route('service.form', [$ticket->formResult->form->service, $ticket]) }}">
                            Редактировать черновик
                        </a>
                    </div>
                @endif
                <div class="d-block button-more text-end">
                    <a href="{{ route('ticket.show', $ticket) }}" class="btn btn-blue rounded-pill px-4">Подробнее</a>
                </div>
            </div>
        @empty
            <div class="content">
                <p>У вас пока нет заявок</p>
            </div>
        @endforelse

        {{ $tickets->links() }}
    </div>

    <script>
      $(function(){
        window.setTimeout(function(){
          $('#ticketadd-alert').alert('close');
        },3000);
      });
    </script>

    <script>
        $(document).ready(function() {
            $('#table_tickets').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.1/i18n/ru.json"
                },
                "order": [[ 0, "desc" ]]
            });
        });
    </script>

@endsection

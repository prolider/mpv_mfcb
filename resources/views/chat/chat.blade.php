<!-- Chat -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvaschat" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasRightLabel">Чат с оператором</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body px-0 pb-0">
 		@auth
			<div class="px-4 py-3 messages" id="messages">
				<div class="user-message text-right pb-2">
					<div class="user-name small">{{ auth()->user()->name }}</div>
					<div class="date small">{{ date('d.m.Y H:i:s') }}</div> 				
					<div class="message">Подскажите, как оформить Устав для ООО?</div>
					<hr>
				</div>
				<div class="operator-message pb-2">
					<div class="operator-name small">Трифонов Михаил Сергеевич</div>
					<div class="date small">19.10.2020 15:28</div> 
					<div class="message">Перейдите в раздел "Начало бизнеса" и выберите услугу открытие ООО.</div>
					<hr>			
				</div>
				<div class="user-message text-right pb-2">
					<div class="user-name small">Максимов Алексей Юрьевич</div>
					<div class="date small">19.10.2020 15:27</div> 				
					<div class="message">Подскажите, как оформить Устав для ООО?</div>
					<hr>
				</div>
				<div class="operator-message pb-2">
					<div class="operator-name small">Трифонов Михаил Сергеевич</div>
					<div class="date small">19.10.2020 15:28</div> 
					<div class="message">Перейдите в раздел "Начало бизнеса" и выберите услугу открытие ООО.</div>
					<hr>			
				</div>
				<div class="user-message text-right pb-2">
					<div class="user-name small">Максимов Алексей Юрьевич</div>
					<div class="date small">19.10.2020 15:27</div> 				
					<div class="message">Подскажите, как оформить Устав для ООО?</div>
					<hr>
				</div>
				<div class="operator-message pb-2">
					<div class="operator-name small">Трифонов Михаил Сергеевич</div>
					<div class="date small">19.10.2020 15:28</div> 
					<div class="message">Перейдите в раздел "Начало бизнеса" и выберите услугу открытие ООО.</div>
					<hr>			
				</div>
				<div class="user-message text-right pb-2">
					<div class="user-name small">Максимов Алексей Юрьевич</div>
					<div class="date small">19.10.2020 15:27</div> 				
					<div class="message">Подскажите, как оформить Устав для ООО?</div>
					<hr>
				</div>
				<div class="operator-message pb-2">
					<div class="operator-name small">Трифонов Михаил Сергеевич</div>
					<div class="date small">19.10.2020 15:28</div> 
					<div class="message">Перейдите в раздел "Начало бизнеса" и выберите услугу открытие ООО.</div>
					<hr>			
				</div>

			</div>        
			<div class="px-4 py-3 chat">
				<h5>Задать вопрос оператору</h5>
				<div class="mb-3">
					<label for="exampleFormControlInput1" class="form-label">Тема обращения</label>
					<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Тема обращения">
				</div>
				<div class="mb-3">
					<label for="chatmessage" class="form-label">Ваш вопрос оператору</label>
					<textarea class="form-control" id="chatmessage" rows="3" placeholder="Задайте Ваш вопрос оператору" style="height: 80px;"></textarea>
				</div>
				<div class="text-center">
					<button class="btn btn-sm btn-coral" id="" onclick="submit" title="отправить">Отправить</button>
				</div>
			</div>
        @else
            <div class="px-4 py-3">
            	<p>Для использования сервиса, пожалуйста, авторизуйтесь на портале.</p>
            	<div class="col-md-12 justify-content-center">
					<div class="text-center py-3">
						<button class="btn btn-md btn-coral" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvaslogin" aria-controls="offcanvaslogin">
							Войти
						</button>            
					</div>
					<div class="text-center pb-3">
						@if (Route::has('register'))
							<a class="btn btn-brown" href="{{ route('register') }}">Зарегистрироваться</a>
						@endif             
					</div>           
        		</div> 
            </div>
        @endauth
    </div>
</div>
<!-- End Chat -->
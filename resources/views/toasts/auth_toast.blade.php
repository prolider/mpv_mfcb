@auth           
@else
    <div class="toast fade show align-items-start text-dark bg-warning border-0 d-flex" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="true" data-bs-delay="5000">
        <div class="d-flex">
            <div class="toast-body">
                Для использования онлайн сервиса «МФЦБ для Бизнеса» необходимо выполнить <a href="/login">вход</a> в Личный кабинет. Если у вас еще нет личного кабинета, то пройдите <a href="/register">регистрацию</a> удобным способом.
            </div>                           
        </div>
        <button type="button" class="btn-close btn-close-dark me-2 mt-2" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>           
@endauth

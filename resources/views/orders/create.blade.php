@extends('layouts.site')

@section('title')
  Создание талона электронной очереди
@endsection

@section('content')
    <div class="container">
        <div id="app"></div>
    </div>
@endsection

@section('js')
    <script>
        window.store_order = {!! json_encode(route('order.store')) !!};
        window.store_draft = {!! json_encode(route('order.store_draft')) !!};
        window.csrf = {!! json_encode(csrf_token()) !!};
        window.services = {!! json_encode($services) !!};
        window.categories = {!! json_encode($categories) !!};
        window.old_values = {!! json_encode(old()) !!};
        window.errors = {!! json_encode($errors->default->getMessages()) !!};
    </script>
    <script src="{{ asset('js/operator-lk.js') }}"></script>
@endsection
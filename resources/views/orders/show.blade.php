@extends('layouts.site')

@section('title')
  Талон №{{ $order->id }}
@endsection

@section('content')
    <div class="container">
        <div id="app"></div>
    </div>
@endsection

@section('js')
    <script>
        window.order = {!! json_encode($order) !!};
        window.coupon = {!! json_encode($order->coupon) !!};
        window.old_values = {!! json_encode(old()) !!};
        window.errors = {!! json_encode($errors->default->getMessages()) !!};
        window.service = {!! $service !!};
        window.category = {!! $category[0] !!};
        window.affiliate_services = {!! $affiliateServices !!};
        window.categories = {!! json_encode($categories) !!};
        window.services = {!! json_encode($services) !!};
        window.csrf = {!! json_encode(csrf_token()) !!};
        window.update_order = {!! json_encode(route('order.update', $order->id)) !!};
        window.update_draft = {!! json_encode(route('order.update_draft', $order->id)) !!};
    </script>
    <script src="{{ asset('js/operator-lk.js') }}"></script>
@endsection

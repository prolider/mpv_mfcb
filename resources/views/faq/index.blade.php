@extends('layouts.site')

@section('title')
    Часто задаваемые вопросы
@endsection

@section('content')
<!-- FAQ -->
    <div class="container">
        <h1 class="mb-4">Часто задаваемые вопросы</h1>
        <div class="content p-4 shadow-sm">
                @forelse(\App\Models\FaqCategory::ordered()->get() as $category)
                    <div class="mb-3">
                        <h3>
                            {{ $category->name }}
                        </h3>
                        <div class="accordion accordion-flush" id="accordionFlush-category">
                            @forelse($category->faqs as $faq)
                                <div class="accordion-item">
                                    <h3 class="accordion-header" id="flush-heading-faq{{ $faq->id }}">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-faq{{ $faq->id }}" aria-expanded="false" aria-controls="flush-collapse-faq{{ $faq->id }}">        
                                            {{ $faq->name }}
                                        </button>
                                    </h3>
                                    <div id="flush-collapse-faq{{ $faq->id }}" class="accordion-collapse collapse" aria-labelledby="flush-heading{{ $faq->id }}" data-bs-parent="#accordionFlush-category">
                                        <div class="accordion-body">
                                            {!! $faq->description !!}
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="faq-category-empty">Пока нет вопросов-ответов</p>
                            @endforelse
                        </div>
                    </div>
                @empty
                    <p class="faq-categories-empty">Пока нет категорий</p>
                @endforelse     
            </div>    
        </div>
    </div>
<!-- End FAQ -->
@endsection
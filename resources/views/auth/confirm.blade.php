@extends('layouts.site')

@section('title')
    Регистрация на портале
@endsection

@push('style')

@endpush
@section('title')
    Подтверждение входа
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 content p-5 shadow-lg">
                <h4 class="text-center">Подтверждение почты</h4>
                <div class="my-3">
                    <p>На указанную электронную почту было отправлено письмо для подтверждения почты.</p>
                </div>
                <p>Если Вы не получили письмо со ссылкой, повторите отправку.</p>
                <div class="text-center">
                    <div class="text-center mb-3">
                        <form class="d-inline" method="GET" action="{{ route('confirmation') }}">
                            @csrf
                            <button type="submit" class="btn btn-corall px-4 rounded-pill">Выслать письмо еще раз</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

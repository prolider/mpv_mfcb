@extends('layouts.site')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 content p-5 shadow-lg">
                <h4 class="text-center">Аккаунт успешно подтвержден</h4>
                Указанный при регистрации e-mail был успешно подтвержден. Для продолжения работы необходимо войти в аккаунт. 
                <form class="text-center my-4" method="GET" action="{{ route('login') }}">
                    @csrf
                    <button type="submit" class="btn btn-corall px-4 rounded-pill">Вход</button>
                </form>
            </div>
        </div>
    </div>
@endsection

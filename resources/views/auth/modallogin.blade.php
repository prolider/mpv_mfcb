<div class="modal fade" id="login" tabindex="-1" aria-labelledby="loginmodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-light brown">
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body mt-0">
                <div class="justify-content-center my-0 mx-3">
                    <h3 class="text-center" id="loginLabel">Вход в кабинет</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row mt-3">
                            <label for="email" class="col-md-12 col-form-label">Email</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email', 'login') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email', 'login')
                                <span class="invalid-feedback" role="alert">
                                        <small>{{ $message }}</small>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="password" class="col-md-12 col-form-label">Пароль</label>
                            <div class="col-md-12">
                                <div class="input-group" id="show_hide_password_2">
                                    <input id="password" type="password" class="form-control @error('password', 'login') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    <a class="input-group-text btn btn-brown btn-primary" style="min-width: 54px;">
                                        <i class="fa fa-eye-slash"></i>
                                        </a>
                                </div>
                                @error('password', 'login')
                                    <span class="invalid-feedback" role="alert">
                                        <small>{{ $message }}</small>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        Запомнить меня
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-brown btn-md">
                                    Войти
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="modal-footer justify-content-center align-items-center border-light text-center">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        Забыли пароль?
                    </a>
                @endif
                /
                <a href="{{ route('register') }}">
                    Зарегистрироваться
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $('#login').on('shown.bs.modal', function () {
    $('#email').trigger('focus')
});
</script>
<script>
$(document).ready(function() {
$("#show_hide_password_2 a").on('click', function(event) {
event.preventDefault();
if($('#show_hide_password_2 input').attr("type") == "text"){
$('#show_hide_password_2 input').attr('type', 'password');
$('#show_hide_password_2 i').addClass( "fa-eye-slash" );
$('#show_hide_password_2 i').removeClass( "fa-eye" );
}else if($('#show_hide_password_2 input').attr("type") == "password"){
$('#show_hide_password_2 input').attr('type', 'text');
$('#show_hide_password_2 i').removeClass( "fa-eye-slash" );
$('#show_hide_password_2 i').addClass( "fa-eye" );
}
});
});
</script>

@extends('layouts.site')

@section('title')
    Сброс пароля
@endsection

@section('content')
    <div class="container" id="kt_reset">
        <div class="row justify-content-center">
            <div class="col-md-4 content p-5 shadow-sm">
                <h4 class="text-center">Сброс пароля</h4>
                <p>
                    Используйте пароль не менее чем из 8 символов, включающий символы, цифры и буквы в верхнем и нижнем регистре. Не задавайте пароли, использованные ранее. Не применяйте пароли, используемые для других сайтов, и пароли, которые можно легко подобрать.
                </p>
                <div class=" mb-3">
                    <div class="card-body p-5">
                        <form method="POST" action="{{ route('password.update') }}" id="kt_reset_password_form">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group row">
                                <label for="email" class="col-md-12 col-form-label">Email</label>
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-12 col-form-label">Пароль</label>
                                <div class="col-md-12">
                                    <div class="input-group" id="show_hide_password_5">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                            </a>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-12 col-form-label">Повторите пароль</label>
                                <div class="col-md-12">
                                    <div class="input-group" id="show_hide_password_7">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button id="kt_reset_password_submit" type="submit" class="btn btn-corall px-4 rounded-pill">
                                        Сбросить пароль
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $("#show_hide_password_5 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_5 input').attr("type") == "text"){
                $('#show_hide_password_5 input').attr('type', 'password');
                $('#show_hide_password_5 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_5 i').removeClass( "fa-eye" );}
            else if($('#show_hide_password_5 input').attr("type") == "password"){
                $('#show_hide_password_5 input').attr('type', 'text');
                $('#show_hide_password_5 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_5 i').addClass( "fa-eye" );}
        });
    });
</script>

<script>
    $(document).ready(function() {
        $("#show_hide_password_7 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_7 input').attr("type") == "text"){
                $('#show_hide_password_7 input').attr('type', 'password');
                $('#show_hide_password_7 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_7 i').removeClass( "fa-eye" );}
            else if($('#show_hide_password_7 input').attr("type") == "password"){
                $('#show_hide_password_7 input').attr('type', 'text');
                $('#show_hide_password_7 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_7 i').addClass( "fa-eye" );}
        });
    });
</script>


<script src="{{ asset('theme/assets/js/pages/custom/auth/reset.js') }}"></script>
<script src="{{ asset('theme/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/js/scripts.bundle.js') }}"></script>
<link href="{{ asset('theme/assets/css/pages/auth/reset.css') }}" rel="stylesheet" type="text/css">
@endsection

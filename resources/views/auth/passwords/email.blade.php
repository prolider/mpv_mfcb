@extends('layouts.site')

@section('title')
    Восстановление пароля
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 content p-5 shadow-sm">
                <h4 class="text-center">Восстановление пароля</h4>          
                <div class=" my-3">
                    <div class="mb-3">
                        Укажите адрес Вашей электронной почты указанный при регистрации. На него будет отправлено письмо с инструкцией по восстановлению пароля.
                    </div>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <div class="">
                                <input id="email" type="email" placeholder="Введите email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="text-center my-4">
                            <button type="submit" class="btn btn-corall px-4 rounded-pill">
                                Восстановить
                            </button>
                        </div>
                    </form>
                    <div class="text-center mb-3">
                        <a href="{{ route('login') }}">
                            Вход
                        </a>
                    </div>
                    <div class="text-center">
                        @if (Route::has('password.request'))
                            <a href="{{ route('register') }}">
                            Зарегистрироваться
                            </a>
                        @endif                 
                    </div>           
                </div>                         
            </div>
        </div>
    </div>
@endsection

@section('toasts')
    @if (session('status'))
        <div class="toast fade show bg-warning" role="alert" aria-live="assertive" data-autohide="true" data-delay="5000" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Внимание!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                {{ session('status') }}
            </div>                           
        </div> 
    @endif
@endsection

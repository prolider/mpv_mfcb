@extends('layouts.app')

@section('title')
    Подтверждение email адреса
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 content p-5 shadow-sm">
                <div class=" p-5">
                    <div class="card-header">{{ __('Confirm Password') }}</div>
                    <div class="card-body">
                        {{ __('Please confirm your password before continuing.') }}
                        <form method="POST" action="{{ route('password.confirm') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <div class="input-group" id="show_hide_password_6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-corall px-4 rounded-pill">
                                        {{ __('Confirm Password') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
$(document).ready(function() {
$("#show_hide_password_6 a").on('click', function(event) {
event.preventDefault();
if($('#show_hide_password_6 input').attr("type") == "text"){
$('#show_hide_password_6 input').attr('type', 'password');
$('#show_hide_password_6 i').addClass( "fa-eye-slash" );
$('#show_hide_password_6 i').removeClass( "fa-eye" );
}else if($('#show_hide_password_6 input').attr("type") == "password"){
$('#show_hide_password_6 input').attr('type', 'text');
$('#show_hide_password_6 i').removeClass( "fa-eye-slash" );
$('#show_hide_password_6 i').addClass( "fa-eye" );
}
});
});
</script>
@endsection
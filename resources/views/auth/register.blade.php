@extends('layouts.site')

@section('title')
    Регистрация на портале
@endsection

@push('style')

@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center" >
            <div class="col-md-5 content justify-content-center p-5 shadow-sm" id="kt_login">
                <h4 class="text-center">Регистрация</h4>
                <div class="mb-3">
                    <div class="p-2">
                        <form method="POST" action="{{ route('register') }}" class="form fv-plugins-bootstrap fv-plugins-framework" novalidate="novalidate" id="kt_login_signup_form">
                            @csrf
                            <div class="form-group row mt-3">
                                <div class="col-md-6">
                                    <label for="name" class="col-form-label">Имя</label>
                                    <input id="name" type="text" placeholder="Ваше имя" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="firstname" class="col-form-label">Фамилия</label>
                                    <input id="firstname" type="text" placeholder="Ваша фамилия" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                                    @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <div class="col-md-6">
                                    <label for="email" class="col-form-label">Email</label>
                                    <input id="email" type="email" placeholder="Электронная почта" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="phone" class="col-form-label">Телефон</label>
                                    <input id="phone" type="text" placeholder="Номер без +7" class="mask-phone form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="password" class="col-md-12 col-form-label">Пароль</label>
                                <div class="col-md-12">
                                    <div class="input-group" id="show_hide_password_3">
                                        <input id="password" type="password" placeholder="Пароль" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="password-confirm" class="col-md-12 col-form-label">Повторите пароль</label>
                                <div class="col-md-12">
                                    <div class="input-group" id="show_hide_password_4">
                                        <input id="password-confirm" type="password" placeholder="Пароль" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="policy" required id="policy" {{ old('policy') ? 'checked' : '' }}>
                                        <label class="form-check-label small" for="policy">
                                            Нажимая на кнопку «Зарегистрироваться», вы соглашаетесь с <a href="{{ route('policy') }}">Политикой конфиденциальности</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row text-center mt-4">
                                <div class="col-md-12">
                                    <button type="submit" id="kt_login_signup_submit" class="btn btn-corall px-4 rounded-pill">
                                        Зарегистрироваться
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center mb-3">
                    <a href="{{ route('login') }}">
                        Вход
                    </a>
                </div>
                <div class="text-center">
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            Восстановить пароль
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>
	$('.mask-phone').mask('+7 (999) 999-99-99');
</script>

<script>
    $(document).ready(function() {
        $("#show_hide_password_3 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_3 input').attr("type") == "text"){
                $('#show_hide_password_3 input').attr('type', 'password');
                $('#show_hide_password_3 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_3 i').removeClass( "fa-eye" );}
            else if($('#show_hide_password_3 input').attr("type") == "password"){
                $('#show_hide_password_3 input').attr('type', 'text');
                $('#show_hide_password_3 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_3 i').addClass( "fa-eye" );}
        });
    });
</script>

<script>
    $(document).ready(function() {
        $("#show_hide_password_4 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_4 input').attr("type") == "text"){
                $('#show_hide_password_4 input').attr('type', 'password');
                $('#show_hide_password_4 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_4 i').removeClass( "fa-eye" );}
            else if($('#show_hide_password_4 input').attr("type") == "password"){
                $('#show_hide_password_4 input').attr('type', 'text');
                $('#show_hide_password_4 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_4 i').addClass( "fa-eye" );}
        });
    });
</script>

<script src="{{ asset('theme/assets/js/pages/custom/auth/register.js') }}"></script>
<script src="{{ asset('theme/assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('theme/assets/js/scripts.bundle.js') }}"></script>
<link href="{{ asset('theme/assets/css/pages/auth/reset.css') }}" rel="stylesheet" type="text/css">
@endsection

@extends('layouts.site')

@section('title')
    Подтверждение Email адреса
@endsection

@section('content')
<div class="container content p-5 shadow-sm">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="text-center">
                <p>Подтверждение Email адреса</p>
            </div>            
            <div class="card mb-3">
                <div class="card-body p-5">
                    <p>На Вашу почту было отправлено письмо со ссылкой на подтверждение Вашей электронной почты. 
                        Пожалуйста, завершите рагистрацию переходом по ссылке из письма. 
                    </p>
                    <p>Если Вы не получили письмо со ссылкой,</p>
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-corall px-4 rounded-pill">Отправить повторно</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('toasts')
    @if (session('resent'))
        <div class="toast fade show bg-warning" role="alert" aria-live="assertive" data-autohide="true" data-delay="5000" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Внимание!</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Новая ссылка для подтверждения была успешно отправлена на Ваш email указанный при регистрации
            </div>                           
        </div> 
    @endif
@endsection

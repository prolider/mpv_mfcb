@extends('layouts.site')

@section('title')
    Подтверждение входа
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 content p-5 shadow-lg">
                <h4 class="text-center">Подтверждение входа</h4>
                <div class="my-3">
                    <p>На указанную электронную почту было отправлено письмо с проверочным кодом. Пожалуйста, введите
                        код из письма и нажмите кнопку "Подтвердить". Код действителен в течение 10 минут.</p>
                </div>
                <div class="mb-3">
                    <form method="POST" action="{{ route('verify-authentication.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group mb-3">
                            <label for="email" class="col-form-label">Код подтверждения</label>
                            <input name="two_factor_code" type="text"
                                   class="form-control{{ $errors->has('two_factor_code') ? ' is-invalid' : '' }}"
                                   required autofocus placeholder="Введите код из письма">
                            @if($errors->has('two_factor_code'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('two_factor_code') }}
                                </div>
                            @endif
                        </div>
                        <div class="text-center my-4">
                            <button type="submit" class="btn btn-coral px-4 rounded-pill">
                                Подтвердить
                            </button>
                        </div>
                        <div>
                            <p class="text-muted">
                                Если вы не получили письмо с кодом подтверждения на указанную электронную почту,
                                пожалуйста, проверьте корректность введенный данных или воспользуйтесь ссылкой для <a
                                    href="{{ route('verify-authentication.resend') }}"> повторной отправки проверочного кода
                                    </a>.
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('toasts')
    @if(session()->has('message'))
    <div class="toast fade show align-items-start text-dark bg-warning border-0 d-flex" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="true" data-bs-delay="5000">
        <div class="d-flex">
            <div class="toast-body">
            {{ session()->get('message') }}
            </div>
        </div>
        <button type="button" class="btn-close btn-close-dark me-2 mt-2" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    @endif
@endsection

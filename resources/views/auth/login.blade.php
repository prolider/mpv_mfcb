@extends('layouts.site')

@section('title')
    Авторизация в личном кабинете
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center mt-3">
            <div class="col-md-4 justify-content-center content p-5 shadow-sm">
                <h4 class="text-center">Вход в кабинет</h4>
                <div class="mb-0">
                    <!--<div class="card-header">{{ __('Login') }}</div>-->
                    <div class="p-4">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row mb-3">
                                <label for="email" class="col-md-12 col-form-label">Email</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" placeholder="Введите электронную почту" class="form-control @error('email', 'login') is-invalid @enderror @error('email', 'throttle') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email', 'login')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    @error('email', 'throttle')
                                        <span class="invalid-feedback" role="alert">
                                            <small id="throttle_message">{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <label for="password" class="col-md-12 col-form-label">Пароль</label>

                                <div class="col-md-12">
                                    <div class="input-group" id="show_hide_password_1">
                                        <input id="password" type="password" placeholder="Введите пароль" class="form-control @error('password', 'login') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                            <i class="fa fa-eye-slash"></i>
                                            </a>
                                    </div>
                                    @error('password', 'login')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            Запомнить меня
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row text-center">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-corall px-4 rounded-pill">
                                        Войти
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center mb-3">
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            Восстановить пароль
                        </a>
                    @endif
                </div>
                <div class="text-center">
                    <a href="{{ route('register') }}">
                        Зарегистрироваться
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    function startCountdown(){
        var regexp = /[0-9]+/ig;

        var interval = setInterval(function () {
            var message = $("#throttle_message").text();
            var time = regexp.exec(message);

            if(time > 1) {
                time--;
                $("#throttle_message").text(message.replace(regexp, time));
            } else {
                $("#throttle_message").text('');
                clearInterval(interval);
            }
        }, 1000);
    }

    $(document).ready(function() {
        $("#show_hide_password_1 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_1 input').attr("type") == "text"){
                $('#show_hide_password_1 input').attr('type', 'password');
                $('#show_hide_password_1 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_1 i').removeClass( "fa-eye" );}
            else if($('#show_hide_password_1 input').attr("type") == "password"){
                $('#show_hide_password_1 input').attr('type', 'text');
                $('#show_hide_password_1 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_1 i').addClass( "fa-eye" );}
        });

        startCountdown();
    });
</script>
@endsection

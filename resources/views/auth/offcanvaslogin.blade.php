<div class="offcanvas offcanvas-end @error('email', 'login') show @enderror @error('email', 'throttle') show @enderror @error('password', 'login') show @enderror" tabindex="-1" id="offcanvaslogin" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasRightLabel">Вход в кабинет</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <div class="justify-content-center my-0 mx-3">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group row mt-3">
                    <label for="email" class="col-md-12 col-form-label">Email</label>
                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control @error('email', 'login') is-invalid @enderror @error('email', 'throttle') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email', 'login')
                            <span class="invalid-feedback" role="alert">
                                <small>{{ $message }}</small>
                            </span>
                        @enderror
                        @error('email', 'throttle')
                            <span class="invalid-feedback" role="alert">
                                <small id="throttle_message_canvas">{{ $message }}</small>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <label for="password" class="col-md-12 col-form-label">Пароль</label>
                    <div class="col-md-12">
                        <div class="input-group" id="show_hide_password_2">
                            <input id="password" type="password" class="form-control @error('password', 'login') is-invalid @enderror" name="password" required autocomplete="current-password">
                            <a class="input-group-text btn btn-blue" style="min-width: 54px;">
                                <i class="fa fa-eye-slash"></i>
                                </a>
                        </div>
                        @error('password', 'login')
                            <span class="invalid-feedback" role="alert">
                                <small>{{ $message }}</small>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-md-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3 text-center">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-corall rounded-pill px-4">
                            Войти
                        </button>
                    </div>
                </div>
            </form>
            <div class="text-center mt-3 mb-3">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        Восстановить пароль
                    </a>
                @endif
            </div>
            <div class="text-center">
                <a href="{{ route('register') }}">
                    Зарегистрироваться
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    $('#offcanvaslogin').on('shown.bs.offcanvas', function () {
    $('#email').trigger('focus')
});
</script>
<script>
    function startCountdownCanvas(){
        var regexp = /[0-9]+/ig;

        var interval = setInterval(function () {
            var message = $("#throttle_message_canvas").text();
            var time = regexp.exec(message);

            if(time > 1) {
                time--;
                $("#throttle_message_canvas").text(message.replace(regexp, time));
            } else {
                $("#throttle_message_canvas").text('');
                clearInterval(interval);
            }
        }, 1000);
    }

    $(document).ready(function() {
        $("#show_hide_password_2 a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password_2 input').attr("type") == "text"){
                $('#show_hide_password_2 input').attr('type', 'password');
                $('#show_hide_password_2 i').addClass( "fa-eye-slash" );
                $('#show_hide_password_2 i').removeClass( "fa-eye" );
            }
            else if($('#show_hide_password_2 input').attr("type") == "password"){
                $('#show_hide_password_2 input').attr('type', 'text');
                $('#show_hide_password_2 i').removeClass( "fa-eye-slash" );
                $('#show_hide_password_2 i').addClass( "fa-eye" );
            }
        });

        startCountdownCanvas();
    });
</script>

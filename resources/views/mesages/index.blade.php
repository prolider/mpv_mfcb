@extends('layouts.site')

@section('title')
    Комментарии в заявках
@endsection

@section('content')
    <div class="container">
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <div class="title">    
                <h1>Комментарии в заявках</h1>
            </div>
        </div>          
        @if ($tickets->count() > 0)        
            @foreach($tickets as $ticket)
                <div class="mb-3 block">                    
                    <div class="d-flex justify-content-between">
                        <div class="text-muted fs-6">
                            <small>{{ $ticket->updated_at->format('d.m.Y, m:h') }}</small>
                        </div>
                        <div class="mb-2 text-end">                                
                            @if ($ticket->new_comments_count > 0)                     
                                <span class="badge rounded-pill badge-corall w-auto">Новые сообщения {{ $ticket->new_comments_count }}</span>
                            @else
                                <span class="badge rounded-pill badge-light w-auto">Новые сообщений нет</span>
                            @endif
                        </div>
                    </div>                    
                    <div class="d-block align-items-center">
                        <h5 class="d-block">
                            <a href="{{ route('ticket.show', $ticket) }}">
                                {{ $ticket->formResult->form->service->name }}
                            </a>
                        </h5>
                    </div>
                    <div class="d-block button-more text-end">
                        <a href="{{ route('ticket.show', $ticket) }}" class="btn btn-sm btn-blue">Подробнее <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            @endforeach
        @else
            <div class="content p-4 shadow-sm"> 
                <p>Пока у Вас нет комментариев.</p>
            </div>
        @endif
    </div>
@stop

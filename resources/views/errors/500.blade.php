@extends('layouts.site')

@section('title')
    Внутренняя ошибка сервера
@endsection

@section('content')
    <div class="content p-5 col-4 mx-auto shadow-sm">
        <div class="text-center">
            <div class="my-4"><i class="fa fa-exclamation-triangle fa-4x text-corall" aria-hidden="true"></i></div>
            <h2>Внутренняя ошибка сервера</h2>
            <p>Мы уже работаем над устранением данной ошибки</p>
        </div>
        <div class="text-center">
            <a href="{{ route('index') }}" class="btn btn-corall px-5 rounded-pill mx-3 my-2">На главную</a>
        </div>
    </div>
@endsection
@extends('layouts.site')

@section('title')
    Страница не существует
@endsection

@section('content')
    <div class="content p-5 col-4 mx-auto shadow-sm">
        <div class="text-center">
            <div class="my-4 d-flex align-items-center justify-content-center">
                <span class="error-number">4</span>
                <i class="fa fa-exclamation-circle fa-4x text-corall" aria-hidden="true"></i>
                <span class="error-number">4</span>
            </div>
            <h2>Cтраница не найдена</h2>
            <p>Мы уже работаем над устранением данной ошибки</p>
        </div>
        <div class="text-center">
            <a href="{{ route('index') }}" class="btn btn-corall px-5 rounded-pill mx-3 my-2">На главную</a>
        </div>
    </div>
@endsection
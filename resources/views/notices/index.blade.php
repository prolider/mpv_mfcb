@extends('layouts.site')

@section('title')
  Уведомления
@endsection

@section('content')
  <div class="container">
    <div class="mb-4 d-flex align-items-center justify-content-between">
      <div class="title">
          <h1>Уведомления</h1>
      </div>
    </div>
    @if($notifications->count() > 0)
      @foreach($notifications as $notification)
        <div class="mb-4 content p-4 shadow-sm">
          <div class="d-flex justify-content-between">
            <div class="text-muted fs-6">
                <small>{{ $notification->updated_at->format('d.m.Y, m:h') }}</small>
            </div>
            <div class="mb-2 text-end">
              @if (in_array(auth()->user()->role->name, ['admin', 'operator']))
                <button class="btn btn-outline-corall btn-sm remove-notice" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
              @endif
              <span class="badge rounded-pill badge-blue w-auto">#{{ $notification->notificable_id }}</span>
            </div>
          </div>
          <div class="d-block">
            <h5>
                <a href="{{ route($notification->notificableRoute, $notification->notificable) }}">{{ $notification->name }}</a>
            </h5>
            <p>{{ $notification->notificable->formResult->form->service->name ?? $notification->text}}</p>
            <p>{{ $notification->description }}</p>
          </div>
          <div class="d-block text-end mt-3">
            <a href="{{ route($notification->notificableRoute, $notification->notificable) }}" class="btn btn-sm btn-blue" data-id="{{ $notification->id }}" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Открыть">Подробнее <i class="fa fa-chevron-right"></i></a>
          </div>
        </div>
      @endforeach
    @else
      <div class="content">
        <p>Пока у Вас нет уведомлений.</p>
      </div>
    @endif
  </div>
@stop

@push('javascript')
    <script>
        $('.remove-notice').on('click', function (e) {
          e.preventDefault();
          const id = $(e.currentTarget).data('id');
          $.ajax(`/api/notification/${id}`, {
            method: 'POST',
            data: {
              _method: 'DELETE',
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: (response) => {
              $(e.currentTarget).parent().parent().parent().remove();
              $('meta[name="csrf-token"]').attr('content', response.token);
            }
          })
        })
    </script>
@endpush

@extends('layouts.site')

@section('title')
    Отчет
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mb-4">Отчет</h1>
        <div class="content shadow-sm p-4 mb-4">
            <div class="form-group">
                <label class="form-label">Дата оказания услуги (c, до)</label>
                <div class="row">
                    <div class="col-md-8">
                        <form action="{{ route('report') }}" method="GET" class="row">
                            <div class="col-md-3 mb-4">
                                <input id="date_open" type="date" name="date_open" placeholder="Дата начала" class="form-control" value="{{ request('date_open') }}">
                            </div>
                            <div class="col-md-3 mb-4">
                                <input id="date_close" type="date" name="date_close" placeholder="Дата завершения" class="form-control" value="{{ request('date_close') }}">
                            </div>
                            <div class="col-md-4 mb-4">
                                <button type="submit" class="btn btn-blue rounded-pill px-3">
                                    Применить
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 mb-4 text-end">
                        <form action="{{ route('report-export') }}" method="GET">
                            <input id="hidden_date_open" type="date" name="date_open" class="d-none" value="{{ request('date_open') }}">
                            <input id="hidden_date_close" type="date" name="date_close" class="d-none" value="{{ request('date_close') }}">
                            <button type="submit" class="btn btn-corall rounded-pill px-3">
                                Скачать отчет
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive content shadow-sm p-4">
            @include('reports.tickets')
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#table_tickets').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.1/i18n/ru.json"
                },
                "order": [[ 0, "desc" ]]
            });

            $('#date_open').on('change', function () {
                $('#hidden_date_open').val( $('#date_open').val() );
            });

            $('#date_close').on('change', function () {
                $('#hidden_date_close').val( $('#date_close').val() );
            });
        });
    </script>
@endsection

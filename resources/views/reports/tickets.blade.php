<table id="table_tickets" class="table table-bordered align-middle table-responsive">
    <thead class="thead-dark">
    <tr class="text-center">
        <th>Наименование</th>
        <th>ФИО</th>
        <th>ИНН</th>
        <th>Паспорт</th>
        <th>Телефон</th>
        <th>Email</th>
        <th>Наименование организации</th>
        <th>ИНН организации</th>
        <th>ОГРН</th>
        <th>ОГРНИП</th>
        <th>Наименование услуги</th>
        <!--<th>Вид оказания услуги</th>-->
        <th>Дата оказания услуги</th>
        <th>Время оказания услуги</th>
        <th>Оператор</th>
        <th>Окно</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tickets as $ticket)
        <tr>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->name))
                    {{ optional($ticket->formresult->user)->firstname }} {{ optional($ticket->formresult->user)->name }} {{ optional($ticket->formresult->user)->patronymic }}
                @else
                    Не указан
                @endif
            </td>
            <td>
                @if(isset(optional($ticket->formresult->user)->name))
                    {{ optional($ticket->formresult->user)->firstname }} {{ optional($ticket->formresult->user)->name }} {{ optional($ticket->formresult->user)->patronymic }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->inn))
                    {{ optional($ticket->formresult->user)->inn }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->passport_serial))
                    {{ optional($ticket->formresult->user)->passport_serial }} {{ optional($ticket->formresult->user)->passport_number }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->phone))
                    {{ optional($ticket->formresult->user)->phone }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->email))
                    {{ optional($ticket->formresult->user)->email }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->company_name))
                    {{ optional($ticket->formresult->user)->company_name }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->company_inn))
                    {{ optional($ticket->formresult->user)->company_inn }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->company_ogrn))
                    {{ optional($ticket->formresult->user)->company_ogrn }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if(isset(optional($ticket->formresult->user)->company_ogrnip))
                    {{ optional($ticket->formresult->user)->company_ogrnip }}
                @else
                    Не указан
                @endif
            </td>
            <td class="text-center">
                @if ($ticket->status === \App\Models\Ticket::STATUS_DRAFT || $ticket->status === \App\Models\Ticket::STATUS_CLIENT_WRITE)
                    {{ $ticket->formResult->form->service->name }}
                @else
                    <a href="{{ route('ticket.show', $ticket) }}">
                        {{ $ticket->formResult->form->service->name }}
                    </a>
                @endif
            </td>
            <!--<td class="text-center">
                @if ($ticket->formResult->form->service->use_button)
                    Сопроводительная
                @else
                    Информационная
                @endif
            </td>-->
            <td class="text-center">
                @if (!is_null($ticket->created_at))
                    {{ $ticket->created_at->format('d.m.Y') }}
                @endif
            </td>
            <td class="text-center">
                @if (!is_null($ticket->created_at))
                    {{ $ticket->created_at->format('H:i') }}
                @endif
            </td>
            <td>
                @if(isset(optional($ticket->operator)->name))
                    {{ optional($ticket->operator)->name }} {{ optional($ticket->operator)->firstname }}
                @else
                    Не назначен
                @endif
            </td>
            <td>
                @if(isset(optional($ticket->window)->name))
                    {{ (optional($ticket->window)->name) }}
                @else
                    Не указано
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

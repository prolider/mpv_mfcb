@extends('layouts.site')

@section('content')
    <div class="container">
		<h1  class="mb-4">{{ $service->category->name ?? 'Без категории' }}</h1>
        <div class="row">			
            <div class="col-md-9 col-12">
            	<div class="content shadow-sm p-4">
	                @forelse($services as $service)
	                    <div class="row" style="margin-bottom: 1rem;">
	                        <div class="col-12">
	                            <h4>
									<a href="{{ route('service.detail', $service) }}">
	                               		{{ $service->name }}
									</a>
								</h4>
	                            <div class="button-more text-right">
	                                <a href="{{ route('service.detail', $service) }}" class="btn btn-outline-blue rounded-pill stretched-link px-4">Подробнее</a>
	                            </div>
	                        </div>
	                    </div>
	                    <hr />
	                @empty
					<div class="content shadow-sm p-4">
	                    <h4>Нет услуг в данной категории</h4>
					</div>
	                @endforelse
            	</div>
            </div>
            <div class="col-md-3 col-12">
            	<div class="side card mb-3 p-4 shadow-sm">
	                <div class="right-menu rounded px-3">
	                    @forelse($categories as $category)
	                        <div class="mb-4">
	                            <a class="right-menu-link" href="{{ route('categories.index', ['category_id' => $category->id]) }}">
	                                {{ $category->name }}
	                            </a>
							</div>	                        
	                    @empty
	                        <div>Нет категорий</div>
	                    @endforelse
	                </div>
	            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{ $services->links() }}
            </div>
        </div>
    </div>
@endsection

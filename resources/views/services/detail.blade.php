@extends('layouts.site')

@section('title')
    {{ $service->name }}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 mb-3">
                <div class="content shadow-sm p-5">
                    <div class="d-flex justify-content-between">
                        <div class="text-start">
                            <small class="text-muted mb-3 d-block">Направление: <a href="{{ route('categories.detail', $service->service_category_id) }}">{{ $service->category->name ?? 'Категория не указана' }}</a></small>
                        </div>
                        <div class="text-end">
                            @if($service->use_button === 'Y')
                                <small class="rounded-pill bg-warning text-sm text-dark py-1 px-3">Сопроводительная</small>
                            @elseif ($service->use_button === 'N')
                                <small class="rounded-pill bg-light text-sm text-dark py-1 px-3">Информационная</small>
                            @endif
                        </div>
                    </div>
                    <div class=" my-3">
                        <h3 class="">{{ $service->name }}</h3>
                    </div>
                    <div class="border-bottom border-top mb-4 py-2">
                        <span class="d-block">{{ $service->official_name }}</span>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            {!! $service->shortdescription !!}
                        </div>
                    </div>
                    <div class="card-footer align-items-center border-0 bg-transparent px-0 pt-4">

                        @auth
                            @if (!in_array(auth()->user()->role->name, ['admin', 'operator']))
                                @if (!auth()->user()->inn)
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        Для продолжения работы необходимо корректно заполнить все данные включая ИНН на странице <a href="/profile">Профиль пользователя</a>. После заполнения всех данных, пожалуйста, повторите попытку.
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <div class="button-use text-end">
                                        <a class="btn btn-outline-blue rounded-pill px-4 disabled" href="">Воспользоваться</a>
                                    </div>
                                @elseif (!auth()->user()->is_confirmed)
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    Вам необходимо подтвердить почтовый адрес.
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <div class="button-use text-end">
                                        <a class="btn btn-outline-blue rounded-pill px-4" href="{{ route('confirm', auth()->user()) }}">Подтвердить</a>
                                    </div>
                                @elseif (auth()->user()->two_factor_code)
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        Вам необходимо ввести двухфакторный код.
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <div class="button-use text-end">
                                        <a class="btn btn-outline-blue rounded-pill px-4" href="{{ route('verify-authentication.index', auth()->user()) }}">Ввести код</a>
                                    </div>
                                @else
                                    <div class="button-use text-end">
                                        <a class="btn btn-outline-blue rounded-pill px-4" href="{{ route('service.form', $service) }}">Воспользоваться</a>
                                    </div>
                                @endif
                            @else
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    Установлено ограничение для административных пользователей на заполнению формы услуги.
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            @if (in_array(auth()->user()->role->name, ['admin']))
                                <a class="btn btn-outline-blue rounded-pill px-4 " href="{{ route('service.detail-admin', $service) }}">Посмотреть</a>
                            @endif
                        @else
                            <div class="alert alert-warning alert-dismissible fade show d-block" role="alert">
                                Для получения подробной информации необходимо выполнить вход в личный кабинет
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            <div class="button-use text-end">
                                <div class="button-use text-end">
                                    <button class="btn btn-corall rounded-pill px-4" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvaslogin" aria-controls="offcanvaslogin">
                                        Войти
                                    </button>
                                </div>
                            </div>
                        @endauth
                        </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="side card mb-3 p-4 shadow-sm">
                    <div class="card-header">
                        <h5>Файлы на скачивание</h5>
                    </div>
                    <div class="card-body">
                        @auth
                            @forelse($service->documents as $document)
                                <div class="row row pb-2 mb-2" style="align-items: center; border-bottom: 1px solid #eee;">
                                    <div class="col-1">
                                        <a href="/storage/{{ $document->file }}" download="{{ $document->download_name }}"><i class="fa fa-download"></i></a>
                                    </div>
                                    <div class="col-10">
                                        <a href="/storage/{{ $document->file }}" download="{{ $document->download_name }}">{{ $document->name }}</a>
                                    </div>
                                </div>
                            @empty
                                <p>Нет файлов на скачивание</p>
                            @endforelse
                        @else
                            <small>Для доступа к файлам необходимо выполнить вход в личный кабинет</small>
                        @endauth
                    </div>
                </div>
                <div class="side card mb-3 p-4 shadow-sm">
                    <div class="card-header">
                        <h5>Ссылки</h5>
                    </div>
                    <div class="card-body">
                        @auth
                            @forelse($service->links_array as $link)
                                <div class="row row pb-2 mb-2" style="align-items: center; border-bottom: 1px solid #eee;">
                                    <div class="col-1">
                                        <a href="{{ $link['url'] }}" target="_blank"><i class="fa fa-external-link"></i></a>
                                    </div>
                                    <div class="col-10">
                                        <a href="{{ $link['url'] }}" target="_blank">{{ $link['name'] }}</a>
                                    </div>
                                </div>
                            @empty
                                <p>Нет ссылок</p>
                            @endforelse
                        @else
                            <small>Для доступа к ссылкам необходимо выполнить вход в личный кабинет</small>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('toasts')
    @include('toasts.auth_toast')
@endsection

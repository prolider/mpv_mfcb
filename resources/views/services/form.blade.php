@extends('layouts.site')

@section('title')
    Получение услуги
@endsection

@section('content')
    <div class="container form">
        <div class="mb-4 d-flex align-items-center justify-content-between">
			<div class="title">
				<h1>Получение услуги</h1>
			</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <div class="content shadow-sm p-4">
                    <div class="d-flex justify-content-between">
                        <div class="text-start">
                            <small class="text-muted mb-3 d-block">Направление: <a href="{{ route('categories.detail', $service->service_category_id) }}">{{ $service->category->name ?? 'Категория не указана' }}</a></small>
                        </div>
                        <div class="text-end">
                            @if($service->use_button === 'Y')
                                <small class="rounded-pill bg-warning text-sm text-dark py-1 px-3">Сопроводительная</small>    
                            @elseif ($service->use_button === 'N')
                                <small class="rounded-pill bg-light text-sm text-dark py-1 px-3">Информационная</small>    
                            @endif
                        </div>
                    </div>
                    <div class=" my-3">
                        <h3 class="">{{ $service->name }}</h3>
                    </div>
                    <div class="border-bottom border-top mb-4 py-2">
                        <span class="d-block">{{ $service->official_name }}</span>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            {!! $service->description !!}
                        </div>
                    </div>
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach

                    @if ($service->form->is_valid)
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    Внимательно ознакомьтесь с информацией и заполните необходимые поля формы
                                </div>
                            </div>
                        </div>

                        <form method="POST" action="{{ route('service.store-form', [$service, $ticket ?? null, 'user' => $properties['user'] ?? null, 'operator' => $properties['operator'] ?? null ]) }}"
                              enctype="multipart/form-data">
                            @csrf
                            <div id="form"></div>
                        </form>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="side card shadow-sm p-4">
                            <div class="card-header">
                                <h5>Файлы на скачивание</h5>
                            </div>
                            <div class="card-body">
                                @auth
                                    @forelse($service->documents as $document)
                                        <div class="row row pb-2 mb-2" style="align-items: center; border-bottom: 1px solid #eee;">
                                            <div class="col-1">
                                                <a href="/storage/{{ $document->file }}" download="{{ $document->download_name }}"><i class="fa fa-download"></i></a>
                                            </div>
                                            <div class="col-10">
                                                <a href="/storage/{{ $document->file }}" download="{{ $document->download_name }}">{{ $document->name }}</a>
                                            </div>
                                        </div>
                                    @empty
                                        <p>Нет файлов на скачивание</p>
                                    @endforelse
                                @else
                                    <p><a class="" href="#login" id="" role="button" data-toggle="modal"
                                          aria-haspopup="true" aria-expanded="false">Авторизуйтесь</a> для доступа к
                                        файлам</p>

                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="side card shadow-sm p-4">
                            <div class="card-header">
                                <h5>Ссылки</h5>
                            </div>
                            <div class="card-body">
                                @auth
                                    @forelse($service->links_array as $link)
                                        <div class="row row pb-2 mb-2" style="align-items: center; border-bottom: 1px solid #eee;">
                                            <div class="col-1">
                                                <a href="{{ $link['url'] }}" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </div>
                                            <div class="col-10">
                                                <a href="{{ $link['url'] }}" target="_blank">{{ $link['name'] }}</a>
                                            </div>
                                        </div>
                                    @empty
                                        <p>Нет ссылок</p>
                                    @endforelse
                                @else
                                    <p><a class="" href="#login" id="" role="button" data-toggle="modal"
                                          aria-haspopup="true" aria-expanded="false">Авторизуйтесь</a> для доступа к
                                        ссылкам</p>

                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    <script>
        window.form = {!! $service->form->form_data !!};
        window.serviceType = '{{ $service->use_button }}';
        @if (isset($ticket) && $ticket->formResult->result)
        window.result = {!! $ticket->formResult->result !!};
        @endif
        window.user = {!! json_encode(auth()->user()) !!};
    </script>
    <script src="/js/forms/form-renderer.js"></script>
@endsection

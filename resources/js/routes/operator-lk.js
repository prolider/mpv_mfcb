export default [
  {
    path: '/ticket',
    name: 'ticket.index',
    component: () => import('../components/operator-lk/tickets/list'),
  },
  {
    path: '/ticket/archive',
    name: 'ticket.archive',
    component: () => import('../components/operator-lk/tickets/list-archive'),
  },
  {
    path: '/ticket/:id',
    name: 'ticket.show',
    component: () => import('../components/operator-lk/tickets/show'),
    props: true,
  },
  {
    path: '/order',
    name: 'order.index',
    component: () => import('../components/operator-lk/orders/list'),
  },
  {
    path: '/order/create',
    name: 'order.create',
    component: () => import('../components/operator-lk/orders/create'),
  },
  {
    path: '/order/:id',
    name: 'order.show',
    component: () => import('../components/operator-lk/orders/show'),
    props: true,
  },
  {
    path: '/discussions/:id',
    name: 'discussions.show',
    component: () => import('../components/operator-lk/discussions/show'),
    props: true,
  },
];

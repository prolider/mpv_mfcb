const statuses = {
  N: 'Отправлена',
  W: 'Взята в работу',
  O: 'Оформление',
  C: 'Отменена',
  D: 'Услуга оказана',
  R: 'Черновик',
  L: 'Клиент заполняет',
  P: 'Запрос на отмену',
  E: 'Требует уточнения',
  F: 'Отказ',
  I: 'Приглашение',
  CR: 'Создана заявка'
};

// Возвращает доступные статусы по текущему статусу заявки
const availableStatuses = (currentStatus) => {
  switch (currentStatus) {
    case 'N':
      return {
        W: statuses.W,
      };

    case 'W':
      return {
        W: statuses.W,
        E: statuses.E,
        F: statuses.F,
        I: statuses.I,
        D: statuses.D,
      };

    case 'R':
      return {
        R: statuses.R,
        L: statuses.L,
      };
    case 'E':
      return {
        E: statuses.E,
        L: statuses.L,
      };

    case 'F':
      return {
        F: statuses.F,
        W: statuses.W,
      };

    case 'P':
      return {
        P: statuses.P,
        C: statuses.C,
      };

    case 'I':
      return {
        I: statuses.I,
        F: statuses.F,
        D: statuses.D,
      };

    default:
      if (statuses[currentStatus]) {
        const obj = {};
        obj[currentStatus] = statuses[currentStatus];
        return obj;
      }

      return {};
  }
};

export { statuses, availableStatuses };

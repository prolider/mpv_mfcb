import Vue from 'vue';
import App from './components/form-builder/app';
import Notifications from './components/notifications/module';

Vue.use(Notifications);

const vm = new Vue({
  el: '#form-builder',
  render: h => h(App),
});

global.vm = vm;

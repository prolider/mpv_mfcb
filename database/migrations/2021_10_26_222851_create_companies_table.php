<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('user_owner_id')->nullable();
            $table->integer('company_inn');
            $table->integer('company_ogrn')->nullable();
            $table->string('company_name')->nullable();
            $table->date('company_registration_date')->nullable();
            $table->integer('company_kpp')->nullable();
            $table->string('company_registration_adress')->nullable();
            $table->string('company_fio_head')->nullable();
            $table->string('company_head_position')->nullable();
            $table->boolean('company_confirmed')->nullable();
            $table->timestamp('company_confirmation_date')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}

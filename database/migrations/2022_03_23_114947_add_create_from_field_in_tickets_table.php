<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreateFromFieldInTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('is_msp');
            $table->tinyInteger('create_from')->nullable()->default(0);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('is_damask')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->boolean('is_msp')->nullable()->default(false);
            $table->dropColumn('create_from');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('is_damask');
        });
    }
}

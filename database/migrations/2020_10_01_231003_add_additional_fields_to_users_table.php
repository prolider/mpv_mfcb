<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('firstname')->nullable();
            $table->string('patronymic')->nullable();
            $table->enum('sex', [\App\Models\User::SEX_MALE, \App\Models\User::SEX_FEMALE])->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone')->nullable();

            $table->string('inn')->nullable();
            $table->string('snils')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('passport_serial')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('passport_date')->nullable();
            $table->string('passport_subdivision')->nullable();
            $table->string('passport_organization')->nullable();
            $table->string('registration_place')->nullable();

            $table->string('company_inn')->nullable();
            $table->string('company_ogrn')->nullable();
            $table->string('company_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'firstname',
                'patronymic',
                'sex',
                'birthday',
                'phone',
                'inn',
                'snils',
                'citizenship',
                'passport_serial',
                'passport_number',
                'passport_date',
                'passport_subdivision',
                'passport_organization',
                'registration_place',
                'company_inn',
                'company_ogrn',
                'company_name',
            ]);
        });
    }
}

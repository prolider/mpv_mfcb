<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketCheckListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_check_lists', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ticket_id')
                ->nullable()
                ->references('id')
                ->on('tickets');
            $table->foreignId('service_id')
                ->nullable()
                ->references('id')
                ->on('services');
            $table->text('check_list')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_check_lists');
    }
}

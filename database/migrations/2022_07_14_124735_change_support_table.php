<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supports', function (Blueprint $table) {
            $table->dropColumn('recipient');
            $table->text('okwed')->nullable();
            $table->text('not_okwed')->nullable();
            $table->boolean('ukep');
            $table->boolean('scoring');
            $table->integer('min_empl_amount')->nullable();
            $table->integer('max_empl_amount')->nullable();
            $table->integer('min_exist_term')->nullable();
            $table->text('okato');
            $table->string('support_service_name');
            $table->string('support_service_type');
            $table->string('support_phone');
            $table->string('support_email');
            $table->string('source_type')->nullable();
            $table->string('support_event_name');
            $table->boolean('complex_service_info');
            $table->text('review_decline_reason')->nullable();
            $table->text('offer_decline_reason')->nullable();
            $table->integer('support_amount_percent')->nullable();
            $table->integer('support_is_paid');
            $table->float('support_min_cost')->nullable();
            $table->float('support_max_cost')->nullable();
            $table->integer('provision_wait_period');
            $table->integer('provision_start_time')->nullable();
            $table->integer('loan_terms_min')->nullable();
            $table->integer('loan_terms_max')->nullable();
            $table->integer('loan_percent_min')->nullable();
            $table->integer('loan_percent_max')->nullable();
            $table->json('recipient_category');
            $table->string('recipient_special_category')->nullable();
            $table->text('additional_requirements')->nullable();
            $table->text('preferential_recipients')->nullable();
            $table->integer('support_amount_from')->nullable()->change();
            $table->integer('support_amount_till')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supports', function (Blueprint $table) {
            $table->text('recipient');
            $table->dropColumn('okwed');
            $table->dropColumn('not_okwed');
            $table->dropColumn('ukep');
            $table->dropColumn('scoring');
            $table->dropColumn('min_empl_amount');
            $table->dropColumn('max_empl_amount');
            $table->dropColumn('min_exist_term');
            $table->dropColumn('okato');
            $table->dropColumn('support_service_name');
            $table->dropColumn('support_service_type');
            $table->dropColumn('support_phone');
            $table->dropColumn('support_email');
            $table->dropColumn('source_type');
            $table->dropColumn('support_event_name');
            $table->dropColumn('complex_service_info');
            $table->dropColumn('review_decline_reason');
            $table->dropColumn('offer_decline_reason');
            $table->dropColumn('support_amount_percent');
            $table->dropColumn('support_is_paid');
            $table->dropColumn('support_min_cost');
            $table->dropColumn('support_max_cost');
            $table->dropColumn('provision_wait_period');
            $table->dropColumn('provision_start_time');
            $table->dropColumn('loan_terms_min');
            $table->dropColumn('loan_terms_max');
            $table->dropColumn('loan_percent_min');
            $table->dropColumn('loan_percent_max');
            $table->dropColumn('recipient_category');
            $table->dropColumn('recipient_special_category');
            $table->dropColumn('additional_requirements');
            $table->dropColumn('preferential_recipients');
        });
    }
};

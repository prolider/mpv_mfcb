<?php

use App\Models\DiscussionMessage;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('discussion_group_id')->constrained('discussions')->cascadeOnDelete();
            $table->foreignId('user_owner_id')->constrained('users')->cascadeOnDelete();
            $table->text('text')->nullable();
            $table->json('filenames')->nullable();
            $table->integer('status')->default(DiscussionMessage::STATUS_UNREAD);
            $table->date('status_changed_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_messages');
    }
}

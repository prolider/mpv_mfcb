<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_categories', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
        });

        Schema::table('supports', function (Blueprint $table) {
            $table->text('preview')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_categories', function (Blueprint $table) {
            $table->text('description')->change();
        });

        Schema::table('supports', function (Blueprint $table) {
            $table->string('preview', 250)->change();
        });
    }
};

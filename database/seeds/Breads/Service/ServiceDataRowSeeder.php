<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ServiceDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'services')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Название',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'is_msp');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Связано с мерой для МСП',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'description');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Полное описание',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'predescription');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Анонс',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'shortdescription');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Краткое описание',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_category_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Категория услуг',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'window_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Окно',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'affiliate_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Филиал',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'use_button');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'select_dropdown',
                'display_name' => 'Тип услуги',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 8,
                'details' => [
                    'default' => 'Y',
                    'choice_disabled' => true,
                    'options' => [
                        'N' => 'Информационная',
                        'Y' => 'Сопроводительная',
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'status');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'hidden',
                'display_name' => 'Тип услуги',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 9,
                'details' => [
                    'options' => [
                        'S' => 'Опубликовано',
                        'P' => 'Приватный',
                        'I' => 'Внутренний',
                    ],
                    'default' => 'S',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_period');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Период оказания услуг',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 10,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_period_type');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'select_dropdown',
                'display_name' => 'Тип периода',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 11,
                'details' => [
                    'options' => [
                        'D' => 'День',
                        'W' => 'Неделя',
                        'M' => 'Месяц',
                    ],
                    'default' => 'D',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'created_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Создано',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 12,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'updated_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Обновлено',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 13,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'deleted_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Удалено',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 14,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_belongsto_service_category_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Категория услуги',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'model' => \App\Models\ServiceCategory::class,
                    'table' => 'service_categories',
                    'type' => 'belongsTo',
                    'column' => 'service_category_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => 0,
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'form_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'hidden',
                'display_name' => 'Форма',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'official_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Официальное наименование',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 17,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'links');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Ссылки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 18,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'check_list');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'hidden',
                'display_name' => 'Check List',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 19,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'feedback');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Express',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 20,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_belongsto_affiliate_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Филиалы',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 20,
                'details' => [
                    'model' => \App\Models\Affiliate::class,
                    'table' => 'affiliates',
                    'type' => 'belongsTo',
                    'column' => 'affiliate_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot_table' => 'affiliate_services',
                    'pivot' => 0,
                    'taggable' => 0,
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_belongsto_window_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Окно',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 21,
                'details' => [
                    'model' => \App\Models\Window::class,
                    'table' => 'windows',
                    'type' => 'belongsTo',
                    'column' => 'window_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => 0,
                    'view' => 'vendor.voyager.formfields.relationship_window',
                ],
            ])->save();
        }
    }
}

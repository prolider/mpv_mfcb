<?php

use Illuminate\Database\Seeder;

class ServiceDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'services');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'services',
                'display_name_singular' => 'Услуга',
                'display_name_plural' => 'Услуги',
                'icon' => 'voyager-chat',
                'model_name' => \App\Models\Service::class,
                'generate_permissions' => 1,
                'description' => '',
                'controller' => \App\Http\Controllers\Voyager\ServiceController::class,
            ])->save();
        }
    }
}

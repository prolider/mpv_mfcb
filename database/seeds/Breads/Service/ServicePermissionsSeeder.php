<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class ServicePermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('services');

        $this->seed('PermissionRoleTableSeeder');
    }
}

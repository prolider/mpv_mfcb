<?php

use Illuminate\Database\Seeder;

class AffiliateDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'affiliates');
        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'affiliates',
                'display_name_singular' => 'Филиал',
                'display_name_plural' => 'Филиалы',
                'icon' => 'voyager-news',
                'model_name' => \App\Models\Affiliate::class,
                'generate_permissions' => 1,
                'description' => '',
                'controller' => '',
            ])->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class AffiliateDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'affiliates')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Филиал',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'code');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Код для Дамаска',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'description');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Адрес',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }
    }
}

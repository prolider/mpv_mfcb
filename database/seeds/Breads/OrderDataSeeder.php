<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class OrderDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('OrderDataTypeSeeder');
        $this->seed('OrderDataRowSeeder');
        $this->seed('OrderPermissionsSeeder');
    }
}

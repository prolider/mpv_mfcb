<?php

use Illuminate\Database\Seeder;

class SupportDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'supports');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'supports',
                'display_name_singular' => 'Услуга МСП',
                'display_name_plural' => 'Услуги МСП',
                'icon' => 'voyager-chat',
                'model_name' => \App\Models\Support::class,
                'generate_permissions' => 1,
                'description' => '',
                // 'controller' => 'App\\Http\\Controllers\\Voyager\\SupportController',
            ])->save();
        }
    }
}

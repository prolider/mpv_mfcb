<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class SupportPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('supports');

        $this->seed('PermissionRoleTableSeeder');
    }
}

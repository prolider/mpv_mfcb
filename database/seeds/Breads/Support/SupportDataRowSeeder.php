<?php

use App\Models\Support;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class SupportDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'supports')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Услуга',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_service_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Услуга',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\Service::class,
                    'table' => 'services',
                    'type' => 'belongsTo',
                    'column' => 'service_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'service',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'active');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Активно',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Название',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'preview');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Краткий Анонс',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'short_description');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Краткое Описание',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 6,
                'details' => [
                    'validation' => [
                        'rule' => 'required|max:350',
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'opf');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'multiple_checkbox',
                'display_name' => 'Получатели меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'options' => Support::OPF_LIST,
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_type');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Форма поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_support_type_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Форма поддержки',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'model' => \App\Models\SupportType::class,
                    'table' => 'support_types',
                    'type' => 'belongsTo',
                    'column' => 'support_type',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportType',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_count');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => __('Доступность меры поддержки по объему предоставляемой поддержки'),
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 10,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_amount_from');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => __('Размер поддержки - от (р.)'),
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 11,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_amount_till');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => __('Размер поддержки - до (р.)'),
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 12,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'full_description');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Полное, подробное описание меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 13,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'start_date');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата начала принятия заявок',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 14,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'end_date');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата окончания принятия заявок',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'okwed');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Перечисление допустимых кодов ОКВЭД через запятую',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'not_okwed');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Перечисление недопустимых кодов ОКВЭД через запятую',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'ukep');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Требование наличия электронной квалифицированной подписи',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'scoring');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Требование прохождения скоринга перед заполнением Заявки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'min_empl_amount');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Минимальное количество сотрудников в компании',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'max_empl_amount');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Максимальное количество сотрудников в компании',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'min_exist_term');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Минимальный срок деятельности субъекта МСП для получения меры поддержки, значение в количестве месяцев',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'okato');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Наличие ограничений предоставления меры поддержки по территориальному признаку (0 – ограничений нет или код ОКAТО – указать код соответствующей территории (через запятую, если несколько))',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_service_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Наименование меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_support_service_name_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Наименование меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\SupportServiceName::class,
                    'table' => 'support_service_names',
                    'type' => 'belongsTo',
                    'column' => 'support_service_name',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportServiceName',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_service_type');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Вид/тип меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_support_service_type_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Вид/тип меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\SupportServiceType::class,
                    'table' => 'support_service_types',
                    'type' => 'belongsTo',
                    'column' => 'support_service_type',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportServiceType',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_phone');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Телефон структурного подразделения организации образующей инфраструктуру поддержки субъектов малого и среднего предпринимательства, предоставляющего меру поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_email');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Эл.почта структурного подразделения организации образующей инфраструктуру поддержки субъектов малого и среднего предпринимательства, предоставляющего меру поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'source_type');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Способ  обращения за  мерой поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_source_type_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Способ  обращения за  мерой поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\SupportSourceType::class,
                    'table' => 'support_source_types',
                    'type' => 'belongsTo',
                    'column' => 'source_type',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportSourceType',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_event_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Наименование мероприятия, направленного на предоставление меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_support_event_name_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Наименование мероприятия, направленного на предоставление меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\SupportEventName::class,
                    'table' => 'support_event_names',
                    'type' => 'belongsTo',
                    'column' => 'support_event_name',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportEventName',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'complex_service_info');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Данная услуга является комплексной услугой',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 15,
                'details' => [
                    'display' => [
                        'width' => 6,
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'review_decline_reason');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Основание для отказа в рассмотрении отправленной Заявки на получение меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'offer_decline_reason');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Основание для отказа в рассмотрении отправленной Заявки на получение меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_amount_percent');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Размер поддержки на одного получателя меры поддержки(Заявителю данная информация будет отображаться в виде - “до 70 %”)',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_is_paid');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Платная мера поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_min_cost');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Минимальный размер платы',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_max_cost');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Максимальный размер платы',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'provision_wait_period');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Регламентный срок  предоставления меры поддержки/принятия решения после предоставления полного пакета документов, в днях',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'provision_start_time');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Срок  предоставления меры поддержки, в днях',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'loan_terms_min');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Минимальный срок, на который  предоставляется(выдается) микрозаём, заём,  кредит, и т.д., в месяцах',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'loan_terms_max');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Максимальный срок, на который  предоставляется(выдается) микрозаём, заём,  кредит, и т.д., в месяцах',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'loan_percent_min');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Максимальная величина процентной ставки предоставляемого микрозайма, займа,  кредита, и т.д. в процентах',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'loan_percent_max');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Минимальная величина процентной ставки предоставляемого микрозайма, займа,  кредита, и т.д. в процентах',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'recipient_category');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'multiple_checkbox',
                'display_name' => 'Категория получателя меры поддержки',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'options' => Support::RECIPIENT_CATEGORY,
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'recipient_special_category');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Категория получателя меры поддержки по наличию специального статуса',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_belongsto_support_recipient_special_category_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Категория получателя меры поддержки по наличию специального статуса',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 20,
                'details' => [
                    'model' => \App\Models\SupportRecipientSpecialCategory::class,
                    'table' => 'support_recipient_special_categories',
                    'type' => 'belongsTo',
                    'column' => 'recipient_special_category',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'supportRecipientSpecialCategory',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'additional_requirements');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Иные требования к получателю меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 22,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'preferential_recipients');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text_area',
                'display_name' => 'Льготные категории получателей меры поддержки',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 23,
            ])->save();
        }
    }
}

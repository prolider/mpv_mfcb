<?php

use Illuminate\Database\Seeder;

class FaqDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'faqs');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'faqs',
                'display_name_singular' => 'FAQ',
                'display_name_plural' => 'FAQ',
                'icon' => 'voyager-params',
                'model_name' => \App\Models\Faq::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

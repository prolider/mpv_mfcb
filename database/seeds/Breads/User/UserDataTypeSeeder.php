<?php

use Illuminate\Database\Seeder;

class UserDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'users');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'users',
                'display_name_singular' => __('voyager::seeders.data_types.user.singular'),
                'display_name_plural' => __('voyager::seeders.data_types.user.plural'),
                'icon' => 'voyager-person',
                'model_name' => \App\Models\User::class,
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => \App\Http\Controllers\Voyager\UserController::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

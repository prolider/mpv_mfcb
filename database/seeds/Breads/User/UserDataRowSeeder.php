<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class UserDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userDataType = DataType::where('slug', 'users')->firstOrFail();

        $dataRow = $this->dataRow($userDataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'firstname');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Фамилия',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.name'),
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'patronymic');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Отчество',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'email');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.email'),
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'password');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'password',
                'display_name' => __('voyager::seeders.data_rows.password'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'remember_token');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.remember_token'),
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'sex');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'select_dropdown',
                'display_name' => 'Пол',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 8,
                'details' => [
                    'options' => [
                        'M' => 'Мужской',
                        'F' => 'Женский',
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'phone');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Телефон',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 9,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'birthday');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата рождения',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 10,
                'details' => [
                    'format' => '%d-%m-%Y',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'created_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => __('voyager::seeders.data_rows.created_at'),
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 11,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'updated_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => __('voyager::seeders.data_rows.updated_at'),
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 12,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'avatar');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'image',
                'display_name' => __('voyager::seeders.data_rows.avatar'),
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 13,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'role_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.role'),
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 14,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'user_belongsto_role_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => __('voyager::seeders.data_rows.role'),
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => [
                    'model' => 'TCG\\Voyager\\Models\\Role',
                    'table' => 'roles',
                    'type' => 'belongsTo',
                    'column' => 'role_id',
                    'key' => 'id',
                    'label' => 'display_name',
                    'pivot_table' => 'roles',
                    'pivot' => 0,
                ],
                'order' => 15,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'user_belongstomany_role_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Roles',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => [
                    'model' => 'TCG\\Voyager\\Models\\Role',
                    'table' => 'roles',
                    'type' => 'belongsToMany',
                    'column' => 'id',
                    'key' => 'id',
                    'label' => 'display_name',
                    'pivot_table' => 'user_roles',
                    'pivot' => '1',
                    'taggable' => '0',
                ],
                'order' => 16,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'affiliate_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Филиал',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 17,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'user_belongsto_affiliate_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Филиал',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 18,
                'details' => [
                    'model' => \App\Models\Affiliate::class,
                    'table' => 'affiliates',
                    'type' => 'belongsTo',
                    'column' => 'affiliate_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'settings');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'hidden',
                'display_name' => 'Settings',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 19,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'inn');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'ИНН',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 20,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'snils');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'СНИЛС',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 21,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'citizenship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Гражданство',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 22,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'passport_serial');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Серия паспорта',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 23,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'passport_number');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Номер паспорта',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 24,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'passport_date');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата выдачи',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 25,
                'details' => [
                    'format' => '%d-%m-%Y',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'passport_subdivision');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'Код подразделения',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 26,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'passport_organization');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Кем выдан',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 27,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'registration_place');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Адрес прописки',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 28,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'company_ogrn');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'ОГРН',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 29,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'company_inn');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'number',
                'display_name' => 'ИНН компании',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 30,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'company_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Полное наименование',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 31,
            ])->save();
        }

        $dataRow = $this->dataRow($userDataType, 'is_confirmed');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Подтверждение почты',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 31,
                'details' => [
                    'on' => 1,
                    'off' => 0,
                ],
            ])->save();
        }
    }
}

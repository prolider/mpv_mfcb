<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class FaqCategoryDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('FaqCategoryDataTypeSeeder');
        $this->seed('FaqCategoryDataRowSeeder');
        $this->seed('FaqCategoryPermissionSeeder');
    }
}

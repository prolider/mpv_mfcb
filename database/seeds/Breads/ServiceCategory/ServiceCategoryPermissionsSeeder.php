<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class ServiceCategoryPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('service_categories');

        $this->seed('PermissionRoleTableSeeder');
    }
}

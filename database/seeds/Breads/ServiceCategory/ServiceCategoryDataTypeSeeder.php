<?php

use Illuminate\Database\Seeder;

class ServiceCategoryDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'service-categories');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'service_categories',
                'display_name_singular' => 'Категория услуг',
                'display_name_plural' => 'Категории услуг',
                'icon' => 'voyager-categories',
                'model_name' => \App\Models\ServiceCategory::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

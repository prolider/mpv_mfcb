<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class TicketDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('TicketDataTypeSeeder');
        $this->seed('TicketDataRowSeeder');
        $this->seed('TicketPermissionsSeeder');
    }
}

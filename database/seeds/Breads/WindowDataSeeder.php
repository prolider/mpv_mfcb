<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class WindowDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('WindowDataTypeSeeder');
        $this->seed('WindowDataRowSeeder');
        $this->seed('WindowPermissionsSeeder');
    }
}

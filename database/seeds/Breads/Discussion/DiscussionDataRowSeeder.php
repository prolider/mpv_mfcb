<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DiscussionDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::where('slug', 'discussions')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Название',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'text');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Текст',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'created_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Создано',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 0,
                'delete' => 1,
                'order' => 9,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'updated_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Обновлено',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 10,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'is_alert');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'checkbox',
                'display_name' => 'Срочное',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_owner_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Автор',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_owner_belongsto_user_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Автор',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'model' => \App\Models\User::class,
                    'table' => 'users',
                    'type' => 'belongsTo',
                    'column' => 'user_owner_id',
                    'key' => 'id',
                    'label' => 'fullName',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'user',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_recipient_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Получатель',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 8,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_recipient_belongsto_user_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Получатель',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 8,
                'details' => [
                    'model' => \App\Models\User::class,
                    'table' => 'users',
                    'type' => 'belongsTo',
                    'column' => 'user_recipient_id',
                    'key' => 'id',
                    'label' => 'fullName',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'operator',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'filenames');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'file',
                'display_name' => 'Файлы',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'discussion_hasmany_discussion_messages_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship_interactable',
                'display_name' => 'Сообщения',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 5,
                'details' => [
                    'model' => \App\Models\DiscussionMessage::class,
                    'table' => 'discussion_messages',
                    'type' => 'hasMany',
                    'column' => 'discussion_group_id',
                    'key' => 'id',
                    'label' => 'text',
                    'pivot' => 0,
                    'taggable' => null,
                    'loads' => 'author',
                    'load_model' => \App\Models\User::class,
                    'load_label' => 'fullName',
                    'load_key' => 'user_owner_id',
                    'attributes' => [
                        'created_at' => ['name' => 'Дата создания', 'type' => 'text'],
                        'status_changed_date' => ['name' => 'Прочитано', 'type' => 'text'],
                        'text' => ['name' => 'Текст', 'type' => 'text'],
                        'filenames' => ['name' => 'Файлы', 'type' => 'file'],
                    ],
                    'route_prefix' => 'voyager.discussion-messages.',
                ],
            ])->save();
        }
    }
}

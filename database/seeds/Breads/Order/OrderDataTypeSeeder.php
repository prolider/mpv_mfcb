<?php

use Illuminate\Database\Seeder;

class OrderDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'orders');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'orders',
                'display_name_singular' => 'Талон',
                'display_name_plural' => 'Талоны',
                'icon' => 'voyager-news',
                'model_name' => \App\Models\Order::class,
                'generate_permissions' => 1,
                'description' => '',
                'controller' => '',
            ])->save();
        }
    }
}

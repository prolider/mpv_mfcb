<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class OrderDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'orders')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'coupon');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Талончик',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_information');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'hidden',
                'display_name' => 'Информация пользователя',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'phone');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Телефон',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'validation' => [
                        'rule' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'email');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'E-mail',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'validation' => [
                        'rule' => 'required|email',
                    ],
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'slug_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Название услуги из Дамаск',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'status');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'select_dropdown',
                'display_name' => 'Статус',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
                'details' => [
                    'options' => [
                        'R' => 'Черновик',
                        'W' => 'Взята в работу',
                        'CR' => 'Создана заявка',
                    ],
                    'default' => 'C',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'ID Пользователя',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'operator_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'ID Оператора',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'ID Услуги',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'user_id_belongsto_user_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Клиент',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => [
                    'model' => \App\Models\User::class,
                    'table' => 'users',
                    'type' => 'belongsTo',
                    'column' => 'user_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot_table' => 'users',
                    'pivot' => 0,
                ],
                'order' => 10,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'operator_id_belongsto_user_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Оператор',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'model' => \App\Models\User::class,
                    'table' => 'users',
                    'type' => 'belongsTo',
                    'column' => 'operator_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'operator',
                ],
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'service_id_belongsto_user_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Сервис',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'model' => \App\Models\Service::class,
                    'table' => 'services',
                    'type' => 'belongsTo',
                    'column' => 'service_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                ],
            ])->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class FormDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'forms');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'forms',
                'display_name_singular' => 'Форма',
                'display_name_plural' => 'Формы',
                'icon' => 'voyager-params',
                'model_name' => \App\Models\Form::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

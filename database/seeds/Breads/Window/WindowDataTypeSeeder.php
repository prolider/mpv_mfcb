<?php

use Illuminate\Database\Seeder;

class WindowDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'windows');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'windows',
                'display_name_singular' => 'Окно',
                'display_name_plural' => 'Окна',
                'icon' => 'voyager-home',
                'model_name' => \App\Models\Window::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

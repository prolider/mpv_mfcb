<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class WindowDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'windows')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Название',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'description');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'rich_text_box',
                'display_name' => 'Описание',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'created_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата создания',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'updated_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата обновления',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'deleted_at');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'timestamp',
                'display_name' => 'Дата удаления',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'affiliate_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'ID Филиала',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'window_belongsto_affiliate_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Филиал',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'order' => 7,
                'details' => [
                    'model' => \App\Models\Affiliate::class,
                    'table' => 'affiliates',
                    'type' => 'belongsTo',
                    'column' => 'affiliate_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                ],
            ])->save();
        }
    }
}

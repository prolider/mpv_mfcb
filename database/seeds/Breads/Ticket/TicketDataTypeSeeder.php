<?php

use Illuminate\Database\Seeder;

class TicketDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'tickets');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'tickets',
                'display_name_singular' => 'Заявка',
                'display_name_plural' => 'Заявки',
                'icon' => 'voyager-ticket',
                'model_name' => \App\Models\Ticket::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

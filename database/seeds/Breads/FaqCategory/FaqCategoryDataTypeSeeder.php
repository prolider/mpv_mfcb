<?php

use Illuminate\Database\Seeder;

class FaqCategoryDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'faq_categories');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'faq_categories',
                'display_name_singular' => 'Категория',
                'display_name_plural' => 'Категории',
                'icon' => 'voyager-params',
                'model_name' => \App\Models\FaqCategory::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

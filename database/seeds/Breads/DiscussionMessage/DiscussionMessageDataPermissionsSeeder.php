<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class DiscussionMessageDataPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('discussion_messages');

        $this->seed('PermissionRoleTableSeeder');
    }
}

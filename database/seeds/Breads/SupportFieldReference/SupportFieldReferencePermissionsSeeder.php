<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class SupportFieldReferencePermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::firstOrCreate(['key' => 'browse_support_field_references', 'table_name' => 'support_field_references']);
        Permission::firstOrCreate(['key' => 'read_support_field_references', 'table_name' => 'support_field_references']);
        Permission::firstOrCreate(['key' => 'edit_support_field_references', 'table_name' => 'support_field_references']);

        $this->seed('PermissionRoleTableSeeder');
    }
}

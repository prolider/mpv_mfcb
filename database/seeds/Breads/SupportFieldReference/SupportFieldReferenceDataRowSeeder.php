<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class SupportFieldReferenceDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'support_field_references')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'field_name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => __('Название'),
                'required' => 0,
                'browse' => 1,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'order' => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'form_reference_id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'text',
                'display_name' => 'Список сопоставления',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'order' => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($dataType, 'support_field_references_belongsto_form_reference_relationship');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type' => 'relationship',
                'display_name' => 'Список сопоставления',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'order' => 1,
                'details' => [
                    'model' => \App\Models\FormReference::class,
                    'table' => 'form_references',
                    'type' => 'belongsTo',
                    'column' => 'form_reference_id',
                    'key' => 'id',
                    'label' => 'name',
                    'pivot' => 0,
                    'taggable' => null,
                    'scope' => 'form_reference',
                ],
            ])->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class SupportFieldReferenceDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'support_field_references');

        if (! $dataType->exists) {
            $dataType->fill([
                'name' => 'support_field_references',
                'display_name_singular' => 'Список сопоставлений МСП',
                'display_name_plural' => 'Список сопоставлений МСП',
                'icon' => 'voyager-chat',
                'model_name' => \App\Models\SupportFieldReference::class,
                'generate_permissions' => 1,
                'description' => '',
            ])->save();
        }
    }
}

<?php

use App\Models\SupportFieldReference;
use Illuminate\Database\Seeder;

class SupportFieldReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = SupportFieldReference::getFieldList();

        foreach ($fields as $key => $value) {
            SupportFieldReference::firstOrCreate(['code' => $key]);
        }
    }
}

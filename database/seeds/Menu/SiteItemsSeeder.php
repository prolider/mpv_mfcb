<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class SiteItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $siteMenu = Menu::where('name', 'site')->firstOrFail();
        MenuItem::where('menu_id', $siteMenu->id)->delete();

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title' => 'Направления',
            'url' => '',
            'route' => 'categories.index',
            'icon_class' => '',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title' => 'Помощь',
            'url' => '',
            'route' => 'faq',
            'icon_class' => '',
            'color' => null,
            'parent_id' => null,
            'order' => 2,
        ]);
    }
}

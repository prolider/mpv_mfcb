<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class AdminItemsSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();
        MenuItem::where('menu_id', $menu->id)->delete();

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'voyager-bell',
            'route' => 'voyager.tickets.index',
            'title' => 'Заявки',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'voyager-chat',
            'route' => 'voyager.discussions.index',
            'title' => 'Обращения',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Талоны',
            'url' => '',
            'route' => 'voyager.orders.index',
            'icon_class' => 'voyager-documentation',
            'color' => null,
            'parent_id' => null,
            'order' => 3,
        ]);

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Филиалы и окна',
            'url' => '',
            'route' => null,
            'target' => '_self',
            'icon_class' => 'voyager-window-list',
            'color' => null,
            'parent_id' => null,
            'order' => 4,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Филиалы',
            'url' => '',
            'route' => 'voyager.affiliates.index',
            'icon_class' => 'voyager-shop',
            'color' => null,
            'parent_id' => $info->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-company',
            'route' => 'voyager.windows.index',
            'title' => 'Окна',
            'order' => 2,
        ]);
        /**
         * Sub menu end
         */
        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Управление услугами',
            'url' => '',
            'route' => null,
            'target' => '_self',
            'icon_class' => 'voyager-compass',
            'color' => null,
            'parent_id' => null,
            'order' => 5,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.service-categories.index',
            'title' => 'Категории услуг',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-documentation',
            'route' => 'voyager.services.index',
            'title' => 'Услуги',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-documentation',
            'route' => 'voyager.supports.index',
            'title' => 'Услуги МСП',
            'order' => 3,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-documentation',
            'route' => 'voyager.support_field_references.index',
            'title' => 'Cопоставления МСП',
            'order' => 4,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-params',
            'route' => 'voyager.forms.index',
            'title' => 'Формы',
            'order' => 5,
        ]);

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Вопрос-ответ',
            'url' => '',
            'route' => null,
            'target' => '_self',
            'icon_class' => 'voyager-question',
            'color' => null,
            'parent_id' => null,
            'order' => 6,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.faq_categories.index',
            'title' => 'Категории вопросов',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-news',
            'route' => 'voyager.faqs.index',
            'title' => 'Вопросы-Ответы',
            'order' => 2,
        ]);
        /**
         * Sub menu end
         */
        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Настройки',
            'url' => '',
            'route' => null,
            'target' => '_self',
            'icon_class' => 'voyager-helm',
            'color' => null,
            'parent_id' => null,
            'order' => 7,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-list',
            'route' => 'voyager.menus.index',
            'title' => 'Конструктор меню',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-settings',
            'route' => 'voyager.settings.index',
            'title' => 'Настройки портала',
            'order' => 2,
        ]);
        /**
         * Sub menu end
         */
        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Инструменты',
            'url' => '',
            'route' => null,
            'target' => '_self',
            'icon_class' => 'voyager-tools',
            'color' => null,
            'parent_id' => null,
            'order' => 8,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-people',
            'route' => 'voyager.users.index',
            'title' => 'Пользователи',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-lock',
            'route' => 'voyager.roles.index',
            'title' => 'Роли',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-images',
            'route' => 'voyager.media.index',
            'title' => 'Медиа',
            'order' => 3,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-bread',
            'route' => 'voyager.bread.index',
            'title' => 'BREAD',
            'order' => 4,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-data',
            'route' => 'voyager.database.index',
            'title' => 'База данных',
            'order' => 5,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-lock',
            'route' => 'voyager.compass.index',
            'title' => 'Compass',
            'order' => 6,
        ]);
        /**
         * Sub menu end
         */
    }
}

<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class UserItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'user')->firstOrFail();
        MenuItem::where('menu_id', $menu->id)->delete();

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => '',
            'route' => 'ticket.index',
            'title' => 'Мои заявки',
            'order' => 1,
        ]);
    }
}

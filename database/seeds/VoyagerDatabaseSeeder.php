<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class VoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::select()->delete();
        DataType::select()->delete();

        $this->seed('RoleDataSeeder');
        $this->seed('UserDataSeeder');
        $this->seed('MenuDataSeeder');

        $this->seed('MenusTableSeeder');
        $this->seed('MenuItemsTableSeeder');
        $this->seed('RolesTableSeeder');
        $this->seed('PermissionsTableSeeder');
        $this->seed('PermissionRoleTableSeeder');
        $this->seed('SettingsTableSeeder');

        $this->seed('AffiliateDataSeeder');
        $this->seed('FaqCategoryDataSeeder');
        $this->seed('FaqDataSeeder');
        $this->seed('FormDataSeeder');
        $this->seed('MenuDataSeeder');
        $this->seed('OrderDataSeeder');
        $this->seed('RoleDataSeeder');
        $this->seed('ServiceDataSeeder');
        $this->seed('SupportDataSeeder');
        $this->seed('SupportFieldReferenceDataSeeder');
        $this->seed('ServiceCategoryDataSeeder');
        $this->seed('TicketDataSeeder');
        $this->seed('UserDataSeeder');
        $this->seed('WindowDataSeeder');
        $this->seed('DiscussionMessageDataSeeder');
        $this->seed('DiscussionDataSeeder');

        // $users = User::where('email', 'admin@admin.com')->get();
        // if (!$users->count()){
        //     App\Models\User::factory()->create();
        // }
    }
}

<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /* Поля пользовательской части */
    public function run()
    {
        $setting = $this->findSetting('site.title');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.site.title'),
                'value' => __('voyager::seeders.settings.site.title'),
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Site',
            ])->save();
        }

        $setting = $this->findSetting('site.description');
        $setting->fill([
            'display_name' => 'Краткое описание',
            'value' => 'МФЦ для Бизнеса это «единое окно», в котором предприниматели региона получают обширный перечень услуг в максимально доступном и понятном режиме в одном месте, что значительно упрощает систему получения государственно - муниципальных и иных услуг для бизнеса и сокращает время на их предоставление.',
            'details' => '',
            'type' => 'text',
            'order' => 2,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.about');
        $setting->fill([
            'display_name' => 'Описание сайта',
            'value' => '
                <div class="container">
                    <div class="row">
                        <div class="col-6 d-none d-md-flex col-sm-4 col-md-3">
                            <img src="images/home-image-2-min.png">
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="pb-4">
                                <div class="p-4 box-white">
                                    <h4>МФЦ для Бизнеса</h4>
                                    <p>Это &laquo;единое окно&raquo;, в котором предприниматели региона получают обширный перечень услуг в максимально доступном и понятном режиме в одном месте, что значительно упрощает систему получения государственно - муниципальных и иных услуг для бизнеса и сокращает время на их предоставление.</p>
                                </div>
                            </div>
                            <div class="row justify-content-between">
                                <div class="col-6 col-md-3 p-4">
                                    <div class="features">Бесплатно</div>
                                    <p>стоимость услуг МФЦ для бизнеса</p>
                                </div>
                                <div class="col-6 col-md-3 p-4">
                                    <div class="features">Более 50</div>
                                    <p>видов услуг для ИП и ЮЛ</p>
                                </div>
                                <div class="col-6 col-md-3 p-4">
                                    <div class="features">0-3 дня</div>
                                <p>срок получения информации</p>
                                </div>
                                <div class="col-6 col-md-3 p-4">
                                    <div class="features">Онлайн</div>
                                    <p>сервис и помощь специалиста</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-container">
                    <div class="container">
                        <div class="row mb-5">
                            <div class="col-md-12 text-center">
                                <h1>Возможности для Бизнеса</h1>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-md-12 text-center">
                                <a href="categories" class="btn btn-lg btn-coral">Перейти к услугам</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div class="card-brown">
                                    <div class="icon-block">
                                        <span class="fa fa-check-circle-o" aria-hidden="true"></span>
                                    </div>
                                    <div class="text-block">
                                        <h4>Самообслуживание на сайте 24/7</h4>
                                        <p>Информационные услуги доступны круглосуточно  на сайте</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div class="card-brown">
                                    <div class="icon-block">
                                        <span class="fa fa-check-circle-o" aria-hidden="true"></span>
                                    </div>
                                    <div class="text-block">
                                        <h4>Онлайн помощь от специалистов МФЦБ</h4>
                                        <p>Специалисты выполнят консультацию или окажут помощь в режиме онлайн</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div class="card-brown">
                                    <div class="icon-block">
                                        <span class="fa fa-check-circle-o" aria-hidden="true"></span>
                                    </div>
                                    <div class="text-block">
                                        <h4>Удаленная запись в офис обслуживания</h4>
                                        <p>Получить талон и записаться на удобное время можно на сайте, не выходя из дома</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div class="card-brown">
                                    <div class="icon-block">
                                        <span class="fa fa-check-circle-o" aria-hidden="true"></span>
                                    </div>
                                    <div class="text-block">
                                        <h4>Архив в личном кабинете</h4>
                                        <p>История обращений, архив  документов в личном кабинете доступны в любое время</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                ',
            'details' => '',
            'type' => 'rich_text_box',
            'order' => 3,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.policy_file');
        $setting->fill([
            'display_name' => 'Политика конфиденциальности',
            'value' => '',
            'details' => '',
            'type' => 'file',
            'order' => 4,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.processing_personal_data_file');
        $setting->fill([
            'display_name' => 'Согласие на обработку персональных данных',
            'value' => '',
            'details' => '',
            'type' => 'file',
            'order' => 5,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.user_agreement_file');
        $setting->fill([
            'display_name' => 'Пользовательское соглашение',
            'value' => '',
            'details' => '',
            'type' => 'file',
            'order' => 6,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.logo');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.site.logo'),
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 5,
                'group' => 'Site',
            ])->save();
        }

        $setting = $this->findSetting('site.official_name');

        $setting->fill([
            'display_name' => 'Официальное название организации',
            'value' => 'АНО «Региональный центр поддержки и сопровождения предпринимательства»',
            'details' => '',
            'type' => 'text',
            'order' => 6,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.abbreviated_name');

        $setting->fill([
            'display_name' => 'Сокращенное название организации',
            'value' => 'АНО «РЦПСП»',
            'details' => '',
            'type' => 'text',
            'order' => 7,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.official_adress');

        $setting->fill([
            'display_name' => 'Юридический адрес организации',
            'value' => 'Адрес: г. Ульяновск, проезд Максимова, д. 4',
            'details' => '',
            'type' => 'text',
            'order' => 8,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('site.official_requisites');

        $setting->fill([
            'display_name' => 'Реквизиты организации (ИНН, КПП, ОГРН)',
            'value' => 'ИНН 7325092769, КПП 732801001, ОГРН 1097300000962',
            'details' => '',
            'type' => 'text',
            'order' => 9,
            'group' => 'Site',
        ])->save();

        $setting = $this->findSetting('admin.bg_image');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.background_image'),
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 8,
                'group' => 'Admin',
            ])->save();
        }
        /* Поля административной панели */
        $setting = $this->findSetting('admin.title');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.title'),
                'value' => 'МФЦБ',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ])->save();
        }

        $setting = $this->findSetting('admin.description');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.description'),
                'value' => __('voyager::seeders.settings.admin.description_value'),
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Admin',
            ])->save();
        }

        $setting = $this->findSetting('admin.loader');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.loader'),
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 3,
                'group' => 'Admin',
            ])->save();
        }

        $setting = $this->findSetting('admin.icon_image');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.icon_image'),
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 4,
                'group' => 'Admin',
            ])->save();
        }

        $setting = $this->findSetting('admin.google_analytics_client_id');
        if (! $setting->exists) {
            $setting->fill([
                'display_name' => __('voyager::seeders.settings.admin.google_analytics_client_id'),
                'value' => '',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}

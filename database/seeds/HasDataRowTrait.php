<?php

use TCG\Voyager\Models\DataRow;

trait HasDataRowTrait
{
    protected function dataRow($type, string $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field' => $field,
        ]);
    }
}

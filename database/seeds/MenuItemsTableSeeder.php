<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class MenuItemsTableSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/Menu/';

    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $this->seed(OperatorItemsSeeder::class);
        $this->seed(SiteItemsSeeder::class);
        $this->seed(AdminItemsSeeder::class);
        $this->seed(UserItemsSeeder::class);
    }
}

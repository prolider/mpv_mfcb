"use strict";

// Class Definition
var KTLogin = function() {
    var _login;

    var _handleSignUpForm = function(e) {
        var validation;
        var form = KTUtil.getById('kt_login_signup_form');

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			form,
			{
				fields: {
					name: {
						validators: {
							notEmpty: {
								message: 'Поле Имя обязательно для заполнения'
							}
						}
					},
					firstname: {
						validators: {
							notEmpty: {
								message: 'Поле Фамилия обязательно для заполнения'
							}
						}
					},
					phone: {
                        validators: {
                            notEmpty: {
                                message: 'Поле Телефон обязательно для заполнения'
                            }
                        }
                    },
					email: {
                        validators: {
							notEmpty: {
								message: 'Поле Email обязательно для заполнения'
							},
                            emailAddress: {
								message: 'Указан некорректный email адрес'
							}
						}
					},
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Поле Пароль обязательно для заполнения'
                            },
                            regexp: {
                                regexp: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})",
                                message: 'Пароль должен содержать не менее 8 символов. Пароль должен состоять из символов разного регистра, цифр и спец. знаков.',
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'Подтверждение пароля обязательно для заполнения'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'Пароли не совпадают'
                            }
                        }
                    },
                    policy: {
                        validators: {
                            notEmpty: {
                                message: 'Вы должны принять соглашение'
                            }
                        }
                    },
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap(),
					defaultSubmit: new FormValidation.plugins.DefaultSubmit()
				}
			}
		);

        $('#kt_login_signup_submit').on('click', function (e) {
            e.preventDefault();

            validation.validate();
        });
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');

            _handleSignUpForm();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});

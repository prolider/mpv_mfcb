"use strict";

// Class Definition
var KTReset = function() {
    var _reset;

    var _handleResetPassword = function(e) {
        var validation;
        var form = KTUtil.getById('kt_reset_password_form');

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
            form,
            {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Поле Email обязательно для заполнения'
                            },
                            emailAddress: {
                                message: 'Указан некорректный email адрес'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Поле Пароль обязательно для заполнения'
                            },
                            regexp: {
                                regexp: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})",
                                message: 'Пароль должен содержать не менее 8 символов. Пароль должен состоять из символов разного регистра, цифр и спец. знаков.',
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'Подтверждение пароля обязательно для заполнения'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'Пароли не совпадают'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit()
                }
            }
        );

        $('#kt_reset_password_submit').on('click', function (e) {
            e.preventDefault();

            validation.validate();
        });
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            _reset = $('#kt_reset');

            _handleResetPassword();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTReset.init();
});
